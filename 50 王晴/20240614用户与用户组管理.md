### 用户和用户组管理

- 用户管理	
  - 添加账户 :默认情况下，会生成一个同名用户组，编号从1000开始依次递减   
  
    -g 原生组
  
    -G 附加组
  
    `useradd` -m -s /bin/bash  `XXX`
  
    `adduser`
  
  - 切换用户：
  
    `su`   只切换身份，不换shell环境，用绝对路径
  
    `su` - 环境和身份一起切换 
  
  - 删除账户 `userdel` -r `db2`
  
  - 修改账户 `usermod` [选项同添加账户] `db2`
  
  - 查看帐户 `getent passwd db2`
  
- 用户组管理
  - 添加用户组 `groupadd stu1`
  - 删除用户组 `groupdel stu1`
  - 添加用户到用户组 `usermod -aG stu1 db2`
  
- `sudo`权限管理
  - 添加到`sudo`组     `usermod -aG sudo xxx`
  - 修改/etc/`sudoers`文件

- 文件类型
  - -普通文件
  - d目录
  - l连接文件
  - b c s p 等其它文件

- 基本权限
  - r 读
  - w 写
  - x 执行

- 权限表示
  - 字符表示
  - 数字表示
    - 4 代表 r
    - 2 代表 w
    - 1 代表 x
  - 权限修改
    - `chmod` 修改权限
    - `chown` 修改拥有者
    - `chgrp` 修改所属组

#### 作业

### 权限管理练习

1. 创建`/guanli `目录，在`/guanli`下创建`zonghe `和 `jishu`两个目录（一条命令）

```bash
root@hecs-104052:~# mkdir -p /guanli/zonghe /guanli/jishu
```

2.添加组帐号`zonghe、caiwu、jishu，GID`号分别设置为2001、2002、2003

```bash
root@hecs-104052:~# groupadd -g  2001 zonghe
root@hecs-104052:~# groupadd -g  2002 caiwu
root@hecs-104052:~# groupadd -g  2003 jishu
```

3.创建`jerry、kylin、tsengia、obama`用户，其中的`kylin`用户帐号在2020年12月30日后失效

```bash
root@hecs-104052:~# adduser jerry
root@hecs-104052:~# useradd -e 2020-12-30 kylin
root@hecs-104052:~# adduser tsengia
root@hecs-104052:~# adduser obama
```

4.将`jerry、kylin、tsengia、obama`等用户添加到`zonghe`组内

```bash
root@hecs-104052:/guanli# gpasswd -M jerry,kylin,tsengia,obama zonghe
```

5.创建`handy、cucci`用户，其中`cucci`帐号的登录Shell设置为`“/sbin/nologin”`

```bash 
root@hecs-104052:~# useradd hady
root@hecs-104052:~# useradd -s /sbin/nologin cucci
```

6.将handy、`cucci`等用户添加到`jishu`组内

```bash
root@hecs-104052:~# gpasswd -M handy,cucci jishu
```

7.将上述的所有用户均要求加入到`guanli`组内

```bash
root@hecs-104052:~# groupadd guanli
root@hecs-104052:/guanli# gpasswd -M jerry,kylin,tsengia,obama,handy,cucci guanli
```

8.将`zonghe`组内的`obama`用户删除

```bash
root@hecs-104052:~# gpasswd -d obama zonghe
```

9.为jerry用户设置密码为“123456”（使用普通方法）为`cucci`用户设置密码为“`redhat`

```bash
root@hecs-104052:~# passwd jerry
root@hecs-104052:~# passwd cucci
```

10.将jerry用户锁定，并查看锁定状态

```bash
root@hecs-104052:~# passwd -L jerry
root@hecs-104052:~# passwd -S jerry
```

11.打开两个xshell窗口，通过（who 或者 w）命令查看连接状态，并通过`fuser`杀掉其中一个

```BASH
root@hecs-104052:~# w
root@hecs-104052:~# fuser -k /dev/pts/3 
```

12.查看`cucci`用户，属于那些组，并查看其详细信息

```bash
root@hecs-104052:~# groups cucci
root@hecs-104052:~# id cucci 
或者  
root@hecs-104052:~# apt-get install finger
root@hecs-104052:~# finger cucci
```

13.手工创建账号student（预留）

```bash
root@hecs-104052:~# useradd student
root@hecs-104052:~# passwd student
```

14.设置权限及归属：/`guanli`目录属组设为`guanli`， /`guanli/zonghe`目录的属组设为`zonghe /guanli/jishu`目录的属组设为`jishu`，设置3个目录都是禁止其他用户访问的权限

```bash
root@hecs-104052:~#chown :guanli /guanli
root@hecs-104052:~#ls -dl /guanli
root@hecs-104052:~#chown :zonghe /guanli/zonghe
root@hecs-104052:~#chown :jishu /guanli/jishu
root@hecs-104052:~# ls -l /guanli/
root@hecs-104052:~#chmod -R o-r /guanli
```

15.建立公共目录/`ceshi`允许技术组内的所有用户读取、写入、执行文件， 禁止其他用户读、写、执行

```bash
root@hecs-104052:~# mkdir -m 777 /ceshi 
root@hecs-104052:~#chown :jishu /ceshi
root@hecs-104052:~#chown -R o-r /ceshi
```

16.清除jerry用户密码

```bash
root@hecs-104052:~# passwd -d jerry
```

17.锁定`cucci`用户密码并查看状态

```bash
root@hecs-104052:~# passwd -L cucci
root@hecs-104052:~# passwd -S cucci
```

18.修改`obama`用户的`UID`为8888

```bash
root@hecs-104052:~# usermod -u 888 obama
```

19.通过`passwd`命令修改`kylin`用户的最长密码使用期限为60天

```bash
root@hecs-104052:~# passwd -x 60 kylin
```

20.通过id groups等命令查看用户handy信息

```bash
root@hecs-104052:~# id handy
root@hecs-104052:~# groups handy
root@hecs-104052:~# finger handy
```

