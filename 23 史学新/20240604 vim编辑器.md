1. vi 编辑器有几种模式?

   ```
   三种模式
   命令模式
   输入模式
   末行模式
   ```

   

2. 如何进入 vi 编辑器的插入模式

   ```
   在命令模式下按“a” “i” “o” 进入编辑模式
   ```

   

3. 如何进入 vi 编辑器的可视化模式

   ```
   按 “v” 或者 ctrl+v 进入可视化模式
   ```

   

4. 在 vi 编辑器中如何复制一行

   ```
   在命令模式下按 yy 复制整行到粘贴板
   ```

   

5. 在 vi 编辑器中如何进行粘贴

   ```
   在命令模式下按 p 在当前位置后粘贴
   在命令模式下按 P 在当前位置前粘贴
   ```

   

6. 如何删除从 3 行到 15 行的所有数据

   ```
   在命令模式下，输入 :3,15d
   语法格式 :start,end d  start表示起始行，end表示结束行  
   ```

   

7. vim练习：

   - 光标移动练习，命令模式下：

     - 单位级 h j k l
     - 单词级 w e b
     - 块级 gg G 0 ^ $ H M L ngg nj nk

     把下列句子按照第一句的正确顺序修改好并把多余的空行删除

     ```
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     ```

     先敲出以下代码，然后修正以下代码中的错误单词、重复单词、错误格式、多余行，修改函数名为 typing 并为定时器添加 300 毫秒延迟

     ```
     这题不会，只会删空格和删个is
     
     const bbb = () => {
     // this is a description
     // another descriptttion 
     const timer   = setTimeout(( ) => {
         console.log(that) alert('cool!')
         // awosome man !
     })
     }
     ```

     尝试在下面的文本中进行复制粘贴练习

     ```
     粘贴到这一行下面
     删除这一行
     剪切 ABC 并把它粘贴到 XYZ 前面，使这部分内容看起来像
     剪切  并把它粘贴到 ABC XYZ 前面。
     ```

     尝试修改下列文本的大小写

     ```SHELL
     cHANGE THIS LINE TO uppercase, then to LOWERCASE.
     ```

     按下面的说明进行操作

     ```
     按 dd 删除本行
     按 . 重复删除操作
     2. 再删除两行
     这行也没了
     p 把刚才删掉的粘回来
     3. 又多出 6 行
     ```

     左缩进、右缩进练习

     ```
     在这一行上依次按 3>>，<< 和 <G 看看效果
     打酱油行，我从你的全世界走过
     ```