### 操作题

1. **查找当前目录及其子目录中所有扩展名为 `.log` 的文件**：
   
   ```js
   find ./ -type f -name "*.log"
   ```
   
2. **在 `/var/logs` 目录及其子目录中查找所有包含关键字 `error` 的文件，并显示匹配的行**：
   
   ```js
   grep "error" /var/logs
   ```
   
3. **在 `/home/user/docs` 目录中查找文件名包含 `report` 的所有文件**：
   
   ```js
   find home/user/docs -type f -name "*report*"
   ```
   
4. **查找 `/etc` 目录中最近7天内修改过的文件**：
   
   ```js
   find /etc -type f -mtime -7
   ```
   
5. **显示 `/usr/bin` 目录中名为 `python` 的可执行文件的路径**：
   
   ```js
   which python
   ```
   
6. **查找系统中名为 `ls` 的命令及其手册页位置**：
   
   ```js
   whereis ls
   ```
   
7. **查找当前目录中包含关键字 `TODO` 的所有文件，并显示匹配的行和文件名**：
   
   ```js
   grep -nr "TODO" .
   // grep: 这是一个用于在文件中搜索指定模式的命令。
   // -nr: 这是 grep 命令的两个选项：
   // -n: 显示匹配行的行号。
   // -r: 递归地搜索子目录。
   // "TODO": 表示要搜索的关键词，即寻找包含 TODO 的行。
   // . :表示当前目录，即在当前目录及其子目录中搜索符合条件的文件。
   ```
   
8. **在 `/home/user/projects` 目录中查找所有包含关键字 `function` 的 `.js` 文件**：
   
   ```js
   grep -f "function" /home/user/projects/*.js
   ```
   
9. **查找并显示当前目录及其子目录中所有空文件**：
   ```js
   find . -type f -empty
   ```

10. **在 `/var/www` 目录中查找包含关键字 `database` 的所有文件，并只显示文件名**：
    
    ```js
    grep -rl "database" /var/www 
    // grep: 这是一个用于在文件中搜索指定模式的命令。
    // -r: 这是 grep 命令的选项，表示递归地搜索子目录。
    // -l: 这是 grep 命令的选项，表示只显示包含匹配关键字的文件名。
    // "database": 表示要搜索的关键词，即查找包含 database 的行。
    /// var/www: 这是要搜索的目录路径，即在 /var/www 目录及其子目录中进行搜索。
    ```

### 综合操作题

**综合操作题：**

假设你在一个名为 `/home/user/workspace` 的目录中工作。你需要完成以下任务：

1. 查找该目录中所有扩展名为 `.conf` 的文件。
2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行。
3. 将包含关键字 `server` 的文件名和匹配的行保存到一个名为 `server_lines.txt` 的文件中。

**预期命令步骤：**

1. 查找所有扩展名为 `.conf` 的文件：
   ```js
   find home/user/workspace -type f -name "*.conf"
   ```

2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行：
   ```js
   // 第一种方法：
   find home/user/workspace -type f -name "*.conf" -exec grep -r {} "server"
   //第二种方法：
   grep "server" home/user/workspace/*.conf  
   ```
   
3. 将结果保存到 `server_lines.txt` 文件中：
   ``` js
   grep -r "server" home/user/workspace/*.conf > home/user/workspace/server_lines.txt
   ```

通过这套操作题和综合操作题，你可以全面地了解和应用Linux系统中与文件和内容查询相关的常用命令。