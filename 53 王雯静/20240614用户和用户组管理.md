## 用户和用户组管理

- 用户管理    
  - 添加账户 useradd -m -s /bin/bash db2
  - 删除账户 userdel -r db2
  - 修改账户 usermod [选项同添加账户] db2
  - 查看帐户 getent passwd db2
- 用户组管理
  - 添加用户组 groupadd stu1
  - 删除用户组 groupdel stu1
  - 添加用户到用户组 usermod -aG stu1 db2
- 密码管理
  - 设置密码 passwd
- sudo权限管理
  - usermod -aG sudo xxx
  - 修改/etc/sudoers文件
- 登录权限管理
- 用户环境管理
  - 用户配置文件 ： .bashrc
  - 用户环境变量： $PATH、$HOME、$SHELL

## 文件与目录的权限管理

- 文件类型
  
  - -普通文件
  - d目录
  - l连接文件
  - b c s p 等其它文件

- 基本权限
  
  - r 读
  - w 写
  - x 执行

- 权限表示
  
  - 字符表示
  - 数字表示
    - 4 代表 r
    - 2 代表 w
    - 1 代表 x
  - 权限修改
    - chmod 修改权限
    - chown 修改拥有者
    - chgrp 修改所属组
  - 权限的继承
    - 目录权限下文件和目录的影响
    - umask
  - 特权权限
    - SUID
    - SGID
    - SBIT
  - 隐藏权限
    - i
      - 用在文件上，无法对该文件进行任何修改
      - 用在目录上，无法在里面创建新文件，但可以修改已有文件
    - a 
      - 除了可追加内容外，无法进行其它修改
    - 隐藏权限的操作
      - 修改隐藏权限 chattr
        - chattr +i XXX
        - chattr -i XXX
      - 查看隐藏权限
        - lsattr
          
          

### 权限管理练习

1. 创建/guanli 目录，在/guanli下创建zonghe 和 jishu 两个目录（一条命令）
   
   ```bash
   root@hecs-336440:/home/614# mkdir -p /guanli/zonghe && mkdir /guanli/jishu
   ```
   
   

2. 添加组帐号zonghe、caiwu、jishu，GID号分别设置为2001、2002、2003
   
   ```bash
   root@hecs-336440:/guanli# groupadd -g 2001 zonghe
   root@hecs-336440:/guanli# groupadd -g 2002 caiwu
   root@hecs-336440:/guanli# groupadd -g 2003 jishu
   
   ```
   
   

3. 创建jerry、kylin、tsengia、obama用户，其中的kylin用户帐号在2020年12月30日后失效
   
   ```bash
   root@hecs-336440:/guanli# useradd jerry
   root@hecs-336440:/guanli# useradd tsengia
   root@hecs-336440:/guanli# useradd obama
   root@hecs-336440:/guanli# useradd kylin -e 2020-12-30
   
   ```
   
   

4. 将jerry、kylin、tsengia、obama等用户添加到zonghe组内
   
   ```bash
   root@hecs-336440:~# gpasswd -M jerry,kylin,tsengia,obama zonghe
   ```
   
   

5. 创建handy、cucci用户，其中cucci帐号的登录Shell设置为“/sbin/nologin”
   
   ```bash
   root@hecs-336440:/guanli# useradd handy && useradd cucci
   root@hecs-336440:/guanli# usermod -s /sbin/nologin cucci
   ```
   
   

6. 将handy、cucci等用户添加到jishu组内
   
   ```bash
   root@hecs-336440:/guanli# gpasswd -M handy,cucci jishu
   
   ```
   
   

7. 将上述的所有用户均要求加入到guanli组内
   
   ```bash
   root@hecs-336440:/guanli#groupadd guanli
   root@hecs-336440:/guanli#gpasswd -M jerry,kylin,tsengia,obama,handy,cucci guanli
   ```
   
   

8. 将zonghe组内的obama用户删除
   
   ```bash
   root@hecs-336440:/guanli#gpasswd -d obama zonghe
   ```
   
   

9. 为jerry用户设置密码为“123456”（使用普通方法）为cucci用户设置密码为“redhat”
   
   ```bash
   root@hecs-336440:/guanli#passwd jerry
   New password: 
   Retype new password: 
    passwd: password updated successfully
   root@hecs-336440:/guanli#passwd cucci
   New password: 
   Retype new password: 
    passwd: password updated successfully
   ```
   
   

10. 将jerry用户锁定，并查看锁定状态
    
    ```bash
    root@hecs-336440:/guanli# passwd -l jerry
     passwd: password expiry information changed.
    root@hecs-336440:/guanli#passwd -S jerry
    jerry L 06/14/2024 0 99999 7 -1
    ```
    
    

11. 打开两个xshell窗口，通过（who 或者 w）命令查看连接状态，并通过fuser杀掉其中一个
    
    ```bash
    root@hecs-336440:/guanli#who
    root@hecs-336440:/guanli#fuser -k /dev/pts/2
    ```
    
    

12. 查看cucci用户，属于那些组，并查看其详细信息
    
    ```bash
    root@hecs-336440:/guanli# id cucci
     uid=1007(cucci) gid=1007(cucci) groups=1007(cucci),2003(jishu),2004(guanli)
    ```
    
    

13. 手工创建账号student（预留）
    
    ```bash
    root@hecs-336440:/guanli#useradd student
    ```
    
    

14. 设置权限及归属：/guanli目录属组设为guanli， /guanli/zonghe目录的属组设为zonghe /guanli/jishu目录的属组设为jishu，设置3个目录都是禁止其他用户访问的权限
    
    ```bash
     wwj@hecs-336440:~/0614$ sudo chgrp guanli guanli
     wwj@hecs-336440:~/0614/guanli$chmod o-r guanli
     wwj@hecs-336440:~/0614/guanli$sudo chgrp zonghe zonghe
     wwj@hecs-336440:~/0614/guanli$ chmod o-r zonghe
     wwj@hecs-336440:~/0614/guanli$sudo chgrp jishu jishu
     wwj@hecs-336440:~/0614/guanli$chmod o-r jishu
    ```
    
    

15. 建立公共目录/ceshi允许技术组内的所有用户读取、写入、执行文件， 禁止其他用户读、写、执行
    
    ```bash
     wwj@hecs-336440:~/0614/guanli$ mkdir ceshi
      wwj@hecs-336440:~/0614/guanli$sudo chgrp jishu ceshi
      wwj@hecs-336440:~/0614/guanli$chmod g+wrx,o-wrx ceshi
    ```
    
    

16. 清除jerry用户密码
    
    ```bash
     wwj@hecs-336440:~/0614/guanli$ sudo passwd -d jerry
     passwd: password expiry information changed.
    ```
    
    

17. 锁定cucci用户密码并查看状态
    
    ```bahs
    root@hecs-336440:/guanli#passwd -l cucci 
    passwd: password expiry information changed.
     root@hecs-336440:/guanli#passwd -S cucci
     cucci L 06/14/2024 0 99999 7 -1
    ```
    
    

18. 修改obama用户的UID为8888
    
    ```bash
    root@hecs-336440:/guanli#usermod -u 8888 obama
    ```
    
    

19. 通过passwd命令修改kylin用户的最长密码使用期限为60天
    
    ```bash
    root@hecs-336440:/guanli#chage -M 60 kylin
    root@hecs-336440:/guanli#passwd -S kylin
    ```
    
    

20. 通过id groups等命令查看用户handy信息
    
    ```bash
    root@hecs-336440:/guanli#id handy
    uid=1006(handy) gid=1006(handy) groups=1006(handy),2003(jishu),2004(guanli)
    root@hecs-336440:/guanli#groups handy
    handy : handy jishu guanli
    ```
    
    


