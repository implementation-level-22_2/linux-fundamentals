vi 编辑器有几种模式?

```bash
vi 编辑器有 3 种基本工作模式，分别是命令模式、文本输入模式和末行模式
```

2. 如何进入 vi 编辑器的插入模式

```bash
在命令模式下，按 "a" "i" "o" 均可进入编辑模式。此模式下能够进行：文本的输入、删除。
```

3. 如何进入 vi 编辑器的可视化模式

```bash
按 v ， V 或 Ctrl-V 键
```

4. 在 vi 编辑器中如何复制一行

```bash
在光标的位置按“ yy ”，复制当前行
```

5. 在 vi 编辑器中如何进行粘贴

```bash
p(小写) 在光标位置之后粘贴
P(大写) 在光标位置之前粘贴
```

6. 如何删除从 3 行到 15 行的所有数据

```bash
1、按Esc键进入正常模式。

2、输入:3,15d，然后按Enter键以删除行。
```

7. vim练习：

   - 光标移动练习，命令模式下：

     - 单位级 h j k l
     - 单词级 w e b
     - 块级 gg G 0 ^ $ H M L ngg nj nk

     把下列句子按照第一句的正确顺序修改好并把多余的空行删除

     ```bash
     this is a simple easy vim tutorial
     
     tutorial simple a easy this vim is
     is this tutorial vim simple a easy
     
     
     tutorial vim this is a easy simple
     tutorial easy vim simple a this is
     simple a vim easy tutorial is this
     
     tutorial is easy vim a simple this
     
     
     vim simple this tutorial a easy is
     a vim tutorial simple easy is this
     
     
     easy a simple vim is tutorial this
     vim tutorial is a easy simple this
     a this vim tutorial is easy simple
     this tutorial simple easy a is vim
     
     
     easy tutorial this simple a is vim
     a tutorial easy is this simple vim
     
     a tutorial vim is easy this simple
     simple this easy is vim tutorial a
     
     this tutorial is a easy simple vim
     vim is tutorial simple this easy a
     
     vim is simple this tutorial easy a
     easy a simple is vim this tutorial
     vim is tutorial simple a easy this
     this vim is tutorial simple easy a
     ```

     先敲出以下代码，然后修正以下代码中的错误单词、重复单词、错误格式、多余行，修改函数名为 typing 并为定时器添加 300 毫秒延迟

     ```bash
     const bbb = () => {
     // this is is a description
     //
     //   another descriptttion 
     const timer   = setTimeout(( ) => {
         console.log(that) alert('cool!')
         // awosome man !
     })
     }
     ```

     尝试在下面的文本中进行复制粘贴练习

     ```
     删除这一行
     粘贴到这一行下面
     剪切 ABC 并把它粘贴到 XYZ 前面，使这部分内容看起来像
     剪切 并把它粘贴到 ABC XYZ 前面。
     ```

     尝试修改下列文本的大小写

     ```
     Change this line to UPPERCASE, THEN TO lowercase.
     ```

     按下面的说明进行操作

     ```
     按 dd 删除本行
     按 . 重复删除操作
     2. 再删除两行
     这行也没了
     p 把刚才删掉的粘回来
     3. 又多出 6 行
     ```

     左缩进、右缩进练习

     ```
     在这一行上依次按 3>>，<< 和 <G 看看效果
     打酱油行，我从你的全世界走过
     ```