### 用户与文件的权限

#### 用户管理

超级用户：编号为0	不受权限限制	提示符#

普通用户：从1000开始	1—999为系统预留	受权限约束	提示符$

#### 添加

默认情况会生成一个同名的用户组，从1000开始

-g	为原生的组	-G	附加组

useradd -m -d /home/test/ -s /bin/bash xxx 

adduser

#### 删除

userdel XX	删除用户

userdel -r xx	彻底删除

#### 切换用户

su	只切换身份,shell等环境不变,用命令时得用绝对路径

su -	一同切换

#### 查看

查看哪些用户	gentent passwd

查看指定用户	id

设置密码	passwd	xxx

#### 修改

usermod- aG	修改用户附加一个新组

用户组管理

#### 添加

groupadd	组名

groupadd -g 编号 组名	-0 可以添加相问编高

删除

groupdel

修改 

groupmod

文件权限

10个字符

第一个为文件类型	d为目录	I为链接	-为普通	csp为其他

2-4	为文件拥有者权限

5-7	文件所属用户组权限

8-10	其他人权限

#### 权限表示

##### 字符

read 读 r	write 写 w	execute 执行 x	

##### 数字

4 读	2 写	1 执行	

u 用户 user	g 用户组 groug 	o其他 other	a 所有 all

'+' 加入	'-' 除去	'='设定

chomod u+w 文件名

### 权限管理练习

1. 创建/guanli 目录，在/guanli下创建zonghe 和 jishu 两个目录（一条命令）

```bash
root@hecs-288852:/# mkdir -p /guanli/zonghe /guanli/jishu 
```



1. 添加组帐号zonghe、caiwu、jishu，GID号分别设置为2001、2002、2003	

   ```bash
   root@hecs-288852:~# groupadd -g 2001 zonghe 
   root@hecs-288852:~# groupadd -g 2003 jishu 
   root@hecs-288852:~# groupadd -g 2002 caiwu 
   ```

   

2. 创建jerry、kylin、tsengia、obama用户，其中的kylin用户帐号在2020年12月30日后失效

```bash
root@hecs-288852:~# useradd -e 2020-12-30 kylin
root@hecs-288852:~# useradd jerry
root@hecs-288852:~# useradd tsengia
root@hecs-288852:~# useradd obama
```

1. 将jerry、kylin、tsengia、obama等用户添加到zonghe组内

   ```bash
   root@hecs-288852:~# usermod -aG zonghe jerry
   root@hecs-288852:~# usermod -aG zonghe kylin
   root@hecs-288852:~# usermod -aG zonghe tsengia
   root@hecs-288852:~# usermod -aG zonghe obama
   ```

   

2. 创建handy、cucci用户，其中cucci帐号的登录Shell设置为“/sbin/nologin”

   ```bash
   root@hecs-288852:~# useradd -s /sbin/nologin cucii
   ```

   

3. 将handy、cucci等用户添加到jishu组内

   ```bash
   root@hecs-288852:~# usermod -aG jishu handy
   root@hecs-288852:~# usermod -aG jishu cucci
   ```

   

4. 将上述的所有用户均要求加入到guanli组内

   ```bash
   root@hecs-288852:~# gpasswd -M jerry,kylin,tsengia,obama,handy,cucci guanli
   ```

   

5. 将zonghe组内的obama用户删除

   ```bash
   root@hecs-288852:~# gpasswd -d obama zonghe 
   Removing user obama from group zonghe
   ```

   

6. 为jerry用户设置密码为“123456”（使用普通方法）为cucci用户设置密码为“redhat”

   ```bash
   root@hecs-288852:~# passwd jerry 
   New password: 
   Retype new password: 
   passwd: password updated successfully
   root@hecs-288852:~# passwd cucci
   New password: 
   Retype new password: 
   passwd: password updated successfully
   ```

   

7. 将jerry用户锁定，并查看锁定状态

   ```bash
   root@hecs-288852:~# passwd -l jerry 
   root@hecs-288852:~# passwd -S jerry 
   jerry L 06/14/2024 0 99999 7 -1
   ```

   

1. 打开两个xshell窗口，通过（who 或者 w）命令查看连接状态，并通过fuser杀掉其中一个

   ```bash
    fuser -k /dev/pts/4
   ```

   

2. 查看cucci用户，属于那些组，并查看其详细信息

```bash
root@hecs-288852:~# id cucci
```



1. 手工创建账号student（预留）

   ```bash
   useradd student
   passwd student
   ```

   

2. 设置权限及归属：/guanli目录属组设为guanli， /guanli/zonghe目录的属组设为zonghe /guanli/jishu目录的属组设为jishu，设置3个目录都是禁止其他用户访问的权限

   ```bash
   root@hecs-288852:~# chown :guanli /guanli
   root@hecs-288852:~# chown :zonghe /guanli/zonghe
   root@hecs-288852:~# chown :jishu /guanli/jishu
   root@hecs-288852:/# chmod 700 guanli guanli/zonghe guanli/jishu
   ```

   

3. 建立公共目录/ceshi允许技术组内的所有用户读取、写入、执行文件， 禁止其他用户读、写、执行

   ```bash
   root@hecs-288852:/# chmod a=rwx,o-rx ceshi/
   root@hecs-288852:/# ls -ld /ceshi
   drwxrwx--- 2 root root 4096 Jun 14 17:47 /ceshi
   ```

   

4. 清除jerry用户密码

   ```bash
   root@hecs-288852:/# passwd -d jerry 
   passwd: password expiry information changed.
   ```

   

5. 锁定cucci用户密码并查看状态

   ```bash
   root@hecs-288852:~# passwd -l cucci 
   root@hecs-288852:~# passwd -S cucci
   ```

   

6. 修改obama用户的UID为8888

   ```bash
   root@hecs-288852:/# usermod -u 8888 obama
   ```

   

7. 通过passwd命令修改kylin用户的最长密码使用期限为60天

   ```bash
   root@hecs-288852:/# passwd -x 60 kylin
   passwd: password expiry information changed.
   ```

   

8. 通过id groups等命令查看用户handy信息

```bash
root@hecs-288852:/# id handy 
uid=1006(handy) gid=1006(handy) groups=1006(handy)
root@hecs-288852:/# groups handy 
handy : handy
```