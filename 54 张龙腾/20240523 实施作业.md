假设您刚刚登录到一个Linux系统，并位于您的家目录（`~`）下。您需要完成以下一系列复杂的操作来组织和清理您的文件和目录。请按照顺序执行，并给出相应的命令。

1. **创建测试文件**：在家目录下创建三个文本文件，分别命名为`.hidden.txt`（隐藏文件）、`visible1.txt`和`visible2.txt`。

   ~~~ shell
   su@hecs-169541:~$ touch .hidden.txt visible1.txt visible2.txt
   
   ~~~

   

2. **列出文件和目录**：列出家目录（`~`）下的所有文件和目录，包括隐藏文件，并查看其详细权限和属性。

   ~~~ shell
   su@hecs-169541:~$ ls -al
   total 52
   drwxr-xr-x 5 su   su   4096 May 23 23:15 .
   drwxr-xr-x 4 root root 4096 May 21 17:39 ..
   -rw------- 1 su   su   4150 May 23 11:19 .bash_history
   -rw-r--r-- 1 su   su    220 May 20 19:06 .bash_logout
   -rw-r--r-- 1 su   su   3526 May 20 19:06 .bashrc
   drwxr-xr-x 4 su   su   4096 May 23 21:35 development
   -rw-r--r-- 1 su   su      0 May 23 22:30 .hidden.txt
   -rw-r--r-- 1 su   su    807 May 20 19:06 .profile
   -rw-r--r-- 1 su   su      6 May 22 19:22 text.txt
   drwxr-xr-x 2 su   su   4096 May 23 11:18 .vim
   -rw------- 1 su   su   4295 May 23 11:18 .viminfo
   -rw-r--r-- 1 su   su      0 May 23 23:15 visible1.txt
   -rw-r--r-- 1 su   su      0 May 23 23:15 visible2.txt
   drwxr-xr-x 3 su   su   4096 May 22 10:38 个人文档
   ~~~

   

3. **创建工作区**：创建一个新的目录`work_area`，并在其中创建三个子目录：`project_a`、`project_b`和`docs`。

   ~~~ shell
   su@hecs-169541:~$ mkdir work_area
   su@hecs-169541:~/work_area$ mkdir project_a project_b docs
   su@hecs-169541:~/work_area$ ls
   docs  project_a  project_b
   
   ~~~

   

4. **移动文本文件**：将家目录下的所有`.txt`文件移动到`work_area/docs`目录中，并确保这些文件在移动后仍然是隐藏的（如果它们是隐藏的）。

   ~~~ shell
   su@hecs-169541:~$ mv /home/su/.*txt work_area/docs/ #移动隐藏文件
   su@hecs-169541:~$ mv /home/su/*.txt* work_area/docs/ #移动普通文件
   
   ~~~

   

5. **创建新文件**：在`work_area/project_a`目录下创建一个新的文本文件`notes.txt`，并添加一些内容（例如：`echo "Initial notes for project A" > work_area/project_a/notes.txt`）。

   ~~~ shell
   su@hecs-169541:~/work_area$ cd project_a
   su@hecs-169541:~/work_area/project_a$ touch notes.txt
   su@hecs-169541:~/work_area/project_a$ vim notes.txt 
   
   ~~~

   

6. **复制目录**：递归地复制`work_area/project_a`目录到`work_area/project_b`，并命名为`project_a_backup`。

   ~~~ shell
   su@hecs-169541:~$ cp -r work_area/project_a work_area/project_b
   su@hecs-169541:~/work_area/project_b$ mv project_a project_a_backup
   ~~~

   

7. **列出文件并按大小排序**：列出`work_area/docs`目录下的所有文件，并按文件大小降序排列。

   ~~~ shell
   su@hecs-169541:~/work_area$ ls -lsah docs/
   total 8.0K
   4.0K drwxr-xr-x 2 su su 4.0K May 23 23:28 .
   4.0K drwxr-xr-x 5 su su 4.0K May 23 23:17 ..
      0 -rw-r--r-- 1 su su    0 May 23 23:27 .hidden.txt
      0 -rw-r--r-- 1 su su    0 May 23 23:25 visible1.txt
      0 -rw-r--r-- 1 su su    0 May 23 23:25 visible2.txt
   ~~~

   

8. **删除所有文件**：删除`work_area/docs`目录下所有文件。

   ~~~ shell
   su@hecs-169541:~/work_area$ rm -rf docs/
   su@hecs-169541:~/work_area$ ls
   project_a  project_b
   ~~~

   

9. **删除目录**：假设您不再需要`work_area/project_b`目录及其所有内容，请递归地强制删除它。

   ~~~ shell
   su@hecs-169541:~/work_area$ ls
   project_a  project_b
   su@hecs-169541:~/work_area$ rm -rf project_b
   su@hecs-169541:~/work_area$ ls
   project_a
   ~~~

   

10. **清理空目录**：清理`work_area`目录，删除其中所有的空目录（注意：不要删除非空的目录）。

    ~~~ shell
    su@hecs-169541:~$ rmdir work_area/
    rmdir: failed to remove 'work_area/': Directory not empty
    ~~~

    

11. **创建别名**：回到您的家目录，并创建一个别名`llh`，该别名能够列出当前目录下文件和目录的长格式列表，并以人类可读的格式显示文件大小（类似于`ls -lh`命令）。

    ~~~ shell
    su@hecs-169541:~$ ls -a
    su@hecs-169541:~$ vim .bashrc
    su@hecs-169541:~$ vim .bashrc 
    su@hecs-169541:~$ llh
    -bash: llh: command not found
    su@hecs-169541:~$ logout
    root@hecs-169541:~# logout
    su@hecs-169541:~$ llh
    total 12K
    drwxr-xr-x 4 su su 4.0K May 23 21:35 development
    drwxr-xr-x 3 su su 4.0K May 23 23:43 work_area
    drwxr-xr-x 3 su su 4.0K May 22 10:38 个人文档
    ~~~
    
    