````
 **ls**：列出目录内容。
   
        ls #列出当前目录下的文件和目录    
        ls 文件夹A #列出指定目录文件夹A下的文件和目录    
        ls -d 文件夹A # 列出文件夹A本身的信息，而不是其它下面的目录和内容    
        ls -a # all 列出当前目录下的所有文件和目录，包括隐藏文件/目录    
        ls -l #以长格式列出当前目录下的文件和目录，包括权限、所有者、组、大小、创建日期等信息
        ls -h # 以易读的方式列出当前目录下的文件和目录，将文件大小以KB、MB\GB等单位表示    
        ls -t # t->time 根据修改时间排序列出当前目录下的文件和目录  .默认是大到小  
        ls -r # r -> reverse 反向排序列出当前目录下的文件和目录    
        ls -S # S -> Size 根据文件大小排序列出当前目录下的文件和目录    
        ls -R # 递归列出当前目录及其子目录下的所有文件和目录        
        ls --color     # --color（英文全拼：colorize 给着色 ） 
                        # 以不同颜色区分不同类型的文件，例如蓝色表示目录、绿色表示可执行文件等。
   
   ## 扩展如何修改环境变量 家目录下的自己的文件下，使用ls -a 显示所有文件，即可看到.bashrc这个环境变量的配置文件。将# alias ll='ls -l' 前的#号删除，并保存文件。最后要生效，就要退出帐号一次，再登录，即可使用ll来代替ls -l
   
      alias la='ls -a'
      alias l='ls -CF'



## 2. **cd**：改变当前目录。

* 进入某个目录：`cd 目录名`

* 返回上二级目录：`cd ../../`

* 返回家目录：`cd ~` 或者 `cd ` （啥也不跟）: /home/用户名

* 返回上次所在目录：`cd -`
  
  ## 3.**pwd**：显示当前工作目录的绝对路径。
  
      pwd # 显示当前工作目录的绝对路径
      dir1 = $(pwd) # 将当前工作目录路径保存到变量中（在Shell脚本中常用）
      echo $dir1

## 4. **mkdir**：创建新目录。

* 示例：`mkdir 目录名`
  
      #场景：在当前位置创建一个新的目录
      mkdir 目录名 # 创建一个新目录
      
      mkdir 路径/目录名 # 在指定路径下创建目录
      
      #场景：在创建目录的同时，如果父目录不存在，则一并创建父目录。
      mkdir -p 父目录/新目录 # 创建多个目录，包括父目录（如果不存在）
                          # 通常在写脚本的时候，为避免已存在，或不存在某目录后续命令无法继续执行，在mkdir的时候，会加-p选项
      
      #场景：创建一个具有特定权限的目录，可以用于设置目录的读写执行权限。
      mkdir -m 权限模式 目录名 # 创建具有特定权限的目录
                           # mkdir -m 777 mydir
      #场景：需要一个文件夹
      mkdir -p /tmp/新目录 # 创建临时目录,不长期保留,与用户文件隔离，避免暴露用户

## 5. **rmdir**：删除空目录。

* 示例：`rmdir 目录名` ，只能删除空的目录
  
  ## 6. **rm**：删除文件或目录。
  
      # ------------------- 删除文件 -------------------
      rm -i 文件名 # 删除指定的文件，删除前会询问是否删除（y 确认，n 取消）。
      rm -f 文件名 # 强制删除指定的文件，不会进行询问。
      rm -f a.txt b.txt # 同时删除多个文件：可以在命令中添加多个文件名，用空格分隔，如 。
                    # mkdir,touch等操作也支持一次性操作多个目标。都是用空格隔开
      rm -f a* # 删除所有以 a 开头的文件 ;删除以特定字符或模式开头的文件：使用通配符* 。
      rm -f .* # 删除隐藏文件：使用 .*，如 （但请注意，这可能会删除当前目录下的所有隐藏文件，包括 . 和 ..，所以请谨慎使用）。
      ## 扩展
      mv example.txt .example.txt  # 隐藏文件  
      mv mydir .mydir              # 隐藏目录
      # ------------------- 删除目录 -------------------
      rmdir 目录名 # 仅删除空的目录。
      rm -r 目录名 # 递归地删除目录及其子目录和文件，删除前会询问是否删除。
      rm -rf 目录名 # 递归地强制删除目录及其子目录和文件，不会进行询问。
      rm -rfv 目录名 # 除了递归强制删除外，还会显示删除的详细步骤。
      rm -d 目录名 # 只删除空目录（与 rmdir 命令类似）。

## 7. **cp**：copy 复制文件或目录。

复制的时候，如果目标已存在，会被覆盖

* 复制文件：`cp 源文件 目标文件`
* 复制文件到目录： cp 源文件 目标目录
* 复制目录：`cp -r 源目录 目标目录` :没-r复制不了目录
* 当cp有三个参数以上时：
  * 最后一个是目标目录
  * 而且必须是目录，不是文件

`cp` 是 Linux 和 Unix 系统中用于复制文件或目录的命令。以下是 `cp` 命令的一些常见用法：
       # 1.复制单个文件：
       cp 文件1 目录1/文件1 # 将文件1复制一份到目录1，目标文件如果已经存在就会被覆盖掉
       cp 文件1 文件2 # 将文件1复制一份为文件2
       cp 文件1 目录1/文件2 # 将文件1复制一份到目录1，并改名为文件2


       # 例如，将 `old.txt` 复制为 `new.txt`：
       cp old.txt new.txt # 最终得到old.txt 和 new.txt  


       # 2.复制文件到目录：
       cp 要复制的文件名 要复制到的文件夹路径
       # 例如，将 `成绩单.txt` 复制到 `/usr/local/各班成绩汇总` 目录：
       cp 成绩单.txt /usr/local/各班成绩汇总/  # 最终在/usr/local/各班成绩汇总/也会出现成绩单.txt文件


       #3. 递归复制目录：
       # 复制目录时使用 `-r` 或 `-R` 选项可以递归地复制目录及其内容。
       cp -r 源目录 目标目录
       # 例如，将 `22级1班成绩` 目录及其内容复制到 `22级成绩汇总`目录下：
       cp -r 22级1班成绩 22级成绩汇总/


       #4. 交互式复制：# 会确认覆盖信息
       #使用 `-i` 选项，在覆盖已存在的文件前会提示用户确认。
       cp -i 源文件 目标文件 # 复制源文件为目标文件，如果目标文件已经存在，会底部是否覆盖
       # 例如：将c1.txt复制成c2.txt时，发现c2.txt已存在
       $ cp -i c1.txt c2.txt
       cp: 是否覆盖 'c2.txt'？ # 要覆盖就输入y,如果是n是取消复制


       #5. 保留文件属性：原封不动的复制
       # 使用 `-p` 选项可以保留原文件的属性（如时间戳、权限等）。
       cp -p 源文件 目标文件
       # 例如：将c1.txt复制成c3.txt，并保留c1.txt的文件属性
       $ cp -p c1.txt c3.txt

       #6. 显示复制进度：
       # 使用 `-v` 选项可以显示详细的复制进度信息。类似的有tar rm 
       cp -rv 源目录 目标目录 # 会在递归复制的时候显示进度信息
          ```
       #7. 仅当源文件较新时复制**： update
       # 使用 `-u` 选项可以仅当源文件比目标文件新时才进行复制。update
       cp 1班成绩.txt 1班成绩.txt.bak  # 直接覆盖
       cp -u 1班成绩.txt 1班成绩.txt.bak  # 更新才复制


       #8. 强制复制：rm -f 
       # 使用 `-f` 或 `--force` 选项可以强制复制，即使目标文件已存在也不提示，直接覆盖。
       cp -f old_file new_file

       #9. 批量复制文件：
       # 可以使用通配符（如 `*`）来批量复制文件。
       cp *.txt 笔记/
       # 这将复制当前目录下所有 `.txt` 结尾的文件到 `笔记` 目录。

       #10 同时复制多个文件或目录到 某个目录
       cp 1.txt 2.txt 3.txt 笔记 # 一次性同时将1、2、3.txt都复制到笔记目录
       cp -r dir1 dir2 dir3 # 同时将dir1,dir2复制到dir3内，因为是复制目录，要加-R
       cp -R 1.txt dir1 dir2 # 同时将文件和目录一起复制到dir2

       #11 复制时产生备份文件,尾标~
       cp -b a.txt tmp/ # 如果/tmp目录已经有a.txt 就会将原来的a.txt备份成a.txt~ backup

       #12 指定备份文件尾标   
       cp -b -S _bak a.txt /tmp # 如果/tmp目录已经有a.txt 就会将原来的a.txt备份成a.txt_bak
        #使用 cp -b -S _bak a.txt /tmp 命令，当目标目录 /tmp 中已经存在 a.txt 文件时，会先将其重命名为 a.txt_bak 作为备份，然后再将新的 a.txt 文件复制到目标目录。这可以有效地防止文件被覆盖，确保数据的安全性

## 8. **mv**：move 移动文件或目录，也可用于重命名文件或目录。

* 移动文件或目录：`mv 源目录/文件 目标目录` ，相当于剪切

* 重命名文件或目录：`mv 原文件名 新文件名`在 Linux 中，`mv` 命令用于移动文件或目录，或者对它们进行重命名。以下是 `mv` 命令的一些常见用法：

原则： 目标不存在，一定重命名

1. 当是移动时，只能是文件移到目录

2. 如何分辩是移动还是重命名
   
   1. 源文件 - 目标文件 :mv a.txt b.txt
      
      1. 目标已经存在：源文件替换了目标文件
      2. 目标不存在：直接将源文件改名为目标文件
   
   2. 源文件 -目标目录、:mv a.txt dir1
      
      1. 目标存在：将源文件移到目标目录里
      2. 目标不存在：将源文件改为目标名
   
   3. 源目录 - 目标目录
      
      1. 目标已存在：将源移到目标里
      2. 已经不存在：源目录改为目标目录名
   
   4. 8种场景
      
      1. 文件到文件：
      
      2. 目标存在：目标会源文件替换掉。原文件不见了
      
      3. 目标不存在：直接重命名了
      
      4. 文件到目录
      
      5. 目标目录存在：
         
         1. 目标目录又存在源文件的同名文件：直接覆盖
         2. 目标目录不存在同名文件，直接移动
      
      6. 目标目录不存在：重命名
      
      7. 目录到目录
      
      8. 目标存在：移动到目标目录
      
      9. 目标不存在：重命名
      
      10. 目录到文件
      
      11. 目标存在：报错
      
      12. 目标不存在：重命名
   
   5. **移动文件或目录**：

    # 将文件从移动到指定目录：
    mv 文件 目标目录
    # 将源目录A移动到目标目录B
    mv 目录A 目录B
    
    #注意：如果目标目录不存在，`mv` 命令会将其视为要重命名的文件名

2. **重命名文件或目录**：
   
   # 重命名文件：
   
    mv 旧文件名 新文件名
   
   # 重命名目录
   
    mv 旧目录名 新目录名
    #注意，如果新文件名已存在，就会被覆盖，如果新目录名已经存在，就变成了移动

3. **移动并重命名：**
   
   # 移动一个文件到新目录，并重命名这个文件。
   
    mv 1.txt /tmp/2.txt # 移动当前目录下的1.txt到/tmp目录，并重命名为2.txt
   
   # 移动了一个目录到另一个目录下，并重命名
   
    mv aDir /tmp/bDir # 将当前目录下的aDir目录移动到/tmp目录下，将改名为bDir
   
   # 移动多个文件 参数大于等于3个时：
   
   - mv # 跟了3个参数以上时，最后一个默认是目录
     
     # 同时移动多个文件到指定目标
     
     mv 1.txt 2.txt  /tmp  # 将1.txt和2.txt 移动到/tmp目录下
     
     # 同时移动多个目录到指定目录下
     
     mv aaa bbb /tmp # 将aaa和bbb 移动到/tmp目录下
     
     # 使用通配符（如 `*`）来匹配多个文件或目录，并将它们移动到目标目录。
     
     mv *.txt /tmp     # 移动当前目录下所有.txt结尾的文件和目录到/tmp目录下
     mv a* /tmp         # 移动当前目录下，所有a开头的文件和目录到 /tmp目录下
     mv * /tmp          # 移动当前目录下的，所有文件和目录到/tmp目录下

4. **交互式操作**：
   
   # 使用 `-i` 选项，`mv` 命令会在移动或重命名文件前进行确认，以避免意外覆盖文件。
   
    mv -i 1.txt 2.txt # 当2.txt文件已经存在时，系统会提示你是否要覆盖。y 确认，n 取消

5. **显示详细信息**：
   
   # 使用 `-v` 选项，`mv` 命令会显示详细的操作信息，包括移动或重命名的文件名。
   
    mv -v 1.txt 2.txt # 重命名1.txt为2.txt并显示详情
    renamed '1.txt' -> '2.txt' # 显示详细

7. **覆盖文件前先备份**：
* 使用 `-b` 选项，`mv` 命令会在覆盖文件之前对其进行备份。
    mv -b 1.txt 2.txt # 如果2.txt已存在，就先备份2.txt为2.txt~,再次1.txt改成为2.txt

## 9. **touch**：`touch` 命令主要用于创建新文件和更新文件时间戳。

```bash
#1. 创建一个空文件
touch filename #如果 filename 不存在，touch 会创建一个新的空文件。

#2. 更新文件的时间戳
touch filename
#如果 filename 已经存在，touch 会更新该文件的访问和修改时间为当前时间。

#3.创建多个空文件
touch file1 file2 file3  #touch 可以同时创建多个空文件
```

10. **chmod**：修改文件或目录权限。
* 示例：`chmod 权限值 文件名`
1. **chown**：改变文件或目录的所有者。
* 示例：`chown 用户名 文件名`
1. **chgrp**：改变文件或目录的所属组。
* 示例：`chgrp 组名 文件名`

这些是常用的Linux文件夹和文件管理相关操作命令，通过它们你可以进行各种文件和目录的管理

## 10.常见的文件属性：

### 1. 权限（Permissions）

权限决定了文件或目录的访问级别。每个文件或目录都有三组权限，分别针对文件拥有者（user）、所属组（group）和其他人（others）。权限可以通过 `ls -l` 命令查看。

* **读（r）**：4
* **写（w）**：2
* **执行（x）**：1

权限组合用三个八进制数字表示，例如 `755` 表示 `rwxr-xr-x`。

### 2. 所有权（Ownership）

每个文件或目录都有一个所有者和一个所属组。所有权信息可以通过 `ls -l` 命令查看。

* **用户（User）**：文件的拥有者。
* **组（Group）**：文件所属的组。

可以使用 `chown` 命令更改文件的所有权：
    chown user:group filename

### 3. 时间戳（Timestamps）

每个文件或目录有三个时间戳，分别记录不同的时间信息，可以通过 `ls -l` 或 `stat` 命令查看。

* **访问时间（atime）**：最后一次读取文件内容的时间。
* **修改时间（mtime）**：最后一次修改文件内容的时间。
* **状态改变时间（ctime）**：最后一次改变文件元数据（如权限或所有权）的时间。

### 4. 文件类型（File Type）

文件类型决定了文件的用途和行为，可以通过 `ls -l` 命令查看第一个字符来识别。

* **普通文件（-）**：普通数据文件。
* **目录（d）**：包含其他文件和目录的文件。
* **符号链接（l）**：指向另一个文件的链接。
* **字符设备文件（c）**：用于与设备通信的文件，按字符流读取数据。
* **块设备文件（b）**：用于与设备通信的文件，按块读取数据。
* **套接字文件（s）**：用于网络通信的文件。
* **命名管道（p）**：用于进程间通信的文件。

### 5. 文件大小（Size）

文件大小以字节为单位，表示文件内容的实际存储空间，可以通过 `ls -l` 命令查看。

### 6. 链接数（Links）

链接数表示硬链接的数量，即指向同一个文件内容的不同文件名数量。可以通过 `ls -l` 命令查看链接数。

## 11.查找文件和目录

### 1. `find` 命令

`find` 是一个功能非常强大的查找工具，可以递归地搜索目录及其子目录中的文件和目录。它支持多种搜索条件，如名称、大小、修改时间等。
    find /path -name "filename"  # 按名称查找文件
    find /path -type d -name "dirname"  # 按名称查找目录
    find /path -type f -name "*.txt"  # 查找所有 .txt 文件
    find /path -type f -size +1M  # 查找大于 1MB 的文件
    find /path -type f -mtime -7  # 查找最近7天内修改过的文件
    find /path -type f -exec command {} \;  # 查找文件并对其执行命令

示例：
    find /home/user -name "document.txt"  # 在 /home/user 目录及其子目录中查找名为 document.txt 的文件
    find /var/log -type f -name "*.log" -mtime -1  # 查找 /var/log 目录中最近一天内修改过的 .log 文件

### 2. `locate` 命令 （第三方软件需要安装）

`locate` 命令通过预建的数据库快速查找文件和目录。它的速度比 `find` 更快，但需要定期更新数据库（通常通过 `updatedb` 命令）。

#### 基本用法

```bash
locate filename  # 查找文件名中包含 "filename" 的所有文件
locate "*.txt"  # 查找所有 .txt 文件
```

### 3. `which` 命令

`which` 命令查找可执行文件的位置。它在用户的 `PATH` 环境变量中搜索命令。

which命令通常用于以下几种场景：

1. **查找命令的完整路径**：如果你想知道某个命令（如ls、cd等）在系统中的完整路径，可以使用which命令。例如，`which ls`会返回ls命令的完整路径，如`/bin/ls`。
2. **验证命令是否存在**：通过检查which命令的输出，你可以验证系统中是否存在某个命令。如果which命令没有输出或返回错误信息，那么说明该命令不存在。

#### 基本用法

```bash
which command  # 查找命令的路径
# 例子
which ls  # 查找 ls 命令的路径
```

### 4. `whereis` 命令

`whereis` 命令查找可执行文件、源代码文件和手册页的位置。

#### 基本用法

```bash
whereis command  # 查找命令的可执行文件、源代码文件和手册页
#例子
whereis ls  # 查找 ls 命令的可执行文件、源代码文件和手册页
```

### 5. `type` 命令

`type` 命令显示命令的类型，例如是否是内置命令、别名或可执行文件。
    type ls  # 显示 ls 命令的类型

### 6. `grep` 命令

`grep` 命令用于在文件内容中搜索匹配的字符串。虽然它通常用于搜索文件内容，但也可以结合其他命令一起使用来查找文件。
    grep "pattern" filename  # 在文件中搜索字符串
    grep -r "pattern" /path  # 递归地在目录中搜索字符串
    # 示例：
    grep "error" /var/log/syslog  # 在 /var/log/syslog 文件中搜索包含 "error" 的行
    grep -r "TODO" /home/user/projects  # 在 /home/user/projects 目录及其子目录中搜索包含 "TODO" 的文件

### 7. `find` 与 `grep` 结合使用

通过结合 `find` 和 `grep` 命令，可以更加灵活地查找文件及其内容。
    find /path -type f -name "*.txt" -exec grep "pattern" {} +  # 查找包含特定字符串的所有 .txt 文件

### 8. `stat` 命令

`stat` 命令用于显示文件或文件系统的详细信息，包括大小、权限、修改时间等。

#### 基本用法

```bash
stat filename  # 显示文件的详细信息
# 示例：
stat document.txt  # 显示 document.txt 文件的详细信息
```



**1. 初始文件结构创建**

在您的家目录（`~`）下创建一个名为`my_projects`的目录，并在该目录下创建`proj_a`、`proj_b`两个子目录，同时在`proj_a`中创建`file1.txt`和`file2.jpg`两个文件。
    abc@hecs-288034:~$ mkdir my_projects
    abc@hecs-288034:~$ ls
    my_projects
    abc@hecs-288034:~$ cd my_projects/
    abc@hecs-288034:~/my_projects$ mkdir proj_a proj_b -p
    abc@hecs-288034:~/my_projects$ tree
    .
    ├── proj_a
    └── proj_b

    2 directories, 0 files

**2. 复制同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_a/file1_copy.txt`。
    abc@hecs-288034:~/my_projects$ mv proj_a/file1.txt  proj_b/file1_copy.txt
    abc@hecs-288034:~/my_projects$ tree
    .
    ├── proj_a
    │   └── file2.txt
    └── proj_b
        └── file1_copy.txt

    2 directories, 2 files

**3. 复制不同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_b/`，并重命名为`copied_file1.txt`。
    abc@hecs-288034:~$ cp my_projects/proj_a_copy/file1.txt my_projects/proj_b/copied_file1.txt
    abc@hecs-288034:~$ tree
    .
    └── my_projects
        ├── proj_a_copy
        │   ├── file1.txt
        │   └── file2.txt
        └── proj_b
            ├── copied_file1.txt
            └── file1_copy.txt

    3 directories, 4 files

**4. 复制同路径目录**

复制`my_projects/proj_a`到`my_projects/proj_a_copy`。
    abc@hecs-288034:~$ mv my_projects/proj_a  my_projects/proj_a_copy
    abc@hecs-288034:~$ tree
    .
    └── my_projects
        ├── proj_a_copy
        │   └── file2.txt
        └── proj_b
            └── file1_copy.txt

    3 directories, 2 files

**5. 复制不同路径目录**

将`my_projects/proj_a`目录复制到您的家目录（`~`），并重命名为`home_proj_a`。
    abc@hecs-288034:~$ cp -r my_projects/proj_a_copy   home_proj_a
    abc@hecs-288034:~$ tree
    .
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    └── my_projects
        ├── proj_a_copy
        │   ├── file1.txt
        │   └── file2.txt
        └── proj_b
            ├── copied_file1.txt
            └── file1_copy.txt

    4 directories, 6 files

**6. 复制文件和目录到不同路径**

将`my_projects/proj_a/file2.jpg`和`my_projects/proj_b`目录一起复制到`~/archive`目录下。
    abc@hecs-288034:~$ cp my_projects/proj_a_copy/file2.txt ~/archive/ && cp -r my_projects/proj_b ~/archive/
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    └── my_projects
        ├── proj_a_copy
        │   ├── file1.txt
        │   └── file2.txt
        └── proj_b
            ├── copied_file1.txt
            └── file1_copy.txt

    6 directories, 9 files

**7. 批量复制**

将`my_projects`目录下的所有`.txt`文件复制到`~/text_files`目录下。
    abc@hecs-288034:~$ find my_projects -type f -name "*.txt" -exec cp {} ~/text_files/ \;
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    ├── my_projects
    │   ├── proj_a_copy
    │   │   ├── file1.txt
    │   │   └── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    └── text_files
        ├── copied_file1.txt
        ├── file1_copy.txt
        ├── file1.txt
        └── file2.txt

    这个命令的解释如下：

    find my_projects：在my_projects目录下开始查找。
    -type f：只查找文件（不包括目录）。
    -name "*.txt"：只查找名字以.txt结尾的文件。
    -exec cp {} ~/text_files/ \;：对找到的每个文件执行cp命令，将文件复制到~/text_files/目录下。{}是一个占位符，代表find命令找到的每个文件的路径。\;表示-exec选项的结束。
    注意，如果~/text_files目录下已经存在与要复制的.txt文件同名的文件，cp命令会覆盖它们。如果你不想覆盖现有的文件，你可以使用-i选项来提示你是否要覆盖：



**8. 交互式复制文件**

新建一个`test.jpg`文件，然后将其从`my_projects`目录复制到`~/images`目录下。如果该文件已存在，则提示用户覆盖。
    abc@hecs-288034:~$ cp -i my_projects/test.jpg  ~/images/
    cp: overwrite '/home/abc/images/test.jpg'? y
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    ├── images
    │   └── test.jpg
    ├── my_projects
    │   ├── proj_a_copy
    │   │   ├── file1.txt
    │   │   └── file2.txt
    │   ├── proj_b
    │   │   ├── copied_file1.txt
    │   │   └── file1_copy.txt
    │   └── test.jpg
    └── text_files
        ├── copied_file1.txt
        ├── file1_copy.txt
        ├── file1.txt
        └── file2.txt

    8 directories, 15 files

**9. 显示复制进度**

使用`rsync`命令将`my_projects/proj_a`目录及其内容复制到`~/sync_folder`，并显示复制进度。


    abc@hecs-288034:~$ mkdir ~/sync_folder
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    ├── images
    │   └── test.jpg
    ├── my_projects
    │   ├── proj_a_copy
    │   │   ├── file1.txt
    │   │   └── file2.txt
    │   ├── proj_b
    │   │   ├── copied_file1.txt
    │   │   └── file1_copy.txt
    │   └── test.jpg
    ├── sync_folder
    └── text_files
        ├── copied_file1.txt
        ├── file1_copy.txt
        ├── file1.txt
        └── file2.txt

    9 directories, 15 files

    abc@hecs-288034:~$ rsync -av --progress my_projects/proj_a_copy/ ~/sync_folder/
    sending incremental file list
    ./
    file1.txt
                  0 100%    0.00kB/s    0:00:00 (xfr#1, to-chk=1/3)
    file2.txt
                  0 100%    0.00kB/s    0:00:00 (xfr#2, to-chk=0/3)

    sent 181 bytes  received 57 bytes  476.00 bytes/sec
    total size is 0  speedup is 0.00



**10. 条件复制**

只有当`my_projects/proj_a/file1.txt`比`~/old_files/file1.txt`新或不存在时，才复制该文件。

cp -u ~/my_projects/proj_a/file1.txt ~/old_file





**11. 复制时备份**

在复制`my_projects/proj_a/file1.txt`到`~/backup`目录之前，如果`~/backup/file1.txt`已存在，则先创建一个带有`.bak`后缀的备份文件。
    abc@hecs-288034:~$ cp -b  my_projects/proj_a_copy/file1.txt /tmp/backup
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── backup
    │   ├── file1.txt
    │   ├── file1.txt~
    │   └── file1.txt_bak
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    ├── images
    │   └── test.jpg
    ├── my_projects
    │   ├── proj_a_copy
    │   │   ├── file1.txt
    │   │   └── file2.txt
    │   ├── proj_b
    │   │   ├── copied_file1.txt
    │   │   └── file1_copy.txt
    │   └── test.jpg
    ├── sync_folder
    │   ├── file1.txt
    │   └── file2.txt
    └── text_files
        ├── copied_file1.txt
        ├── file1_copy.txt
        ├── file1.txt
        └── file2.txt

cp -b --suffix=.bak my_projects/proj_a/file1.txt backup



**12. 重命名文件或目录**

将`my_projects/proj_a/file1.txt`重命名为`my_projects/proj_a/new_file1.txt`，如果`new_file1.txt`已存在，则提示用户确认是否覆盖。
    abc@hecs-288034:~$ mv my_projects/proj_a_copy/file1.txt  my_projects/proj_a_copy/new_file1.txt
    abc@hecs-288034:~$ tree
    .
    ├── archive
    │   ├── file2.txt
    │   └── proj_b
    │       ├── copied_file1.txt
    │       └── file1_copy.txt
    ├── backup
    │   ├── file1.txt
    │   ├── file1.txt~
    │   └── file1.txt_bak
    ├── file1.txt
    ├── home_proj_a
    │   ├── file1.txt
    │   └── file2.txt
    ├── images
    │   └── test.jpg
    ├── my_projects
    │   ├── proj_a_copy
    │   │   ├── file2.txt
    │   │   └── new_file1.txt
    │   ├── proj_b
    │   │   ├── copied_file1.txt
    │   │   └── file1_copy.txt
    │   └── test.jpg
    ├── new_file1.txt
    ├── sync_folder
    │   ├── file1.txt
    │   └── file2.txt
    └── text_files
        ├── copied_file1.txt
        ├── file1_copy.txt
        ├── file1.txt
        └── file2.txt



**13. 移动并重命名文件或目录**

将`my_projects/proj_b`目录移动到`~/moved_projects`并重命名为`moved_proj_b`，如果`moved_proj_b`已存在，则先备份该目录再进行移动和重命名。

mv -b my_projects/proj_b moved_projects/moved_proj_b
````

![image-20240526142729370](C:\Users\28749\AppData\Roaming\Typora\typora-user-images\image-20240526142729370.png)

![image-20240526142739750](C:\Users\28749\AppData\Roaming\Typora\typora-user-images\image-20240526142739750.png)

![image-20240526142750894](C:\Users\28749\AppData\Roaming\Typora\typora-user-images\image-20240526142750894.png)