# 笔记：

cat:正向显示整个文件

```bash
语法：cat [选项] 文件 ……  
# 作用：显示文件内容，也可以将多个文件内容连接在一起输出
-n 或 --number：对所有输出的行进行编号。
-b 或 --number-nonblank：对非空行进行编号。
-s 或 --squeeze-blank：压缩连续的空白行，即如果有多个连续的空行，则只显示一个空行。
-E 或 --show-ends：在每行的末尾显示$符号，表示行尾。这个选项通常与-v或--show-nonprinting一起使用，以帮助可视化文本中的控制字符。
-T 或 --show-tabs：将制表符显示为^I。这个选项也常与-v或--show-nonprinting结合使用。
-v 或 --show-nonprinting：除了LFD和TAB之外，使用^和M-符号来显示非打印字符。例如，控制字符会显示为^C，其中C是控制字符在ASCII码中的对应字母。
-A 或 --show-all：等同于-vET，即显示所有非打印字符，并将制表符和行尾显示出来。
--help：显示帮助信息。
--version：显示cat命令的版本信息。
另外，cat命令还可以与其他命令通过管道（|）结合使用，以实现更复杂的文本处理任务。例如，你可以使用cat命令将文件内容传递给grep命令进行搜索，或者使用awk、sed等工具进行
进一步的文本处理。


注意：cat会将文件内容一次性全部显示出来，大文件不推荐使用。
```



tac：反向显示文件

```bash
tac 文件 ...


用于反向显示文件内容。其参数包括：
-n：以不打印换行符的方式输出。
-v：详细输出，包括每个读取的文件的文件名。
-f：这个选项在标准的tac命令中并不常见，可能是特定版本或实现的扩展。在常见的tac实现中，并没有这个选项。如果需要使用，请查阅具体版本的tac命令文档。
[文件]：指定需要查看的文件名。


 
```

cat和tac 共同点：1、一次性加载整个文件

                               2、一次性显示整个文件

缺点： 适用于小文件一屏幕能看完的 

             加载大文件则会变慢

more：

```bash
more [选项] 文件

 作用：分页显示文件内容，每次显示一屏。即按页显示文件内容



```



less：

```bash
less  [选项] 文件

作用：与`more`类似，但提供了更多的导航和搜索功能，如上下滚动、搜索等。
使用上下键、Page Up/Page Down键进行滚动，`/`搜索，`n`下一个匹配，`N`上一个匹配，`q`退出。

**常用选项：**

* **无选项**：
  * 直接使用 `less 文件名` 来查看文件内容。
* **-N 或 --line-numbers**：
  * 显示行号。
* **-M 或 --long-prompt**：
  * 显示长提示符，包括文件名和行号，百分比。
* **-MN** 内容中显示行号，底部显示文件名，行号和百分比
  * 两者可以组合使用
* **-m 或 -i**
  * 搜索时忽略大小写
* **+行数**：
  * 打开文件后立即跳转到指定的行数。
```



head   显示前几行内容：

```bash
head [选项] 文件 ...
作用：显示文件开头的内容（默认10行）

- n 选项制定行数 
- p 不显示文件名
```



tail查看末尾

```bash
tail [选项] 文件...
显示文件结尾的内容（默认10行）。
使用`-n`选项指定行数
实时查看文件增长（如日志文件）：`tail -f logfile.txt`
-q：不显示文件名。当使用多个文件作为输入时，该选项可以省略文件名前的标题。


```

nl：

```bash
nl [选项] 文件...
 作用： 显示文件内容，并添加行号（空行不加）。
常见用：

* 为文件内容添加行号：`nl file.txt`
* 使用`-s`选项指定分隔符：`nl -s: file.txt`（使用冒号作为行号分隔符）
```





1. **操作题一**：使用 `cat` 命令显示 `/etc/passwd` 文件的内容。
   
   ```bash
   cat /etc/passwd
   ```

2. **操作题二**：将文件 `/etc/passwd` 的内容复制到 `passwd_bak.txt` 文件中，但不使用 `cp` 命令。
   
   ```bash
   cat /etc/passwd > passwd_bak.txt
   ```

3. **操作题三**：新建两个文件  `file1.txt` 和 `file2.txt`，分别写一些不同的内容，再将这两个文件的内容合并到一个新的文件 `file3.txt` 中。
   
   ```bash
   abc@hecs-288034:~/passwd$ echo 这里是file1.txt > file1.txt
   abc@hecs-288034:~/passwd$ echo 这里是file2.txt > file2.txt
   abc@hecs-288034:~/passwd$ ls
   file1.txt  file2.txt
   abc@hecs-288034:~/passwd$ cat file1.txt file2.txt >file3.txt
   
   ```

### 

1. 使用命令从尾部开始显示 `bigfile.txt` 文件的内容。
   
   ```bash
   abc@hecs-288034:~$  tac bigfile.txt 
   
   ```

2. 尝试找出 `bigfile.txt` 文件的最后一行内容，要使用 `tac` 命令。
   
   ```bash
   abc@hecs-288034:~$ tac bigfile.txt | tail -n 1
   
   ```

3. 查看 `bigfile.txt` 文件的最后5行内容。
   
   ```bash
   tail -n 5 bigfile.txt 
   ```

4. 倒序查看 `bigfile.txt` 文件的最后5行内容。
   
   ```bash
   tac bigfile.txt |tail -n 5
   
   ```

### 

1. **操作题一**：使用 `more` 命令查看 `bigfile.txt` 文件的内容，并在查看过程中使用空格键翻页。
   
   ```bash
   more bigfile.txt   
   使用空格来翻页  向下翻页
   
   
   ```
2. **操作题二**：在 `more` 命令的查看过程中，如何使用回车键来逐行查看文件内容？
   
   ```bash
   more bigfile.txt 
   使用回车向下显示1行
   ```
3. **操作题三**：如何在 `more` 命令中搜索`bigfile.txt`特定字符串（例如 "error"）并跳转到下一个匹配项？
   
   ```bash
   abc@hecs-288034:~$ more bigfile.txt /error
   
   ```

### `less` 命令操作题

1. **操作题一**：使用 `less` 命令查看 `bigfile.txt` 文件，并快速定位到文件的末尾。
   
   ```bash
   less bigfile.txt   使用shift+g来定位到文件末尾
   ```
2. **操作题二**：在 `less` 命令中，如何向上和向下滚动文件内容？
   
   ```bash
   less bigfile.txt 
   1、可以使用方向键  可以向上向下显示1行
   2、使用enter键可以向下显示1行
   3、使用空格可以向下翻页
   4、使用PgUp向上，PgDn向下
   ```
3. **操作题三**：在 `less` 命令中，如何搜索`bigfile.txt`一个特定的函数名（例如 `def my_function`），并查看所有匹配项？
   
   ```bash
   less bigfile.txt / def my_function
   ```

### `head` 命令操作题

1. **操作题一**：使用 `head` 命令显示 `bigfile.txt` 文件的前5行内容。
   
   ```bash
   abc@hecs-288034:~$ head bigfile.txt -n 5
   
   ```
2. **操作题二**：将 `bigfile.txt` 的前20行内容保存到 `bigfile_20.txt` 文件中。
   
   ```bash
   abc@hecs-288034:~$ head bigfile.txt -n 20 > bigfile_20.txt
   
   ```
3. **操作题三**：如何结合 `head` 和 `grep` 命令来查找 `bigfile.txt` 文件中以 "A" 开头的前10行？
   
   ```bash
   abc@hecs-288034:~$ head -n 10 | grep "A" bigfile.txt 
   
   ```

### `tail` 命令操作题

1. **操作题一**：使用 `tail` 命令显示 `bigfile.txt` 文件的最后20行内容。
   
   ```bash
   abc@hecs-288034:~$ tail -n 20 bigfile.txt 
   ```
2. **操作题二**：如何实时跟踪一个正在写入的日志文件（如 `bigfile.txt`）的最后10行内容？
   
   ```bash
   tail -f -n 10 bigfile.txt 
   
   
   ```
3. **操作题三**：在 `tail` 命令中，如何反向显示文件的最后10行内容（即按从旧到新的顺序显示）
   
   ```bash
   abc@hecs-288034:~$ tail -n 10 bigfile.txt | tac
   ```

### 综合题

**综合题**：假设你有一个非常大的日志文件 `bigfile.txt`，你需要找出所有包含 "ERROR" 字符串的行，并查看这些行及其之前的两行内容。

```bash
 方法1、
 awk '/ERROR/{print prev2; print prev1; print; next}{prev2=prev1; prev1=$0}' bigfile.txt
 
 这个 awk 命令的工作原理如下：

当遇到包含 "ERROR" 的行时，它会打印出前两行（prev2 和 prev1）和当前行。
对于不包含 "ERROR" 的行，它会更新 prev2 和 prev1 变量以备后用。
 
 
 
 方使2、 用grep和head/tail组合（较为复杂且效率可能较低）
这种方法涉及到对每个匹配行使用 grep -B 2 来获取匹配行及其前两行，但由于 grep -B 会为每个匹配行都重复打印前两行，所以可能需要去重。这种方法在处理大文件时可能效率较低
 abc@hecs-288034:~$ grep -B 2 "ERROR" bigfile.txt | awk '!seen[$0]++'

 但是，请注意，如果两个错误行相隔少于两行，则使用这种方法可能会导致丢失一些上下文行，因为它们在与上一个错误相关的上下文中已经被打印出来了。此外，awk '!seen[$0]++' 用于去重，但如果上下文行完全相同且不属于连续的错误块，则它们也会被错误地去重。
 
abc@hecs-288034:~$ head | grep -B 2 "ERROR" bigfile.txt 
 
 
 
```


