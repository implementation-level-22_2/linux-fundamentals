```bash
## 进程管理和服务管理

##### 进程管理

- `ps`    `ps` aux

- `pstree`  安装`psmisc`

- top  原生实时查看进程相关  

- `htop` 第三方，自己安装  搜索 过滤 kill

- `pgrep` 通过服务名或程序名搜索进程号

  ​           `pgrep`服务名  `pidof`服务名

  ​           `pgrep` -u 用户名 名下所有进程号

  ​           `pgrep` -u 用户 服务名   

#### 前后台执行程序

###### `fg` 前台进行

- 默认前台
- 将后台程序切换前台   `fg`%+编号

###### `bg` 后台进行  

- 一开始直接&放最后面
- 将前台放到后台 ——> 先挂起（暂停）——>`ctrl`+z
- `bg`%+编号

###### 查看所有后台进程 jobs


##### 服务器管理

服务进程是指系统自动完成的，不需要和用户交互的程序的运行实例。

`systemctl` 选项  服务名

###### 选项：

- start 启动服务
- stop 关闭服务
- restart 重启服务，与服务当前状态无关
- reload 重新載入服务配置信息，并且不中断服务
- `condrestart` 重启服务，此选项会检查当前服务的运行状态，如果服务正在运行，它可以重启服务，否则`condrestart`无法重新启动服务
- status 查看服务的运行状态
- enable 设置服务开机自动启动
- disable 禁止服务开机自动启动
- is-enabled 检查服务在当前环境下是启用还是禁用
- list-unit-files --type=service 输出各个运行级别下，所有服务的启用和禁用情况
- `systemctl` daemon-reload 创建一个新服务文件，或者变更配置时使用

###### 管理系统电源

`systemctl poweroff` 系统关闭

`systemctl reboot`   重启系统进入

`systemctl suspend`    待机模式

`systemctl hibernate` 系统休眠

`systemctl hybrid-sleep` 混合休眠模式
\ No newline at end of file
```


