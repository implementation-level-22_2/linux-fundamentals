# 1.安装

```js
apt-get install net-tools //安装net-tools组件包
apt-get install vim  // vim编辑器
apt-get update // 更新软件库
apt-get install ssh   // 安装ssh,默认情况下，只有普通用户才可以登录这个服务器
```

# 2.修改ssh文件

```js
vim /etc/ssh/sshd_cofnig   //进入并修改ssh配置文件

//更改以下3行命令
port:22  //开启端口22
PermitRootLogin yes  //打开root用户登入
PasswordAuthentication yes  //打开密码验证

//修改完成按Esc键在按:wq保存退出
sudo systemctl restart ssh  //重启SSH服务，是root用户可以去掉sudo

```


