##  笔记

```
压缩文件命令：tar -czvf shi.tar.gz /home    表示使用tar命令把/home目录通过gzip格式进行打包压缩，并命名为shi.tar.gz
ps： -z:表示用gzip压缩或解压   -c：创建压缩文件 //这里的c为小写   
     -v:显示压缩或解压的过程  -f:目标文件名
     
// tar是Linux自带的文件打包压缩或解压命令，如需使用其他格式要自行下载 以rar格式为例：sudo apt-get install unrar
将打包后的压缩包文件指定解压到 /home/unc 中
先创建/home/unc目录（无需求可不创建）：mkdir /home/unc
解压文件命令：tar -xzvf shi.tar.gz -C /home/unc
ps： -x:解开压缩文件  -C:指定解压到的目录  //这里的C为大写
从服务器复制文件到本地：
scp root@×××.×××.×××.×××:/home/unc/shi.tar.gz C:\Users\27817\Desktop

root@×××.×××.×××.×××   root是目标服务器（有你需要拷贝文件的服务器）的用户名，×××.×××.×××.×××是IP地址，如192.168.1.100，后面紧跟的：不要忘记，/home/unc/shi.tar.gz是目标服务器中你要拷贝文件的地址，接一个空格，后面的C:\Users\27817\Desktop是本地接收文件的地址。
在一个文件夹中依次建立文件夹
创建一个文件夹 
# mkdir folder
创建多个文件夹 
# mkdir folder1 folder2 folder3
创建多级文件夹
# mkdir -p parentfolder/childfolder   // -p 参数会确保 "parent" 文件夹存在，即使它不存在也会自动创建
```