# 文件及目录的操作

## ls 列出目录内容

ls 列出当前目录内容

ls 路径/文件夹 列出指定（路径）文件夹的内容

ls -d 路径/文件夹 列出指定（路径）文件夹本身的信息

ls -a all 所有文件目录包括隐藏

ls -l 长格式列出 包括权限、所有者、组、大小、创建日期等信息

ls -h 件大小以KB、MB\GB等单位

ls -t 修改时间排序 默认是大到小

ls -r 反向排序

ls -S 文件大小排序

ls -R 递归列出当前目录及其子目录下的所有文件和目录

## cd改变当前目录

cd 目录名 进入某个目录

cd .. 返回上个目录

cd ~ /cd  /cd /home/用户名 返回家目录

cd - 返回上次所在目录

## pwd显示当前工作目录的绝对路径

pwd 显示当前工作目录的绝对路径

dir1 = $(pwd)   将当前工作目录路径保存到变量中（在Shell脚本中常用）

echo $dir1 输出当前工作目录的路径

## mkdir创建新目录

mkdir 目录名 创建一个新目录

mkdir 路径/目录名 在指定路径下新建目录

mkdir -p 父目录/新目录  创建多个目录，包括父目录（如果不存在），避免保存

mkdir -m 权限模式 目录名  创建具有特定权限的目录  mkdir -m 777 mydir

mkdir -p /tmp/新目录  创建临时目录,不长期保留,与用户文件隔离，避免暴露用户   

## rmdir删除空目录

rmdir 目录名 只能删除空的目录

## rm删除文件或目录

rm -i 文件名 删除指定文件 会询问是否删除

rm -f 文件名 强制删除 不会询问

rm a b 空格分隔 删除多个文件

rm -f a* 删除a开头的文件 *通配符

rm -f .* 删除隐藏文件

rm -r 目录名 递归地删除目录及其子目录和文件 会询问 加f不会询问 加v显示删除步骤

rm -d 目录名 只删除空目录

# 练习

**题目**：

您有一个初始目录结构，其中包含名为 `development` 的目录。在 `development` 目录下，您需要先创建名为 `old_code` 的子目录，并在 `old_code` 中创建一些 `.py`、`.pyc` 和 `.pyo` 文件（模拟已有的项目结构）。之后，您需要对这个项目进行整理，确保每个部分的文件都在正确的位置，并删除不再需要的文件和目录。以下是您需要完成的具体任务：

1. 使用 `cd` 命令进入 `development` 目录（假设您已经在包含 `development` 目录的父目录中）。

   ```bash
   cd development/
   ```

2. 在 `development` 目录下创建名为 `old_code` 的子目录。

   ```bash
    mkdir old_code
   ```

3. 在 `old_code` 目录下创建以下文件和目录结构：

   - `module1/script1.py`
   - `module1/script1.pyc`
   - `module1/script2.py`
   - `module1/script2.pyo`
   - `module2/script3.py`
   - `module2/script3.pyc`

   ```bash
   cd old_code/
   mkdir module1 module2
   touch script1.py script1.pyc script2.py script2.pyo
   cd ../module2
   touch script3.py script3.pyc
   ```

4. 使用 `ls -l` 命令查看 `old_code` 目录下的所有文件和目录。

   ```bash
   ls -l ..
   total 8
   drwxr-xr-x 2 root root 4096 May 23 10:38 module1
   drwxr-xr-x 2 root root 4096 May 23 10:39 module2
   
   ls -lR ..
   ..:
   total 8
   drwxr-xr-x 2 root root 4096 May 23 10:38 module1
   drwxr-xr-x 2 root root 4096 May 23 10:39 module2
   
   ../module1:
   total 0
   -rw-r--r-- 1 root root 0 May 23 10:38 script1.py
   -rw-r--r-- 1 root root 0 May 23 10:38 script1.pyc
   -rw-r--r-- 1 root root 0 May 23 10:38 script2.py
   -rw-r--r-- 1 root root 0 May 23 10:38 script2.pyo
   
   ../module2:
   total 0
   -rw-r--r-- 1 root root 0 May 23 10:39 script3.py
   -rw-r--r-- 1 root root 0 May 23 10:39 script3.pyc
   ```

5. 手动删除 `old_code` 目录中所有的 `.pyc` 和 `.pyo` 文件。

   ```bash
   cd module1
   rm  *.pyc *.pyo
   cd ../module2
   rm  *.pyc
   ```

6. 创建一个新的目录 `source_code`，并使用 `cp` 命令手动复制 `old_code` 目录中所有 `.py` 文件到 `source_code` 目录中，同时保持原始目录结构。

   ```bash
   cd ../..
   mkdir source_code
   cp -r old_code/module* source_code/
   ```

7. 使用 `mkdir` 命令在 `source_code` 目录下创建两个新的子目录 `tests` 和 `docs`。

   ```bash
   mkdir tests docs
   ```

8. 使用 `touch` 命令在 `source_code/docs` 目录下创建两个空文件 `architecture.md` 和 `requirements.md`。

   ```bash
   cd docs
   touch architecture.md requirements.md
   ```

9. 回到 `development` 目录的根目录，使用 `ls -R` 命令递归地列出整个 `development` 目录的结构。

   ```bash
   root@hecs-157832:/home# ls -R development/
   development/:
   old_code  source_code
   
   development/old_code:
   module1  module2
   
   development/old_code/module1:
   script1.py  script2.py
   
   development/old_code/module2:
   script3.py
   
   development/source_code:
   docs  module1  module2  requirements.md  tests
   
   development/source_code/docs:
   architecture.md  requirements.md
   
   development/source_code/module1:
   script1.py  script2.py
   
   development/source_code/module2:
   script3.py
   
   development/source_code/tests:
   
   ```

10. ls清理工作：删除整个 `old_code` 目录（包括其中的所有文件和子目录）。

    ```bash
    cd development/
    rm -rf old_code/
    ```

11. （额外任务）假设您发现 `source_code/module1` 目录下有一个名为 `unused_script.py` 的文件不再需要（尽管在这个示例中我们没有创建它，但假设它存在），请使用 `rm` 命令删除它。

```bash
rm -f module1/unused_script.py
```

# 作业

假设您刚刚登录到一个Linux系统，并位于您的家目录（`~`）下。您需要完成以下一系列复杂的操作来组织和清理您的文件和目录。请按照顺序执行，并给出相应的命令。

1. **创建测试文件**：在家目录下创建三个文本文件，分别命名为`.hidden.txt`（隐藏文件）、`visible1.txt`和`visible2.txt`。

   ```bash
   touch .hidden.txt visible1.txt visible2.txt
   ```

2. **列出文件和目录**：列出家目录（`~`）下的所有文件和目录，包括隐藏文件，并查看其详细权限和属性。

   ```bash
   ls -al
   total 8
   drwxr-xr-x 2 root root 4096 May 23 20:55 .
   drwxr-xr-x 5 root root 4096 May 23 20:54 ..
   -rw-r--r-- 1 root root    0 May 23 20:55 .hidden.txt
   -rw-r--r-- 1 root root    0 May 23 20:55 visible1.txt
   -rw-r--r-- 1 root root    0 May 23 20:55 visible2.txt
   
   ```

3. **创建工作区**：创建一个新的目录`work_area`，并在其中创建三个子目录：`project_a`、`project_b`和`docs`。

   ```bash
   mkdir work_area
   cd work_area/
   mkdir project_a project_b docs
   ```

4. **移动文本文件**：将家目录下的所有`.txt`文件移动到`work_area/docs`目录中，并确保这些文件在移动后仍然是隐藏的（如果它们是隐藏的）。

   ```bash
   mv .*.txt work_area/docs/
   ```

5. **创建新文件**：在`work_area/project_a`目录下创建一个新的文本文件`notes.txt`，并添加一些内容（例如：`echo "Initial notes for project A" > work_area/project_a/notes.txt`）。

   ```bash
   cd work_area/project_a
   touch notes.txt
   echo "Initial notes for project A" >notes.txt
   ```

6. **复制目录**：递归地复制`work_area/project_a`目录到`work_area/project_b`，并命名为`project_a_backup`。

   ```bash
   cd ../../..
   cp -r work_area/project_a work_area/project_b/project_a_backup
   ```

7. **列出文件并按大小排序**：列出`work_area/docs`目录下的所有文件，并按文件大小降序排列。

   ```bash
   ls -al -S work_area/docs/
   total 8
   drwxr-xr-x 2 root root 4096 May 23 21:20 .
   drwxr-xr-x 5 root root 4096 May 23 21:00 ..
   -rw-r--r-- 1 root root    0 May 23 21:15 .hidden.txt
   -rw-r--r-- 1 root root    0 May 23 21:08 visible1.txt
   -rw-r--r-- 1 root root    0 May 23 21:08 visible2.txt
   ```

8. **删除所有文件**：删除`work_area/docs`目录下所有文件。

   ```bash
   cd work_area/docs/
   rm -f *
   ```

9. **删除目录**：假设您不再需要`work_area/project_b`目录及其所有内容，请递归地强制删除它。

   ```bash
   rm -rf work_area/project_b
   ```

10. **清理空目录**：清理`work_area`目录，删除其中所有的空目录（注意：不要删除非空的目录）。

    ```bash
    cd work_area/
    find . -type d -empty -delete
    ```

11. **创建别名**：回到您的家目录，并创建一个别名`llh`，该别名能够列出当前目录下文件和目录的长格式列表，并以人类可读的格式显示文件大小（类似于`ls -lh`命令）。

```bash
cd 523
vim .bashrc
alias llh='ls -lah'
source .bashrc
llh
total 4.0K
total 4.0K
total 16K
drwxr-xr-x 3 root root 4.0K May 23 22:20 .
drwxr-xr-x 5 root root 4.0K May 23 20:54 ..
-rw-r--r-- 1 root root   20 May 23 22:20 .bashrc
drwxr-xr-x 4 root root 4.0K May 23 22:06 work_area
```

