# 周期任务与环境变量

## 周期任务

Cron任务的配置文件:

用户级Cron任务：/var/spool/cron/crontabs/username

系统级Cron任务：/etc/crontab和/etc/cron.d/

周期性任务目录：

/etc/cron.hourly/

/etc/cron.daily/

/etc/cron.weekly/

/etc/cron.monthly/

**编辑Cron任务**：crontab -e  

**查看Cron任务**：crontab -l

**删除Cron任务**：crontab -r

语法：* * * * * 

分钟 1-60 

时 1-24

天 1-31

月 1-12

周 0-6

符号：

| *      | 每一                               |
| ------ | ---------------------------------- |
| ，     | 指定值 2，6，8，4，5               |
| -      | 范围 1-5                           |
| /      | 间隔 */10                          |
| L 最后 | 月代表最后一天 周代表最后一个星期x |
| ?      | 任意                               |
| w      | 工作日                             |

## 环境变量

类型：

用户级别环境变量：

~/.bashrc、~/.profile

系统级别环境变量：

 /etc/environment、/etc/profile、/etc/profile.d

加载顺序：

**系统启动加载**：/etc/environment ,一开源就加载，初始化 

配置文件的理论加载顺序通常是 `/etc/environment` > `/etc/profile` > `~/.profile` > `~/.bashrc`。

设置环境变量：

临时：变量名=值 环境变量是全局的，整个环境有效

永久：

编辑用户的 `~/.bashrc` 或 `~/.profile` 文件，并添加如下行：export MY_VAR="value"

常用：

打印所有环境变量：

printenv#或env

**系统级**

* `PATH` 可执行文件的搜索路径
* `LANG` 定义系统的语言环境和字符集， export LANG=zh_CN.utf8，编辑文件不乱码
* `LANGUAGE` 用于设置消息语言的优先级 ，提示语言为中文
* `EDITOR` 默认文本编辑器 editor

## 练习

###  周期任务练习

执行在家目录touch a.txt

1. 每天3:00执行一次

   ```bash
   0 3 * * * 
   ```

2. 每周六2:00执行 

   ```bash
   0 2 * * 6
   ```

3. 每周六1:05执行

   ```bash
   5 1 * * 6
   ```

4. 每周六1:25执行 

   ```bash
   25 1 * * 6
   ```

5. 每天8：40执行 

   ```bash
   40 8 * * *
   ```

6. 每天3：50执行 

   ```bash
   50 3 * * *
   ```

7. 每周一到周五的3：40执行 

   ```bash
   40 3 * * 1-5
   ```

8. 每周一到周五的3：41开始，每10分钟执行一次 

   ```bash
   41/10 3 * * 1-5
   ```

9. 每天的10：31开始，每2小时执行一次

   ```bash
   31 10/2 * * *
   ```

10. 每周一到周三的9：30执行一次

    ```bash
    30 9 * * 1-3

11. 每周一到周五的8：00，每周一到周五的9：00执行一次

    ```bash
    0 8,9 * * 1-5

12. 每天的23：45分执行一次

    ```bash
    45 23 * * *

13. 每周三的23：45分执行一次

    ```bash
    45 23 * * 3
    ```

14. 每周一到周五的9：25到11：35、13：00到15：00之间，每隔10分钟执行一次

    ```bash
    25-35/10 9-11 * 1-5
    */10 13-15 * * 1-5
    ```

15. 每周一到周五的8：30、8：50、9：30、10：00、10：30、11：00、11：30、13：30、14：00、14：30、5：00分别执行一次

    ```bash
    30 8-11,13,14 * * 1-5
    50 8 * * 1-5
    0 10,11,14 * * 1-5
    ```

16. 每天16：00、10：00执行一次

    ```bash
    0 10,16 * * *
    ```

17. 每天8：10、16：00、21：00分别执行一次

    ```bash
    10 8 * * *
    0 16,21 * * *
    ```

18. 每天7：47、8：00分别执行一次

    ```bash
    47 7 * * *
    0 8 * * *
    ```

### 练习题 1: 显示当前所有的环境变量

* 使用`printenv`或`env`命令来显示所有的环境变量。


```bash
root@hecs-157832:~# printenv
SHELL=/bin/bash
HISTSIZE=10000
HISTTIMEFORMAT=%F %T root 
PWD=/root
LOGNAME=root
XDG_SESSION_TYPE=tty
MOTD_SHOWN=pam
HOME=/root
LANG=en_US.UTF-8
SSH_CONNECTION=112.5.195.104 5280 192.168.0.242 22
XDG_SESSION_CLASS=user
TERM=xterm
USER=root
SHLVL=1
XDG_SESSION_ID=1039
XDG_RUNTIME_DIR=/run/user/0
SSH_CLIENT=112.5.195.104 5280 22
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
SSH_TTY=/dev/pts/0
_=/usr/bin/printenv
```

### 练习题 2: 显示`HOME`环境变量的值

* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。


```bash
root@hecs-157832:~# echo $HOME
/root
```

### 练习题 3: 临时设置一个新的环境变量

* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。


```bash
root@hecs-157832:~# MY_AGE=18
```

### 练习题 4: 显示新设置的环境变量

* 使用`echo`命令来显示`MY_AGE`的值。


```bash
root@hecs-157832:~# echo $MY_AGE
18
```

### 练习题 5: 在新的shell会话中检查环境变量

* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？

  **没有出现任何东西 因为是局部变量**

### 练习题 6: 修改`PATH`环境变量

* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置


```bash
vim .prodile
export PATH=$PATH:$HOME
```

将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。

```bash
export PATH=/tmp:$PATH
```

### 练习题 7: 验证`PATH`的修改

* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。


```bash
echo $PATH
```

### 练习题 8: 永久设置环境变量

* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。

例如，对于bash shell，你可以使用：


```bash
echo 'export MY_NAME="奥德彪"' >> ~/.bashrc
```

如何让`MY_NAME`生效，并验证

```bash
source ~/.bashrc
echo $MY_NAME
```

### 练习题 9: 清理

* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。


```bash
unset MY_AGE
export PATH=$(echo $PATH | sed -e 's|:'"$HOME"'||' -e 's|/tmp:||')
```

### 练习题 10: 修改默认器

* 使用`EDITOR`变量，修改你默认的编辑器为nano。


```bash
export EDITOR=nano
```

### 练习题 11: 修改语言

* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。

```bash
export LANG=zh_CN.UTF-8
```

- 使用`LANGUAGE`变量，让你的命令提示为中文

```bash
export LANGUAGE=zh_CN.UTF-8
```

