##  目录结构

```js
Bin: binary 二进制.存储的都是二进制文件,文件可以被运行
Dev: 存放的是外接设备,例如u盘,光盘.是不能被直接使用的,需要挂载(分配盘符)
Etc:主要存储一些配置文件
Home:除root用户以外的其他用户的家目录,类似windows下的User/用户目录
Proc:process,表示进程,存储的是Linux运行时的进程
Root:root用户的家目录
Sbin:super binary,存储一些必须有super权限的用户才能执行的二进制文件
Tmp:表示"临时",存储系统运行时产生的临时文件
Usr:存放的是用户自己安装的软件,类似于windows下的program files
Var:存放的程序/系统的日志文件
Mnt:当外接设备需要挂载时,就需要挂载到该目录
```