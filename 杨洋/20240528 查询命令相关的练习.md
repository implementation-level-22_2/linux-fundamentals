### 操作题

1. **查找当前目录及其子目录中所有扩展名为 `.log` 的文件**：
   ```bash
   pxx@hecs-18032:~$ find ./ -name "*.log"
   
   ```
   
2. **在 `/var/logs` 目录及其子目录中查找所有包含关键字 `error` 的文件，并显示匹配的行**：
   ```bash
   pxx@hecs-18032:~$ grep -rn "error" var/logs/
   
```
   
3. **在 `/home/user/docs` 目录中查找文件名包含 `report` 的所有文件**：
   ```bash
   pxx@hecs-18032:~$ grep -r "report" user/docs
   
```
   
4. **查找 `/etc` 目录中最近7天内修改过的文件**：
   ```bash
   pxx@hecs-18032:~$ find var/etc/ -type f -mtime -7
   
```
   
5. **显示 `/usr/bin` 目录中名为 `python` 的可执行文件的路径**：
   ```bash
   pxx@hecs-18032:~$ find user/bin/ -type f -name "python"
   ```

6. **查找系统中名为 `ls` 的命令及其手册页位置**：
   ```bash
   pxx@hecs-18032:~$ whereis ls
   
```
   
7. **查找当前目录中包含关键字 `TODO` 的所有文件，并显示匹配的行和文件名**：
   
   ```bash
   pxx@hecs-18032:~$ grep -rnl "TODO" ./

   ```
   
8. **在 `/home/user/projects` 目录中查找所有包含关键字 `function` 的 `.js` 文件**：
   ```bash
   pxx@hecs-18032:~$ grep -r "function" user/projects/*
   
```
   
9. **查找并显示当前目录及其子目录中所有空文件**：
   ```bash
   pxx@hecs-18032:~$ find ./ -empty -type f
   ```

10. **在 `/var/www` 目录中查找包含关键字 `database` 的所有文件，并只显示文件名**：
    ```bash
    pxx@hecs-18032:~$ grep -r "database" var/www/ 
    ```

### 综合操作题

**综合操作题：**

假设你在一个名为 `/home/user/workspace` 的目录中工作。你需要完成以下任务：

1. 查找该目录中所有扩展名为 `.conf` 的文件。
2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行。
3. 将包含关键字 `server` 的文件名和匹配的行保存到一个名为 `server_lines.txt` 的文件中。

**预期命令步骤：**

1. 查找所有扩展名为 `.conf` 的文件：
   ```bash
   pxx@hecs-18032:~$ find user/workspace/ -type f -name "*.conf"
   
```
   
2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行：
   ```bash
   pxx@hecs-18032:~$ grep -r "server" user/workspace/
   
```
   
3. 将结果保存到 `server_lines.txt` 文件中：
   ```bash
   pxx@hecs-18032:~$ grep -r "server" user/workspace/ >>server_lines.txt
   
   ```

通过这套操作题和综合操作题，你可以全面地了解和应用Linux系统中与文件和内容查询相关的常用命令。