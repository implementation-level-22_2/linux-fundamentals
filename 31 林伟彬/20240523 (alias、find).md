

# 笔记：

### find：查找文件

-type  文件类型
d      目录
-empty 空目录
-exec  允许后面执行一个命令
{}     将会被匹配到的文件名代理 
\;     命令结束

```js
// 按文件名查找文件
find 起始路径 -name "被查找的文件名" 
例：find / -name “test” //查找文件名叫做：test的文件，从根目录开始搜索

// 按文件大小查找文件
find 起始路径 -size [+ -] n [kMG]
例：find / -size -10k //查找小于10KB的文件
// +、- 表示大于和小于
// n表示大小数字
// kMG表示大小单位，k(小写字母)表示kb，M表示MB，G表示GB
--------------------
find ~/work_area -type d -empty -exec rm -r {} \;
// find   查找文件或目录
// -type  文件类型
// d      目录
// -empty 空目录
// -exec  允许后面执行一个命令
// {}     将会被匹配到的文件名代理 
// \;     命令结束
```

### alias：创建别名

语　　法：alias[别名]=[指令名称]

```js
alias llh='ls -l'
//语　　法：alias[别名]=[指令名称]
```

# 作业：

假设您刚刚登录到一个Linux系统，并位于您的家目录（`~`）下。您需要完成以下一系列复杂的操作来组织和清理您的文件和目录。请按照顺序执行，并给出相应的命令。

1. #### **创建测试文件**：在家目录下创建三个文本文件，分别命名为`.hidden.txt`（隐藏文件）、`visible1.txt`和`visible2.txt`。

   ```js
   touch ~/.hidden.txt
   touch ~/visible1.txt
   touch ~/visible2.txt
   ```

2. **列出文件和目录**：列出家目录（`~`）下的所有文件和目录，包括隐藏文件，并查看其详细权限和属性。

   ```js
   ls -lah
   ```

3. **创建工作区**：创建一个新的目录`work_area`，并在其中创建三个子目录：`project_a`、`project_b`和`docs`。

   ```js
   mkdir -p work_area/project_a work_area/project_b work_area/docs
   ```

4. **移动文本文件**：将家目录下的所有`.txt`文件移动到`work_area/docs`目录中，并确保这些文件在移动后仍然是隐藏的（如果它们是隐藏的）。

   ```
   mv ~/*.txt  ~/work_area/docs/
   ```

5. **创建新文件**：在`work_area/project_a`目录下创建一个新的文本文件`notes.txt`，并添加一些内容（例如：`echo "Initial notes for project A" > work_area/project_a/notes.txt`）。

   ```js
   touch work_area/project_a/nots.txt | echo "Initial notes for project A" > work_area/project_a/nots.txt //利用管道符|和echo追加内容
   ```

6. **复制目录**：递归地复制`work_area/project_a`目录到`work_area/project_b`，并命名为`project_a_backup`。

   ```js
   cp -r ~/work_area/project_a/ ~/work_area/project_b/project_a_backup
   ```

7. **列出文件并按大小排序**：列出`work_area/docs`目录下的所有文件，并按文件大小降序排列。

   ```js
   ls -lhS work_area/docs^
   //  -l   使用详细格式列表。
   //  -S   用文件和目录的大小排序。
   //  -h或--human-readable   用"K","M","G"来显示文件和目录的大小。
   ```

8. **删除所有文件**：删除`work_area/docs`目录下所有文件。

   ```
   rm ~/work_area/docs/*
   ```

9. **删除目录**：假设您不再需要`work_area/project_b`目录及其所有内容，请递归地强制删除它。

   ```js
   rm -rf ~/work_area/project_b
   ```

10. **清理空目录**：清理`work_area`目录，删除其中所有的空目录（注意：不要删除非空的目录）。

    ```js
    find ~/work_area -type d -empty -exec rm -r {} \;
    // find   查找文件或目录
    // -type  文件类型
    // d      目录
    // -empty 空目录
    // -exec  允许后面执行一个命令
    // {}     将会被匹配到的文件名代理 
    // \;     命令结束
    ```

11. **创建别名**：回到您的家目录，并创建一个别名`llh`，该别名能够列出当前目录下文件和目录的长格式列表，并以人类可读的格式显示文件大小（类似于`ls -lh`命令）。

    ```js
    alias llh='ls -l'
    //语　　法：alias[别名]=[指令名称]
    ```

    