# Linux基础命令

# 1、ls ：列出目录下的内容

```java
ls  [-a -l -h ] [Linux路径] 
//-a 选项，表示：all的意思，即列出全部文件（包含隐藏的文件/文件夹）

//-l 选项，表示：以列表（竖向排列）的形式展示内容，并展示更多信息

//-h 表示以易于阅读的形式，列出文件大小，如K、M、G

//-h 选项必须要搭配 -l 一起使用
```

# 2、cd / pwd

#### ①cd：切换工作目录

```js
cd [Linux路径] //cd命令无需选项，只有参数，标示要切换到哪个目录下
```

#### ②pwd：查看当前工作目录

```js
pwd  //无选项，无参数，直接输入pwd即可
```

### 相对路径、绝对路径和特殊路径符：

```java
cd /home/itheima/Desktop //绝对路径写法

cd Desktop			 	//相对路径写法
```

# 3、mkdir

```java
mkdir [-p] Linux路径  //-p选项可选填，表示自动创建不存在的父目录，适用于创建连续多层级的目录
```

# 4、touch、cat、more

#### ①touch ：创建文件

```js
touch Linux路径 //touch命令无选项，参数必填
```

#### ②cat命令 查看文件内容

```js
cat Linux路径 //cat同样没有选项，只有必填参数
```

#### ③more命令查看文件内容

```js
more linux路径 //more支持翻页，如果文件内容过多，可以一页页的展示
			 //查看的过程中，通过空格翻页
	          //通过q退出查看
```

# 5、cp、mv、rm

#### ①cp ：命令复制文件文件夹

```js
cp [-r] 参数1 参数2
// -r选项，可选，用于复制 [文件夹] 使用，表示递归,如需要复制文件夹则加 -r
// 参数1，Linux路径，表示被复制的文件或文件夹
// 参数2，Linux路径，表示要复制去的地方

```

#### ②mv：移动、改名文件或文件夹

```js
mv 参数1 参数2
例：mv test.txt ziwen/ //移动文件
例：mv test.txt test2.txt //改名
// 参数1，Linux路径，表示被移动的文件或文件夹
// 参数2，Linux路径，表示要移动去的地方，如果目标不存在，则进行改名，确保目标存在

```

#### ③r m:删除文件、文件夹

```js
rm [-r -f] test.txt // 删除文件,如果要删除文件夹必须加-r
rm test.txt test1.txt test2.txt // 删除多个文件,用空格隔开
// -r选项，可选，文件夹删除
// -f选项，可选，用于强制删除（不提示，一般用于root用户）
```

# 6、which、find

#### ①which: 查看所使用的一系列命令的程序文件存放在哪里

```js
which pwd // 查看pwd命令存放在哪里
```

#### ②find：查找文件

-type  文件类型
d      目录
-empty 空目录
-exec  允许后面执行一个命令
{}     将会被匹配到的文件名代理 
\;     命令结束

```js
// 按文件名查找文件
find 起始路径 -name "被查找的文件名" 
例：find / -name “test” //查找文件名叫做：test的文件，从根目录开始搜索

// 按文件大小查找文件
find 起始路径 -size [+ -] n [kMG]
例：find / -size -10k //查找小于10KB的文件
// +、- 表示大于和小于
// n表示大小数字
// kMG表示大小单位，k(小写字母)表示kb，M表示MB，G表示GB
--------------------
find ~/work_area -type d -empty -exec rm -r {} \;
// find   查找文件或目录
// -type  文件类型
// d      目录
// -empty 空目录
// -exec  允许后面执行一个命令
// {}     将会被匹配到的文件名代理 
// \;     命令结束
```

# 7、grep、wc：和管道符

#### ①grep：从文件中通过关键字过滤文件行

```js
grep [-n] "关键字" 文件路径
// 选项-n，可选，表示在结果中显示匹配的行的行号。
// 参数，关键字，必填，表示过滤的关键字，建议使用””将关键字包围起来
// 参数，文件路径，必填，表示要过滤内容的文件路径，可作为管道符的输入
```

#### ②wc命令：命令统计文件的行数、单词数量、字节数、字符数等

```js
wc [-c -m -l -w] 文件路径
// 不带选项默认统计：行数、单词数、字节数
//- c字节数、-m字符数、-l行数、-w单词数
// 参数，被统计的文件路径，可作为管道符的输入
```

#### ③管道符：|   将管道符左边命令的结果，作为右边命令的输入

# 8、echo、tail：和重定向符

#### echo：在命令行内输出指定内容

```js
echo "输出的内容"  //在命令行内输出指定内容

``反引号符 
被 `` 包围的内容，会被作为命令执行，而非普通字符
例：echo `pwd` 
```

#### tail：查看文件尾部内容，并可以持续跟踪

```js
tail [-f -num] Linux路径 
例：tail 5 test.txt
// -f：持续跟踪，
// -num：启动的时候查看尾部多少行，默认10
// Linux路径，表示被查看的文件
```

#### 重定向符：

```js
// >，将左侧命令的结果，覆盖写入到符号右侧指定的文件中
例：echo “Hello Linux” > itheima.txt 
例：echo “Hello itheima” > itheima.txt，再次执行，覆盖新内容
// >>，将左侧命令的结果，追加写入到符号右侧指定的文件中
例：echo “Hello itcast” >> itheima.txt，再次执行，使用>>追加新内容
```



# 9、vi/vim编辑器

#### vi\vim编辑器的三种工作模式

![image-20240522155720012](https://gitee.com/weibina/computer-fundamentals/raw/master/img/image-20240522155720012.png)

命令模式：

![image-20240522155734748](https://gitee.com/weibina/computer-fundamentals/raw/master/img/image-20240522155734748.png)



底线命令模式：

![image-20240522155840456](https://gitee.com/weibina/computer-fundamentals/raw/master/img/image-20240522155840456.png)

# 10、创建别名



```js
alias llh='ls -l'
//语　　法：alias[别名]=[指令名称]
```

