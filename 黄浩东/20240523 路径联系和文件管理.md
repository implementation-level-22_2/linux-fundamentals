# 笔记

基础命令

```
pwd 
ls -hl
```

# 作业

假设您刚刚登录到一个Linux系统，并位于您的家目录（`~`）下。您需要完成以下一系列复杂的操作来组织和清理您的文件和目录。请按照顺序执行，并给出相应的命令。

1. **创建测试文件**：在家目录下创建三个文本文件，分别命名为`.hidden.txt`（隐藏文件）、`visible1.txt`和`visible2.txt`。

   ```
   cd /home
   sudo touch .hidden.txt visible1.txt visible2.txt
   ```

2. **列出文件和目录**：列出家目录（`~`）下的所有文件和目录，包括隐藏文件，并查看其详细权限和属性。

   ```
   ls -al
   ```

3. **创建工作区**：创建一个新的目录`work_area`，并在其中创建三个子目录：`project_a`、`project_b`和`docs`。

   ```
   sudo mkdir work_area
   cd work_area
   sudo mkdir project_a project_b docs
   ```

4. **移动文本文件**：将家目录下的所有`.txt`文件移动到`work_area/docs`目录中，并确保这些文件在移动后仍然是隐藏的（如果它们是隐藏的）。

   ```
   cd ..
   mv *.txt  ./work_area/docs
   ```

5. **创建新文件**：在`work_area/project_a`目录下创建一个新的文本文件`notes.txt`，并添加一些内容（例如：`echo "Initial notes for project A" > work_area/project_a/notes.txt`）。

   ```
   cd ./work_area/poject_a
   vim notes.txt
   ```

6. **复制目录**：递归地复制`work_area/project_a`目录到`work_area/project_b`，并命名为`project_a_backup`。

   ```
   cd ..
   cp -r ./project_a project_a_backup ./project_b
   ```

7. **列出文件并按大小排序**：列出`work_area/docs`目录下的所有文件，并按文件大小降序排列。

   ```
   ls -s ./docs
   ```

8. **删除所有文件**：删除`work_area/docs`目录下所有文件。

   ```
   rm -rf ./work_area/docs
   ```

9. **删除目录**：假设您不再需要`work_area/project_b`目录及其所有内容，请递归地强制删除它。

   ```
   rm -rf ./work_area/project_b
   ```

10. **清理空目录**：清理`work_area`目录，删除其中所有的空目录（注意：不要删除非空的目录）。

    ```
    rm -d ./work_area
    ```

11. **创建别名**：回到您的家目录，并创建一个别名`llh`，该别名能够列出当前目录下文件和目录的长格式列表，并以人类可读的格式显示文件大小（类似于`ls -lh`命令）。

    ```
    alias llh = 'ls -lh'
    ```