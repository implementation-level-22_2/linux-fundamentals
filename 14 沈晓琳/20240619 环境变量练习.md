### 练习题 1: 显示当前所有的环境变量

* 使用`printenv`或`env`命令来显示所有的环境变量。


```bash
printenv
```
### 练习题 2: 显示`HOME`环境变量的值

* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。


```bash
echo $HOME
```
### 练习题 3: 临时设置一个新的环境变量

* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。


```bash
export MY_AGE=18
```
### 练习题 4: 显示新设置的环境变量

* 使用`echo`命令来显示`MY_AGE`的值。


```bash
echo $MY_AGE
```
### 练习题 5: 在新的shell会话中检查环境变量

* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？

什么都没有，因为在新的 shell 会话中，之前设置的环境变量不会被继承

### 练习题 6: 修改`PATH`环境变量

* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置


```bash
export PATH="$PATH:$HOME"
```
将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。

```bash
export PATH="/tmp:$PATH"
```

### 练习题 7: 验证`PATH`的修改

* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。


```bash
echo $PATH
```
### 练习题 8: 永久设置环境变量

* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。

例如，对于bash shell，你可以使用：


```bash
echo 'export MY_NAME="奥德彪"' >> ~/.bashrc
```
如何让`MY_NAME`生效，并验证

```bash
source ~/.bashrc
echo $MY_NAME
```

### 练习题 9: 清理

* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。


```bash
unset MY_AGE

```
### 练习题 10: 修改默认器

* 使用`EDITOR`变量，修改你默认的编辑器为nano。


```bash
export EDITOR=nano
```

### 练习题 11: 修改语言

* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。

```bash
export LANG=zh_CN.UTF-8
```

- 使用`LANGUAGE`变量，让你的命令提示为中文

```bash
export LANGUAGE=zh_CN:zh
```

