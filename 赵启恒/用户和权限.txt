用户与文件权限

## 用户

### 用户管理

超级用户root：0 0 0 不受权限的限制

普通用户从1000开始

#### 添加用户：

低级useradd 用户名 

-m 生成同名家目录

-d 指定目录

-s /bin/shell 指定生成shell

-g 用户组原生组 -G附加组

高级 adduser

#### 删除用户：

userdel 用户名

-r 删除用户及相关的目录

#### 查询：

找普通用户：

cat /etc/passwd 

查找某个用户：

id 用户名

#### 修改用户：

usermod -aG 组名 用户名 追加组

#### 设置密码：

passwd 用户名

passwd -l 用户名 锁定用户

passwd -S 用户名 查看用户状态

### 用户组管理

#### 添加用户到组：

groupadd 组名 

-g指定组号 -o可以与原有组同名

#### 查询组：

cat /etc/group

#### 删除组：

groupdel 组名

#### 修改：

groupmod 可以修改组id

## 文件权限

### 文件类型

10个字符中 第一位表示：

目录 d

普通文件 -

链接 l

其他 b c s p

### 基本权限

r 读 4 文件只打开内容，目录只能浏览极少信息

w 写 2 修改 删除 移动 复制

x 执行 1 执行文件 进入目录

在原有的权限上增加新权限 +

在原有的权限上减少权限 -

覆盖权限 =

### 权限修改

2-4 文件所有者 u

5-7 所属用户组 g

8-10 其他人 o

所有人 a



chmod o+x a.txt 

其他人增加a.txt的执行权限

chmod 421 a.txt

a.txt中用户所有者u具有读权限，所属组g具有写权限，其他人o具有执行权限。



chown 用户名 目录/文件 修改目录/文件的所有者

chagp 组名 文件/目录 修改文件/目录的所属组

# 作业

1. 创建/guanli 目录，在/guanli下创建zonghe 和 jishu 两个目录（一条命令）

   ```bash
   sx@hecs-157832:~/0614$ mkdir -p guanli/zonghe guanli/jishu
   ```

2. 添加组帐号zonghe、caiwu、jishu，GID号分别设置为2001、2002、2003

   ```bash
   root@hecs-157832:~# groupadd zonghe -g 2001
   root@hecs-157832:~# groupadd caiwu -g 2002
   root@hecs-157832:~# groupadd jishu -g 2003
   ```

   

3. 创建jerry、kylin、tsengia、obama用户，其中的kylin用户帐号在2020年12月30日后失效

   ```bash
   root@hecs-157832:~# useradd jerry
   root@hecs-157832:~# useradd kylin
   root@hecs-157832:~# useradd tsengia
   root@hecs-157832:~# useradd obama
   root@hecs-157832:~# usermod -e '2020-12-30' kylin
   ```

   

4. 将jerry、kylin、tsengia、obama等用户添加到zonghe组内

   ```bash
   root@hecs-157832:~# usermod -aG zonghe jerry 
   root@hecs-157832:~# usermod -aG zonghe tsengia
   root@hecs-157832:~# usermod -aG zonghe obama
   ```

   

5. 创建handy、cucci用户，其中cucci帐号的登录Shell设置为“/sbin/nologin”

   ```bash
   root@hecs-157832:~# useradd handy
   root@hecs-157832:~# useradd cucci -s /sbin/nologin
   ```

   

6. 将handy、cucci等用户添加到jishu 组内

   ```bash
   root@hecs-157832:~# usermod -aG jishu handy
   root@hecs-157832:~# usermod -aG jishu cucci
   ```

   

7. 将上述的所有用户均要求加入到guanli组内

   ```bash
   root@hecs-157832:~# groupadd guanli 
   root@hecs-157832:~# usermod -aG guanli jerry
   root@hecs-157832:~# usermod -aG guanli kylin
   root@hecs-157832:~# usermod -aG guanli tsengia
   root@hecs-157832:~# usermod -aG guanli obama
   root@hecs-157832:~# usermod -aG guanli handy
   root@hecs-157832:~# usermod -aG guanli cucci
   ```

   

8. 将zonghe组内的obama用户删除

   ```bash
   root@hecs-157832:~# gpasswd -d obama zonghe
   Removing user obama from group zonghe
   ```

   

9. 为jerry用户设置密码为“123456”（使用普通方法）为cucci用户设置密码为“redhat”

   ```bash
   root@hecs-157832:~# passwd jerry
   New password: 
   Retype new password: 
   passwd: password updated successfully
   root@hecs-157832:~# passwd cucci
   New password: 
   Retype new password: 
   passwd: password updated successfully
   ```

   

10. 将jerry用户锁定，并查看锁定状态

    ```bash
    root@hecs-157832:~# passwd -l jerry
    passwd: password expiry information changed.
    root@hecs-157832:~# passwd -S jerry
    jerry L 06/14/2024 0 99999 7 -1
    ```

    

11. 打开两个xshell窗口，通过（who 或者 w）命令查看连接状态，并通过fuser杀掉其中一个

    ```bash
    root@hecs-157832:~# who
    root     pts/4        2024-06-14 16:56 (112.5.195.104)
    root     pts/5        2024-06-14 16:56 (112.5.195.104)
    sx       pts/6        2024-06-14 16:56 (112.5.195.104)
    sx       pts/7        2024-06-14 16:56 (112.5.195.104)
    ```

    

12. 查看cucci用户，属于那些组，并查看其详细信息

    ```bash
    root@hecs-157832:~# id cucci
    uid=1007(cucci) gid=1007(cucci) groups=1007(cucci),2003(jishu),2004(guanli)
    ```

    

13. 手工创建账号student（预留）

    ```bash
    root@hecs-157832:~# useradd student
    ```

    

14. 设置权限及归属：/guanli目录属组设为guanli， /guanli/zonghe目录的属组设为zonghe /guanli/jishu目录的属组设为jishu，设置3个目录都是禁止其他用户访问的权限

    ```bash
    sx@hecs-157832:~/0614$ sudo chgrp guanli guanli
    sx@hecs-157832:~/0614$ chmod o-r guanli
    sx@hecs-157832:~/0614/guanli$ sudo chgrp zonghe zonghe
    sx@hecs-157832:~/0614/guanli$ chmod o-r zonghe
    sx@hecs-157832:~/0614/guanli$ sudo chgrp jishu jishu
    sx@hecs-157832:~/0614/guanli$ chmod o-r jishu
    ```

    

15. 建立公共目录/ceshi允许技术组内的所有用户读取、写入、执行文件， 禁止其他用户读、写、执行

    ```bash
    sx@hecs-157832:~/0614/guanli$ mkdir ceshi
    sx@hecs-157832:~/0614/guanli$ sudo chgrp jishu ceshi
    sx@hecs-157832:~/0614/guanli$ chmod g+wrx,o-wrx ceshi
    ```

    

16. 清除jerry用户密码

    ```bash
    sx@hecs-157832:~/0614/guanli$ sudo passwd -d jerry
    passwd: password expiry information changed.
    ```

    

17. 锁定cucci用户密码并查看状态

    ```bash
    root@hecs-157832:~# passwd -l cucci
    passwd: password expiry information changed.
    root@hecs-157832:~# passwd -S cucci
    cucci L 06/14/2024 0 99999 7 -1
    ```

    

18. 修改obama用户的UID为8888

    ```bash
    root@hecs-157832:~# usermod -u 8888 obama
    ```

    

19. 通过passwd命令修改kylin用户的最长密码使用期限为60天

    ```bash
    root@hecs-157832:~# chage -M 60 kylin
    root@hecs-157832:~# passwd -S kylin
    ```

    

20. 通过id groups等命令查看用户handy信息

```bash
root@hecs-157832:~# id handy
uid=1006(handy) gid=1006(handy) groups=1006(handy),2003(jishu),2004(guanli)
root@hecs-157832:~# groups handy
handy : handy jishu guanli