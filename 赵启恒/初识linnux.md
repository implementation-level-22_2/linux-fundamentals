| linux |                                                              |                                                |
| ----- | ------------------------------------------------------------ | ---------------------------------------------- |
|       | [2](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_2) |                                                |
|       | [3](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_3) | 一种操作系统内核，有centos，debian，redhat等。 |
|       | [4](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_4) |                                                |
|       | [5](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_5) | ## 安装                                        |
|       | [6](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_6) |                                                |
|       | [7](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_7) | 安装vm17，安装debian iso文件。                 |
|       | [8](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_8) |                                                |
|       | [9](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_9) | ## 命令                                        |
|       | [10](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_10) |                                                |
|       | [11](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_11) | ### 查看ip地址：                               |
|       | [12](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_12) |                                                |
|       | [13](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_13) | 第一种：ip addr show                           |
|       | [14](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_14) |                                                |
|       | [15](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_15) | 第二种：                                       |
|       | [16](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_16) |                                                |
|       | [17](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_17) | apt-get update                                 |
|       | [18](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_18) |                                                |
|       | [19](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_19) | apt-get install net-tools                      |
|       | [20](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_20) |                                                |
|       | [21](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_21) | ifconfig                                       |
|       | [22](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_22) |                                                |
|       | [23](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_23) | ### ssh：                                      |
|       | [24](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_24) |                                                |
|       | [25](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_25) | apt-get install ssh                            |
|       | [26](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_26) |                                                |
|       | [27](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_27) | apt-get install vim                            |
|       | [28](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_28) |                                                |
|       | [29](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_29) | vim /etc/ssh/sshd_config                       |
|       | [30](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_30) |                                                |
|       | [31](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_31) | port 22 打开端口                               |
|       | [32](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_32) |                                                |
|       | [33](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_33) | permitrootlogin yes 启用root登录               |
|       | [34](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_34) |                                                |
|       | [35](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_35) | passwordauthentication yes 验证密码            |
|       | [36](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_36) |                                                |
|       | [37](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_37) | permitemptypasswords no 禁止使用空密码登录     |
|       | [38](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_38) |                                                |
|       | [39](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_39) | /etc/init.d/ssh restart                        |
|       | [40](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_40) |                                                |
|       | [41](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_41) | ### sudo:                                      |
|       | [42](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_42) |                                                |
|       | [43](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_43) | su root 更换给root模式                         |
|       | [44](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_44) |                                                |
|       | [45](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_45) | apt-get install sudo                           |
|       | [46](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_46) |                                                |
|       | [47](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_47) | sudo vim /etc/sudoers                          |
|       | [48](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_48) |                                                |
|       | [49](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_49) | root all=(all:all) all                         |
|       | [50](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_50) |                                                |
|       | [51](https://gitee.com/implementation-level-22_2/linux-fundamentals/pulls/75/files#1630a8d33af83634aaef394b9fd23061729e05f4_0_51) | 用户名 all=(all:all) all 添加用户名            |