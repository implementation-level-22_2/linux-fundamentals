相关和目录可自行创建后再操作

- 绝对路径：/~
- 相对路径：/tmp

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c

   ```bash
   /~
   lala@hecs-393504:~$ sudo cp exam.c ~/tmp/shiyan.c
   lala@hecs-393504:~$ ls
   exam.c tmp
   lala@hecs-393504:~$ ls ~/tmp/
   shiyan.c
   ```

   ```bash
   /tmp
   lala@hecs-393504:~/tmp$ sudo cp ./../exam.c shiyan.c
   lala@hecs-393504:~$ ls ./../
   exam.c tmp
   lala@hecs-393504:~$ ls ./
   shiyan.c
   ```

2. 在任何目录下回到用户主目录？

   ```bash
   lala@hecs-393504:~/tmp$ cd
   lala@hecs-393504:~$ cd tmp
   lala@hecs-393504:~/tmp$ cd ~
   lala@hecs-393504:~$ 
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？

   ```bash
   lala@hecs-393504:~$ ls -all ~/tmp/test
   total 8
   drwxr-xr-x 2 root root 4096 May 27 11:19 .
   drwxr-xr-x 3 root root 4096 May 27 11:18 ..
   -rw-r--r-- 1 root root    0 May 27 11:19 lala.md
   -rw-r--r-- 1 root root    0 May 27 11:19 .la.txt
   ```

   ```bash
   lala@hecs-393504:~/tmp$ ls -all ./test
   total 8
   drwxr-xr-x 2 root root 4096 May 27 11:19 .
   drwxr-xr-x 3 root root 4096 May 27 11:18 ..
   -rw-r--r-- 1 root root    0 May 27 11:19 lala.md
   -rw-r--r-- 1 root root    0 May 27 11:19 .la.txt
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar

   ```bash
   lala@hecs-393504:~$ sudo tar -cvzf ~/tmp/test2/hailiang.tar ~/tmp/test2/*.txt
   tar: Removing leading `/' from member names
   /home/lala/tmp/test2/1.txt
   tar: Removing leading `/' from hard link targets
   /home/lala/tmp/test2/2.txt
   /home/lala/tmp/test2/3.txt
   /home/lala/tmp/test2/4.txt
   /home/lala/tmp/test2/5.txt
   lala@hecs-393504:~$ ls ~/tmp/test2
   1.txt  2.txt  3.txt  4.txt  5.txt  hailiang.tar
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo tar -cvzf test2/hailiang.tar *.txt
   1.txt
   2.txt
   3.txt
   4.txt
   5.txt
   lala@hecs-393504:~/tmp$ ls ./test2
   1.txt  2.txt  3.txt  4.txt  5.txt  hailiang.tar
   
   ```

5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？

   ```bash
   lala@hecs-393504:~$ sudo touch file1.txt && sudo mv file1.txt file2.txt
   lala@hecs-393504:~$ ls
   exam.c  file2.txt  tmp
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo touch file1.txt && sudo mv file1.txt file2.txt
   lala@hecs-393504:~/tmp$ ls
   file2.txt  shiyan.c  test  test2
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？

   ```bash
   lala@hecs-393504:~$ vim bbbb.txt
   lala@hecs-393504:~$ cat bbbb.txt 
   lala
   lala@hecs-393504:~$ ls
   bbbb.txt  exam.c  file2.txt  tmp
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo vim bbbb.txt
   [sudo] password for lala: 
   lala@hecs-393504:~/tmp$ cat bbbb.txt 
   lala
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？

   ```bash
   lala@hecs-393504:~$ sudo cp *.txt *.doc *.bak tmp/test
   lala@hecs-393504:~/tmp/test$ ls ~/tmp/test
   lala.bak  lala.doc  lala.md  lala.txt
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo cp *.txt *.doc *.bak tmp/test
   lala@hecs-393504:~/tmp$ ls ./test
   lala.bak  lala.doc  lala.md  lala.txt
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。

   ```bash
   lala@hecs-393504:~$ sudo mv file1.txt ~/docs
   lala@hecs-393504:~$ ls ~/docs
   file1.txt
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo mv file1.txt ./../docs
   lala@hecs-393504:~/tmp$ ls ./../docs
   file1.txt
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。

   ```bash
   lala@hecs-393504:~$ sudo cp file2.txt ~/backup/
   lala@hecs-393504:~$ ls ~/backup
   file2.txt
   ```

   ```bash
   lala@hecs-393504:~/tmp$ sudo cp file2.txt ./../backup/
   lala@hecs-393504:~/tmp$ ls ./../backup
   file2.txt
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。

    ```bash
    lala@hecs-393504:~$ sudo mv ~/docs/* ~/archive/
    lala@hecs-393504:~$ ls -R ~/archive
    .:
    file1.txt
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv ~/docs/* ./../archive/
    lala@hecs-393504:~/tmp$ ls -R ./../archive
    .:
    file1.txt
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。

    ```bash
    lala@hecs-393504:~$ sudo cp -r photos/* ~/backup/
    lala@hecs-393504:~$ ls -R ~/backup
    .:
    file2.txt  la.png
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp -r ./../photos/* ./../backup/
    lala@hecs-393504:~/tmp$ ls -R ./../backup/
    .:
    file2.txt  la.png
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。

    ```bash
    lala@hecs-393504:~$ sudo mv docs/report.doc papers/final_report.doc
    lala@hecs-393504:~$ ls ~/papers
    final_report.doc
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv ./../docs/report.doc ./../papers/final_report.doc
    lala@hecs-393504:~/tmp$ ls ./../papers
    final_report.doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。

    ```bash
    lala@hecs-393504:~$ sudo cp docs/notes.txt ~/backup/
    lala@hecs-393504:~$ ls ~/backup
    file2.txt  la.png  notes.txt
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp ./../docs/notes.txt ./../backup/
    lala@hecs-393504:~/tmp$ ls ./../backup/
    file2.txt  la.png  notes.txt
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。

    ```bash
    lala@hecs-393504:~$ sudo cp images/*.jpg ~/photos/
    lala@hecs-393504:~$ ls ~/photos
    la.png  lili.jpg
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp ./../images/*.jpg ./../photos/
    lala@hecs-393504:~/tmp$ ls ./../photos
    la.png  lili.jpg
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。

    ```bash
    lala@hecs-393504:~$ sudo cp docs/file*.txt ~/backup/
    lala@hecs-393504:~$ ls ~/backup
    file1.txt  file2.txt  la.png  notes.txt
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp ./../docs/file1*.txt  ./../backup/
    lala@hecs-393504:~/tmp$ ls ./../backup
    file1.txt  file2.txt  la.png  notes.txt
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中

    ```bash
    lala@hecs-393504:~$ sudo cp docs/*.txt ~/text_files/
    lala@hecs-393504:~$ ls ~/text_files/
    file1.txt  file2.txt  notes.txt
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp ./../docs/*.txt ./../text_files/
    lala@hecs-393504:~/tmp$ ls ./../text_files
    file1.txt  file2.txt  notes.txt
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。

    ```bash
    lala@hecs-393504:~$ sudo mv docs/* ~/temp/
    lala@hecs-393504:~$ ls ~/temp
    file1.txt  file2.txt  li.md  notes.txt
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv ./../docs/* ./../temp/
    lala@hecs-393504:~/tmp$ ls ./../temp
    file1.txt  file2.txt  li.md  notes.txt
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。

    ```bash
    lala@hecs-393504:~$ sudo mv -v docs/* archive/
    renamed 'docs/li.md' -> 'archive/li.md'
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv -v ./../docs/* ./../archive/
    renamed './../docs/l.md' -> './../archive/l.md'
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。

    ```bash
    lala@hecs-393504:~$ sudo cp -r docs/*/ backup/
    lala@hecs-393504:~$ ls ~/backup
    file1.txt  file2.txt  la  la.png  notes.txt  temp_la
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp -r ./../docs/*/ ./../backup/
    lala@hecs-393504:~/tmp$ cd ./../backup/
    file1.txt  file2.txt  la  la.png  notes.txt  temp_la
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。

    ```bash
    lala@hecs-393504:~$ sudo mv docs/!(temp_*) backup/
    lala@hecs-393504:~/backup$ ls
    file1.txt  file2.txt  la  la.jpg  la.png  list.pdf  notes.txt  temp_la
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv ./../docs/!(temp_*) ./../backup/
    lala@hecs-393504:~/tmp$ cd ./../backup/
    file1.txt  file2.txt  la  la.jpg  la.png  list.pdf  notes.txt  temp_la
    ```

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。

    ```bash
    lala@hecs-393504:~$ sudo mv -bS _bak docs/report.txt archive
    lala@hecs-393504:~$ ls ~/archive
    file1.txt  li.md  report.txt  report.txt_bak
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo mv -bS _bak ./../docs/report.txt ./../archive
    lala@hecs-393504:~/tmp$ ls ./../archive/
    file1.txt  li.md  report.txt  report.txt_bak
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

    ```bash
    lala@hecs-393504:~$ sudo cp -f docs/*.pdf pdf_files/    
    lala@hecs-393504:~$ ls ~/pdf_files
    list.pdf
    ```

    ```bash
    lala@hecs-393504:~/tmp$ sudo cp -f ./../docs/*.pdf ./../pdf_files/    
    lala@hecs-393504:~/tmp$ ls ./../pdf_files 
    list.pdf
    ```

    