## 文件及文件夹的管理(查看)

1.查看文件内容

- cat   正向显示整个文件的内容

  ```bash
  cat [选项] 文件 ...  # 作用：正向显示文件全部内容
  -n 显示行号
  ```

- tac   反向显示整个文件的内容

  ```bash
  tac 文件 ...   # 作用：反向显示文件全部内容，命令单词倒序
  ```

  cat和tac的共同点：①同时显示多个文件   ②一次性加载整个文件并显示全部内容

  ③比较适用于小文件，大文件不推荐使用

- more   先加载整个文件，再分页显示

  ```bash
  more [选项] 文件   # 作用：分页显示文件内容，每次显示一屏，即按页显示文件内容
  #空格键 下一页   回车键 下一行
  ```

- less   先加载整个文件，根据需求加载内容，再分页显示

  优点：①加载快   ②支持翻页   ③可以搜索文本内容

  ```bash
  less [选项] 文件   # 作用：同more，但提供了更多的导航和搜索功能，如上下滚动、搜索等。
  #使用上下键、Page Up/Page Down键进行滚动，`/`搜索
  #`n`下一个匹配，`N`上一个匹配，`q`退出。
  ```

  - **无选项**：
    - 直接使用 `less 文件名` 来查看文件内容。
  - **-N 或 --line-numbers**：
    - 显示行号。
  - **-M 或 --long-prompt**：
    - 显示长提示符，包括文件名和行号，百分比。
  - **-MN** 内容中显示行号，底部显示文件名，行号和百分比
    - 两者可以组合使用
  - **-m 或 -i**
    - 搜索时忽略大小写
  - **+行数**：
    - 打开文件后立即跳转到指定的行数。

- head   查看文件头部前10行

  ```bash
  head [选项] 文件 ...   # 作用：显示文件开头的内容（默认10行）
  #-n	选项指定行数
  #-q 不显示文件名。当使用多个文件作为输入时，该选项可以省略文件名前的标题
  ```

- tail   查看文件头部前尾部后10行

  ！！！head和tail都只加载相关行的内容

  ```bash
  tail [选项] 文件...   # 作用：显示文件结尾的内容（默认10行）
  #-q 不显示文件名
  #-n 选项指定行数
  #-f 实时查看文件增长（如日志文件）
  ```

- nl   给文件内容家行号，可以实时追踪文件变化并显示最新内容

  ```bash
  nl [选项] 文件...   # 作用： 显示文件内容，并添加行号（空行不加）
  #-s 选项指定分隔符
  ```

  **以下所有操作都在家目录执行：**

  ### 

  1. **操作题一**：使用 `cat` 命令显示 `/etc/passwd` 文件的内容。

     ```bash
     lala@hecs-393504:~$ cat /etc/passwd
     root:x:0:0:root:/root:/bin/bash
     daemon:x:1:1:daemon:/usr/sbin:/usr/sbin/nologin
     bin:x:2:2:bin:/bin:/usr/sbin/nologin
     ...
     lalla:x:1001:1001::/home/lalla:/bin/bash
     la:x:1002:1002::/home/la:/bin/bash
     lala:x:1003:1003::/home/lala:/bin/bash
     ```

  2. **操作题二**：将文件 `/etc/passwd` 的内容复制到 `passwd_bak.txt` 文件中，但不使用 `cp` 命令。

     ```bash
     lala@hecs-393504:~$ sudo cat /etc/passwd > passwd_bak.txt
     [sudo] password for lala: 
     lala@hecs-393504:~$ ls
     lala@hecs-393504:~$ ls
     bbbb.txt  exam.c  file2.txt  passwd_bak.txt
     ```

  3. **操作题三**：新建两个文件  `file1.txt` 和 `file2.txt`，分别写一些不同的内容，再将这两个文件的内容合并到一个新的文件 `file3.txt` 中。

     ```bash
     lala@hecs-393504:~$ sudo vim file2.txt 
     lala@hecs-393504:~$ sudo vim file1.txt 
     lala@hecs-393504:~$ cat file1.txt file2.txt 
     this is file1.txt!
     this is file2.txt!
     lala@hecs-393504:~$ sudo cat file1.txt file2.txt > file3.txt
     lala@hecs-393504:~$ cat file3.txt 
     this is file1.txt!
     this is file2.txt!
     ```

  ### 

  1. 使用命令从尾部开始显示 `bigfile.txt` 文件的内容。

     ```bash
     lala@hecs-393504:~$ tac bigfile.txt 
     最后一行，标志着文件的结束。
     这里是文件末尾的几行内容，用于测试tail命令。  
       
     ERROR: 另一个错误消息，也需要被找到。 
     再次出现的错误消息，用于测试。  
     ...
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     这是一行测试文本，用于演示各种文本处理命令。
     ```

  2. 尝试找出 `bigfile.txt` 文件的最后一行内容，要使用 `tac` 命令。

     ```bash
     lala@hecs-393504:~$ tac bigfile.txt | head -n 1 
     最后一行，标志着文件的结束。
     ```

  3. 查看 `bigfile.txt` 文件的最后5行内容。

     ```bash
     lala@hecs-393504:~$ tail -n 5 bigfile.txt 
     ERROR: 另一个错误消息，也需要被找到。 
       
     这里是文件末尾的几行内容，用于测试tail命令。  
     
     最后一行，标志着文件的结束。
     ```

  4. 倒序查看 `bigfile.txt` 文件的最后5行内容。

     ```bash
     lala@hecs-393504:~$ tac bigfile.txt | head -n 5 
     最后一行，标志着文件的结束。
     这里是文件末尾的几行内容，用于测试tail命令。  
       
     ERROR: 另一个错误消息，也需要被找到。 
     再次出现的错误消息，用于测试。 
     ```

  ### 

  1. **操作题一**：使用 `more` 命令查看 `bigfile.txt` 文件的内容，并在查看过程中使用空格键翻页。

     ```bash
     lala@hecs-393504:~$ more bigfile.txt  
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
     
     Artists are the true creators of our world, shaping our perception of beauty and culture.
     Adventure awaits those who dare to step out of their comfort zones and explore the unknown.
     
     --More--(92%)
     ```

  2. **操作题二**：在 `more` 命令的查看过程中，如何使用回车键来逐行查看文件内容？

     ```bash
     lala@hecs-393504:~$ more bigfile.txt 
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
      ...
     Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.  
     
     --More--(44%)
     ```

  3. **操作题三**：如何在 `more` 命令中搜索`bigfile.txt`特定字符串（例如 "error"）并跳转到下一个匹配项？

     ```bash
     lala@hecs-393504:~$ more bigfile.txt  +/error
     Pattern not found这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
       ...
       
     lala@hecs-393504:~$ more bigfile.txt  +/ERROR
     
     ...skipping
       
     以下是重复的行，用于测试分页和搜索功能。  
     ERROR: 这是一个错误消息，需要被找到。  
     这是一行普通的文本。
     ```

     

  ### `less` 命令操作题

  1. **操作题一**：使用 `less` 命令查看 `bigfile.txt` 文件，并快速定位到文件的末尾。

     ```bash
     lala@hecs-393504:~$ less bigfile.txt
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
     ...
     Apples are my favorite fruit, especially during the autumn season.
     Always remember to be kind to others, as kindness is a virtue that never fades.
     As the sun rises, I greet the new day with a smile and a positive mindset.
     Artists are the true creators of our world, shaping our perception of beauty and culture.
     Adventure awaits those who dare to step out of their comfort zones and explore the unknown.
     -- 空格/PageDown
     
     :
     ```

     

  2. **操作题二**：在 `less` 命令中，如何向上和向下滚动文件内容？

     ```bash
     lala@hecs-393504:~$ less bigfile.txt
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
       
     ...
     
     Although challenges may arise, always persevere in pursuit of your goals.
     -- 回车键/下方向键
     :
     ```

     

  3. **操作题三**：在 `less` 命令中，如何搜索`bigfile.txt`一个特定的函数名（例如 `def my_function`），并查看所有匹配项？

     ```bash
     lala@hecs-393504:~$ less bigfile.txt
     /def my_function
     
     def my_function {
      嘿嘿嘿
     }
     
     再次出现的错误消息，用于测试。  
     ERROR: 另一个错误消息，也需要被找到。  
     
     def my_function {
      哈哈哈
     }
     ...
     
     :
     ```

  ### `head` 命令操作题

  1. **操作题一**：使用 `head` 命令显示 `bigfile.txt` 文件的前5行内容。

     ```bash
     lala@hecs-393504:~$ head -n 5 bigfile.txt 
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
       
     以下是重复的行，用于测试分页和搜索功能。  
     ```

     

  2. **操作题二**：将 `bigfile.txt` 的前20行内容保存到 `bigfile_20.txt` 文件中。

     ```bash
     lala@hecs-393504:~$ head -n 20 bigfile.txt > bigfile_20.txt
     lala@hecs-393504:~$ cat bigfile_20.txt 
     这是一行测试文本，用于演示各种文本处理命令。  
     第二行内容，包含了更多的字符来模拟一个大文件。  
     第三行，同样包含了足够多的字符，以便在查看时能够滚动或分页。  
     ...
     再次出现的错误消息，用于测试。  
     ERROR: 另一个错误消息，也需要被找到。
     ```

  3. **操作题三**：如何结合 `head` 和 `grep` 命令来查找 `bigfile.txt` 文件中以 "A" 开头的前10行？

     ```bash
     lala@hecs-393504:~$ grep 'A' bigfile.txt | head -n 10 
     Although challenges may arise, always persevere in pursuit of your goals.
     Ancient wisdom teaches us the value of patience and understanding.
     Autumn brings with it a sense of calm and reflection, allowing us to appreciate the beauty of nature.
     An apple a day keeps the doctor away, a saying that reminds us of the importance of healthy eating.
     After a long day, a warm bath and a cup of tea are the perfect way to unwind and relax.
     A开头是个错误
     A不是好主意
     Apples are my favorite fruit, especially during the autumn season.
     Always remember to be kind to others, as kindness is a virtue that never fades.
     As the sun rises, I greet the new day with a smile and a positive mindset.
     ```

  ### `tail` 命令操作题

  1. **操作题一**：使用 `tail` 命令显示 `bigfile.txt` 文件的最后20行内容。

     ```bash
     lala@hecs-393504:~$ tail -n 20 bigfile.txt 
     以下是一些随机文本，用于填充文件。  
     Lorem ipsum dolor sit amet, consectetur adipiscing elit.  
     Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  ...
     再次出现的错误消息，用于测试。  
     ERROR: 另一个错误消息，也需要被找到。 
       
     这里是文件末尾的几行内容，用于测试tail命令。  
     
     最后一行，标志着文件的结束。
     ```

  2. **操作题二**：如何实时跟踪一个正在写入的日志文件（如 `bigfile.txt`）的最后10行内容？

     ```bash
     lala@hecs-393504:~$ tail -f bigfile.txt
     lala@hecs-393504:~$ sudo echo 'lala的实时更新' > bigfile.txt 
     lala@hecs-393504:~$ tail bigfile.txt 
     
     ...skipping 1 line
     
     再次出现的错误消息，用于测试。  
     ERROR: 另一个错误消息，也需要被找到。 
       
     这里是文件末尾的几行内容，用于测试tail命令。  
     
     最后一行，标志着文件的结束。
     lala的实时更新
     ```

     

  3. **操作题三**：在 `tail` 命令中，如何反向显示文件的最后10行内容（即按从旧到新的顺序显示）？

     ```bash 
     lala@hecs-393504:~$ tail -n 10 bigfile.txt | tac 
     最后一行，标志着文件的结束。
     这里是文件末尾的几行内容，用于测试tail命令。  
       
     ERROR: 另一个错误消息，也需要被找到。 
     再次出现的错误消息，用于测试。  
     
     
     
     Adventure awaits those who dare to step out of their comfort zones and explore the unknown.
     ```

  ### 综合题

  **综合题**：假设你有一个非常大的日志文件 `bigfile.txt`，你需要找出所有包含 "ERROR" 字符串的行，并查看这些行及其之前的两行内容。

  ```bash
  lala@hecs-393504:~$ cat -n bigfile.txt | grep -B2 'ERROR'  
       4    
       5  以下是重复的行，用于测试分页和搜索功能。  
       6  ERROR: 这是一个错误消息，需要被找到。  
  --
      18
      19  再次出现的错误消息，用于测试。  
      20  ERROR: 另一个错误消息，也需要被找到。  
  --
      25
      26
      27  ERROR: 这是一个错误消息，需要被找到。  
  --
      43  再次出现的错误消息，用于测试。  
      44  A开头是个错误
      45  ERROR: 另一个错误消息，也需要被找到。 
  --
      48
      49  A不是好主意
      50  ERROR: 这是一个错误消息，需要被找到。  
  --
      67
      68  再次出现的错误消息，用于测试。  
      69  ERROR: 另一个错误消息，也需要被找到。
  ```

  

