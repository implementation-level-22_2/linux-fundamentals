# 笔记：

awk 、grep、sed 是linux操作文本的三剑客

Awk命令的基本框架

```bash
awk [选项] '脚本' 文件


awk '{print}' 文件名    打印全文


这里的 `[选项]` 是可选的，它们在 `awk` 命令中通常出现在脚本和文件名之前。
```



## 在awk中文本输入需要用 ” “

Awk 的脚本是由一个或者多个语句块组成的 ：

1、开始语句块：  是在 程序开始时执行的  BEGIN { print ” start“}

2、通用语句块： 逐行处理文件内容 pattern {commands}，

3、结束语句块：程序结束时执行   END {print ”end“}





Awk 中的常用选项（限命令行）

* `-F`：指定输入字段分隔符。例如，`-F,` 将逗号设置为字段分隔符。
  
  * 默认是以空格\t等为分隔符
  * 类似于FS="分隔符" 在文件中使用

* `-v`：赋值外部变量。例如，`-v var=value`。 min=60

* `-f`：指定 Awk 脚本文件。例如，`-f script.awk`。

* `-W`：控制警告和其他运行时选项。例如，`-W version` 用于显示版本信息。



Awk 中的常用模式（pattern）

* 匹配正则表达式
  
  * `/pattern/`：匹配包含指定模式的行。例如，`/error/` 匹配所有包含“error”的行。
  * $2 ~ /pattern/ ：匹配第2列包含关键字pattern的行，
  * $2 !~ /pattern/：匹配第2列<font color= yellow>不包含</font>关键字pattern的行

* 比较运算符
  
  * 匹配第 n 行。例如，`NR == 1` 匹配第一行。
  * `==`（等于），例$2 == "张三" 匹配第二列等于张三的行
  * `!=`（不等于）
  * `<`（小于）
  * `<=`（小于等于）
  * `>`（大于）
  * `>=`（大于等于）

* 逻辑运算符
  
  * `NR >= m && NR <= n`：匹配第 m 到第 n 行。例如，`NR >= 2 && NR <= 4` 匹配第2到第4行。
  * `&&`：逻辑与（AND）
  * `||`：逻辑或（OR）
  * `!`：逻辑非（NOT）

* 三元运算符
  
  * 条件运算符 `? :`
  * grade = ($2 >= 60 ? "及格" : "不及格")

#### Awk 中的常用动作（action）

* `print`：打印指定内容。例如，`print $1` 打印第一字段。

* `printf`：格式化输出。例如，`printf "%s\n", $1` 以格式化方式打印第一字段。

语法：

```bash
printf (format, expression1, expression2, ...)
# format 是一个包含格式说明符的字符串。
# expression1, expression2, ... 是要格式化并插入到 format 字符串中的表达式。
```



* * 常用的格式说明符
    
    * `%s`：字符串
    * `%d`：有符号十进制整数
    * `%f`：浮点数
  
  * 带有宽度和对齐的写法：
    
    * `%5s`：字段宽度为 5 的字符串，右对齐
    * `%-5s`：字段宽度为 5 的字符串，左对齐
    * `%10d`：字段宽度为 10 的有符号十进制整数，右对齐
    * `%-10d`：字段宽度为 10 的有符号十进制整数，左对齐
    * `%8.2f`：字段总宽度为 8，其中小数点后有 2 位的浮点数

* `{}`：包含一个或多个动作的块。例如，`{ print $1; print $2 }`。
  
  * 用;号分隔多个动作语句
  * 如果每个语句在单独的行上，;号可以省略

#### Awk 中的特殊变量

* `NR`：表示记录的数量（当前行号）。Numbers of Rows
* `NF`：表示当前行的字段数量。`$NF`表示什么？最后一列 Number of flied
* `$0`：包含当前行的文本内容，即一整行内容。有时候也省略
* `$1`、`$2`：表示当前行的第1个、第2个字段的内容，以次类推。
* `FS`：输入时的域分割符。效果同-F选项 File split
* `OFS`：输出时的域分割符。out File split







作业：

1. 只显示/etc/passwd的账户
   
   ```bash
   abc@hecs-288034:~$ awk -F: '{print $1}' /etc/passwd
   
   ```

2. 只显示/etc/passwd的账户和对应的shell，并在第一行上添加列名用户制表符shell，最后一行添加----------------
   
   ```bash
   abc@hecs-288034:~$ awk -F: 'BEGIN {print "用户\tshell"} {print $1,$NF}' /etc/passwd
   
   ```

3. 搜索/etc/passwd有关键字root的所有行
   
   ```bash
   abc@hecs-288034:~$ awk -F: '/root/ {print}' /etc/passwd
   
   ```

4. 统计/etc/passwd文件中，每行的行号，每列的列数，对应的完整行内容以制表符分隔
   
   ```bash
   abc@hecs-288034:~$ awk -F: '{print NR"\t"NF"\t"$0}' /etc/passwd
   
   
   ```

5. 输出/etc/passwd文件中以nologin结尾的行
   
   ```bash
   abc@hecs-288034:~$ awk -F: '$NF ~ /nologin/ {print $0}' /etc/passwd
   
   ```

6. 输出/etc/passwd文件中uid字段小于100的行
   
   ```bash
   abc@hecs-288034:~$ awk -F: '$3<100 {print}' /etc/passwd
   
   ```

7. /etc/passwd文件中gid字段大于200的，输出该行第一、第四字段，第一，第四字段并以制表符分隔
   
   ```bash
   abc@hecs-288034:~$ awk -F: '$4>200 {print $1"\t"$4 }' /etc/passwd
   
   ```

8. 输出/etc/passwd文件中uid字段大于等于100的行

```bash

abc@hecs-288034:~$ awk -F: '$3>=100 {print }' /etc/passwd

```