#  Linux

Linux是一套免费使用和自由传播的类Unix操作系统，是一个多用户、多任务、支持多线程和多CPU的操作系统

有centos，debian，redhat等

# 安装必要组件

```java
IP address show  //查看IP地址等信息
apt-get install net-tools //安装net-tools组件包
apt-get install vim  // vim编辑器
apt-get update // 更新软件库
apt-get install ssh   // 安装ssh,默认情况下，只有普通用户才可以登录这个服务器
```

#  修改ssh文件

```java
vim /etc/ssh/sshd_cofnig   //进入并修改ssh配置文件
---------------------------------------------------------------------------------------
//更改以下3行命令
port:22  //开启端口22
PermitRootLogin yes  //打开root用户登入
PasswordAuthentication yes  //打开密码验证
PermitEmptyPassword no //不允许空密码登录
//修改完成按Esc键在按:wq保存退出
---------------------------------------------------------------------------------------
// 用sudo 命令，但这个默认是没安装。自己安装 apt-get isntall sudo
sudo systemctl restart ssh  //重启SSH服务，是root用户可以去掉sudo
su root 更换给root模式
```

