````
## 刚安装完Deiban系统 7步走：

1. 设置国内软件源镜像  /etc/apt/sources.list 
2. 更新软件包列表:apt update
3. 更新系统:apt upgrade -y
4. 安装vim:apt install vim -y  locate 数据库 find 实时
5. 编辑网卡配置，设置静态IP:

```bash
vim /etc/network/interfaces

# 按如下设置
# The primary network interface
allow-hotplug ens192
iface ens192 inet static
address 172.16.90.71
netmask 255.255.255.0
gateway 172.16.90.1

#重新启动网络服务
sudo systemctl restart networking
```

5. 修改SSHD配置，允许root用户远程登录 

```bash
vim /etc/ssh/sshd_config

# 取消如下代码的注释，并且将其值设置为yes，以允许root用户的远程登录
PermitRootLogin yes

```

6. 配置环境变量，简单化ls的用法 


/etc/profile.d    /etc/nginx/conf.d/

```bash
 export LS_OPTIONS='--color=auto'
 alias ls='ls $LS_OPTIONS'
 alias ll='ls $LS_OPTIONS -l'
 alias l='ls $LS_OPTIONS -lA'

```
7. 配置环境变量，设置当前系统字符集，以接受中文

/etc/profile.d/lang.sh
```bash
export LC_ALL=C.utf8
```

要确保设置生效，可以重新加载该文件并检查环境变量

```sh
source /etc/profile.d/lang.sh
echo $LC_ALL
```



几个常用命令

以下是一些常见的 Linux 命令，包括它们的使用场景、核心术语讲解以及示例：

### 1. `time`

**使用场景**：用于测量命令的执行时间。

**核心术语**：
- **real**：总的时间（墙上时钟时间）。
- **user**：用户态 CPU 时间。
- **sys**：内核态 CPU 时间。

**示例**：

```bash
time ls
```

输出：

```plaintext
real    0m0.003s
user    0m0.001s
sys     0m0.002s
```

### 2. `date`

**使用场景**：显示或设置系统日期和时间。

**核心术语**：
- **%Y**：四位数的年份。
- **%m**：两位数的月份。
- **%d**：两位数的日期。

**示例**：

```bash
date "+%Y-%m-%d %H:%M:%S"
```

输出：

```plaintext
2024-06-10 15:30:00
```

### 3. `timedatectl`

**使用场景**：查看和设置系统时间和日期，时区和 NTP（网络时间协议）设置。

**核心术语**：
- **NTP**：网络时间协议，用于同步时间。
- **RTC**：实时时钟。

**示例**：

```bash
timedatectl status
```

输出：

```plaintext
Local time: Mon 2024-06-10 15:30:00 UTC
Universal time: Mon 2024-06-10 15:30:00 UTC
RTC time: Mon 2024-06-10 15:30:00
Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
NTP service: active
```

### 4. `reboot`

**使用场景**：重新启动系统。

**核心术语**：
- **systemd**：系统和服务管理器。

**示例**：

```bash
sudo reboot
```

### 5. `poweroff`   

**使用场景**：关闭系统电源。

**核心术语**：

- **halt**：停止系统所有的 CPU 功能。

**示例**：

```bash
sudo poweroff
```

### 6. `wget`

**使用场景**：从网络上下载文件。

**核心术语**：

- **URL**：统一资源定位符。
- **HTTP/HTTPS**：超文本传输协议。

**示例**：

```bash
wget https://example.com/file.txt
```

### 7. `curl`

**使用场景**：从网络上获取或发送数据。

**核心术语**：

- **URL**：统一资源定位符。
- **GET/POST**：HTTP 请求方法。

**示例**：

```bash
curl -O https://example.com/file.txt
```

### 8. `ps`

**使用场景**：查看当前运行的进程。

**核心术语**：

- **PID**：进程标识符。
- **TTY**：终端类型。

**示例**：

```bash
ps aux  # 显示所有用户的所有进程 一般会结合。grep,awk等过滤数据
```

### 9. `kill`

**使用场景**：向进程发送信号（通常用于终止进程）。

**核心术语**：

- **SIGTERM**：请求中止进程。
- **SIGKILL**：强制终止进程。

**示例**：

```bash
kill 1234  # 用pidof 进程名。来查看具体的进程的进程号
```

### 10. `killall`

**使用场景**：向指定名称的所有进程发送信号。

`先安装psmisc`

1. 安装psmisc 后，就会有pstree和killall

**核心术语**：

- **signal**：信号，通知进程执行某种操作。

**示例**：

```bash
killall firefox
```

### 11. `ip`

**使用场景**：显示和操作网络接口和路由。

**核心术语**：

- **address**：IP 地址。
- **route**：路由信息。

**示例**：

```bash
ip addr show
```

### 12. `ss`

**使用场景**：显示套接字统计信息。旧版是netstat

**核心术语**：
- **TCP**：传输控制协议。
- **UDP**：用户数据报协议。

**示例**：

```bash
ss -tuln # 可以查看哪些端口开放着
```

### 13. `uname`

**使用场景**：显示系统信息。

**核心术语**：

- **kernel**：操作系统内核。
- **OS**：操作系统。

**示例**：

```bash
uname -a
```

### 14. `uptime`

**使用场景**：显示系统运行时间和负载。

**核心术语**：
- **load average**：系统平均负载。

**示例**：

```bash
uptime
```

### 15. `who`

**使用场景**：显示当前登录用户信息。

**核心术语**：

- **login**：用户登录信息。
- **TTY**：终端类型。

**示例**：

```bash
who
```

### 16. `last`

**使用场景**：显示系统上最近的登录信息。

**核心术语**：
- **wtmp**：记录登录和注销事件的文件。

**示例**：

```bash
last
```

### 17. `ping`

**使用场景**：测试网络连通性。

- **ICMP**：互联网控制消息协议。
- **echo request**：回显请求。

**示例**：

```bash
ping example.com
```

### 18. `traceroute`

**使用场景**：显示到达网络主机的路径。

**核心术语**：
- **hop**：从一个网络节点到另一个的跳转。
- **TTL**：生存时间。

**示例**：

```bash
traceroute example.com
```

### 19. `history`

**使用场景**：显示命令历史记录。

**核心术语**：

- **bash history**：记录用户输入的命令历史。

**示例**：

```bash
history
```

`top` 命令用于显示系统的运行进程信息，包括 CPU 占用情况、内存占用情况、进程列表等。通过 `top` 命令可以实时监控系统的运行状态，了解系统的负载情况。

`pidof` 命令用于根据进程名查找对应的进程ID（PID）。例如，如果你知道某个进程的名字，但不知道它的PID，你可以使用 `pidof` 命令来查找。语法通常是 `pidof <进程名>`，比如 `pidof python` 会返回所有名为 `python` 的进程的PID。



`free` 和 `df -h` 是两个用于查看系统资源利用情况的常用命令：

1. **`free`**：该命令用于显示系统内存的使用情况，包括物理内存和交换空间（swap）。执行 `free` 命令时，会显示系统的内存信息，包括总内存、已使用内存、空闲内存以及缓冲区和缓存等情况。

    示例输出：
    ```
                 total        used        free      shared  buff/cache   available
    Mem:        8081256     2133468     3458604      430300     2488184     5261632
    Swap:             0           0           0
    ```

    输出中的字段含义如下：
    - `total`: 总内存量
    - `used`: 已使用的内存量
    - `free`: 空闲的内存量
    - `shared`: 用于共享的内存量
    - `buff/cache`: 用于缓存的内存量
    - `available`: 可用的内存量

2. **`df -h`**：该命令用于显示文件系统的磁盘空间利用情况。执行 `df -h` 命令时，会显示系统中每个挂载的文件系统的磁盘使用情况，包括总容量、已使用空间、剩余空间以及挂载点等信息。

    示例输出：
    ```
    Filesystem      Size  Used Avail Use% Mounted on
    /dev/sda1        20G   15G  3.6G  81% /
    /dev/sda2        30G   20G   8.8G  70% /home
    ```

    输出中的字段含义如下：
    - `Filesystem`: 文件系统设备
    - `Size`: 文件系统总容量
    - `Used`: 已使用的空间
    - `Avail`: 可用空间
    - `Use%`: 使用百分比
    - `Mounted on`: 挂载点

通过这两个命令，你可以快速了解系统的内存和磁盘空间使用情况，以便进行系统资源管理和监控。



`du` 命令用于估算文件或目录的磁盘使用情况。它可以显示指定文件或目录的磁盘使用量，以及每个子目录的磁盘使用量，帮助用户了解文件系统上的空间分布情况。以下是 `du` 命令的一些常见使用场景：

1. **查看当前目录的磁盘使用情况**：

   ```
   复制代码
   du -h
   ```
   
   这将以人类可读的格式显示当前目录及其所有子目录的磁盘使用情况。
   
2. **查看特定目录的磁盘使用情况**：

   ```
   du -h /目录名
   ```
   
   这将显示指定目录及其所有子目录的磁盘使用情况。

3. **显示文件或目录的总磁盘使用量**：

   ```
   du -sh /path/to/file_or_directory
   ```
   
   这将显示指定文件或目录的总磁盘使用量， `-s` 参数表示仅显示总和， `-h` 表示以人类可读的格式显示。

4. **显示目录中每个文件的磁盘使用量**：

   ```
   复制代码
   du -ah /path/to/directory
   ```
   
   这将显示指定目录中每个文件的磁盘使用量， `-a` 参数表示包括所有文件。
   
5. **按照磁盘使用量排序显示目录**：

   ```
   du -h | sort -h
   ```
   
   这将按照磁盘使用量从小到大排序显示当前目录及其所有子目录的磁盘使用情况。

6. **限制显示的深度**：

   ```
   du -h --max-depth=1 /path/to/directory
   ```
   
   这将仅显示指定目录的直接子目录的磁盘使用情况， `--max-depth` 参数用于指定显示的深度。









## 重定向和管道

在 Linux 和其他 Unix 系统中，重定向和管道是非常重要的功能，它们允许用户将命令的输出重定向到文件、其他命令或设备。以下是对重定向和管道符的详细解释、使用场景、核心术语以及示例。

### 重定向

重定向允许将命令的输入或输出重定向到文件或其他命令。常用的重定向符号包括 `>`、`>>`、`<`、`2>` 等。

#### 输出重定向

1. **覆盖输出重定向 `>`**

   将命令的标准输出重定向到一个文件，如果文件已经存在，则覆盖它。

   ```bash
   ls > filelist.txt
   ```

   这个命令将 `ls` 命令的输出保存到 `filelist.txt` 文件中，如果文件已存在，则会被覆盖。

2. **追加输出重定向 `>>`**

   将命令的标准输出追加到一个文件的末尾，如果文件不存在，则创建它。

   ```bash
   echo "Hello, World!" >> greetings.txt
   ```

   这个命令会将 "Hello, World!" 追加到 `greetings.txt` 文件中。

#### 输入重定向

1. **输入重定向 `<`**

   将文件的内容作为命令的输入。

   ```bash
   sort < unsorted_list.txt
   ```

   这个命令将 `unsorted_list.txt` 文件的内容作为 `sort` 命令的输入进行排序。

#### 错误重定向

1. **错误输出重定向 `2>`**

   将命令的错误输出重定向到一个文件。

   ```bash
   ls non_existent_file 2> error_log.txt
   ```

   这个命令会将 `ls` 命令的错误信息保存到 `error_log.txt` 文件中。

2. **错误输出追加重定向 `2>>`**

   将命令的错误输出追加到一个文件。

   ```bash
   ls non_existent_file 2>> error_log.txt
   ```

   这个命令会将 `ls` 命令的错误信息追加到 `error_log.txt` 文件中。

3. **同时重定向标准输出和标准错误输出**

   使用 `&>` 符号将标准输出和错误输出同时重定向到同一个文件。

   ```bash
   command &> output_and_error_log.txt
   ```

   这个命令会将 `command` 的标准输出和错误输出都保存到 `output_and_error_log.txt` 文件中。

### 管道 `|`

管道符号 `|` 将一个命令的输出作为另一个命令的输入。管道是进行命令链式操作的重要工具。

#### 使用场景

1. **将命令的输出传递给另一个命令**

   ```bash
   ls -l | grep "txt"
   ```

   这个命令会将 `ls -l` 的输出传递给 `grep "txt"`，只显示包含 "txt" 的行。

2. **多命令链式操作**

   ```bash
   ps aux | grep "sshd" | awk '{print $2}'
   ```

   这个命令链会列出所有进程 (`ps aux`)，然后过滤包含 "sshd" 的行 (`grep "sshd"`)，最后提取进程 ID (`awk '{print $2}'`)。

### 示例解析

#### 使用管道和重定向的综合示例

1. **查找特定文件并将结果保存到文件**

   ```bash
   find / -name "*.log" 2> errors.txt | sort > sorted_log_files.txt
   ```

   这个命令会在根目录下查找所有以 `.log` 结尾的文件，将错误信息重定向到 `errors.txt`，并将结果进行排序后保存到 `sorted_log_files.txt`。

### 总结

- **重定向**：
  - `>`：将输出重定向到文件（覆盖）。
  - `>>`：将输出追加到文件。
  - `<`：将文件内容作为输入。
  - `2>`：将错误输出重定向到文件。
  - `2>>`：将错误输出追加到文件。
  - `&>`：将标准输出和错误输出同时重定向到文件。

- **管道 `|`**：
  - 将一个命令的输出作为另一个命令的输入，用于链式操作。

## 思考：

```
<< 是什么功能？
()把命令包括起来，是干什么功能？
````