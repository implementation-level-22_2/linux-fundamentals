1. **题目**： 您有一个初始目录结构，其中包含名为 `development` 的目录。在 `development` 目录下，您需要先创建名为 `old_code` 的子目录，并在 `old_code` 中创建一些 `.py`、`.pyc` 和 `.pyo` 文件（模拟已有的项目结构）。之后，您需要对这个项目进行整理，确保每个部分的文件都在正确的位置，并删除不再需要的文件和目录。以下是您需要完成的具体任务：
2. 使用 `cd` 命令进入 `development` 目录（假设您已经在包含 `development` 目录的父目录中）。

```
root@hecs-288034:/home# mkdir development
root@hecs-288034:/home# ls
abc  development
```

1. 在 `development` 目录下创建名为 `old_code` 的子目录。

   ```
   root@hecs-288034:/home/development# mkdir old_code
   root@hecs-288034:/home/development# ls
   old_code
   ```

```
3. 在 `old_code` 目录下创建以下文件和目录结构：
- `module1/script1.py`

  - `module1/script1.pyc`
  - `module1/script2.py`
  - `module1/script2.pyo`
  - `module2/script3.py`
  - `module2/script3.pyc`

```js
root@hecs-288034:/home/development/old_code# mkdir module1
root@hecs-288034:/home/development/old_code# lsmodule1
root@hecs-288034:/home/development/old_code# cd module1
root@hecs-288034:/home/development/old_code/module1# touch script1.py script1.pyc script2.py script2.pyo
root@hecs-288034:/home/development/old_code/module1# lsscript1.py script1.pyc script2.py script2.pyo
root@hecs-288034:/home/development/old_code/module1# 
root@hecs-288034:/home/development/old_code# mkdir module2 
root@hecs-288034:/home/development/old_code# lsmodule1 module2root@hecs-288034:/home/development/old_code# cd module2
root@hecs-288034:/home/development/old_code/module2# touch script2.py script3,pyc
root@hecs-288034:/home/development/old_code/module2# lsscript2.py script3,pycroot@hecs-288034:/home/development/old_code/module2#
```

1. 使用 `ls -l` 命令查看 `old_code` 目录下的所有文件和目录。

```
root@hecs-288034:/home/development/old_code# ls -l 
total 8 
drwxr-xr-x 2 root root 4096 May 23 18:43 module1 
drwxr-xr-x 2 root root 4096 May 23 18:44 module2
```

1. 手动删除 `old_code` 目录中所有的 `.pyc` 和 `.pyo` 文件

   ```
   root@hecs-288034:/home/development/old_code# rm module1/*.pyc
   root@hecs-288034:/home/development/old_code# ls
   
   root@hecs-288034:/home/development/old_code# rm module1/*.pyo
   root@hecs-288034:/home/development/old_code# 
   ```

2. 创建一个新的目录 `source_code`，并使用 `cp` 命令手动复制 `old_code` 目录中所有 `.py` 文件到 `source_code` 目录中，同时保持原始目录结构。

   ```
   root@hecs-288034:/home/development/old_code# cp module1/*.py ../source_code
   root@hecs-288034:/home/development/old_code# cp module2/*.py ../source_code
   root@hecs-288034:/home/development/old_code# 
   ```

3. 使用 `mkdir` 命令在 `source_code` 目录下创建两个新的子目录 `tests` 和 `docs`

   ```
   root@hecs-288034:/home/development/source_code# mkdir tests docs
   root@hecs-288034:/home/development/source_code# ls
   docs  script1.py  script2.py  tests
   ```

4. 使用 `touch` 命令在 `source_code/docs` 目录下创建两个空文件 `architecture.md` 和 `requirements.md`。

   ```
   root@hecs-288034:/home/development/source_code/docs# touch architecture.md requirements.md
   root@hecs-288034:/home/development/source_code/docs# ls
   architecture.md  requirements.md
   ```

5. 回到 `development` 目录的根目录，使用 `ls -R` 命令递归地列出整个 `development` 目录的结构。

   ```
   root@hecs-288034:/home/development# ls -R
   .:
   architecture.md  old_code  requirements.md  source_code  sourcs_code
   
   ./old_code:
   module1  module2
   
   ./old_code/module1:
   script1.py  script2.py
   
   ./old_code/module2:
   script3.py  script3,pyc
   
   ./source_code:
   docs  script1.py  script2.py  script3.py  tests
   
   ./source_code/docs:
   architecture.md  requirements.md
   
   ./source_code/tests:
   ```

6. 清理工作：删除整个 `old_code` 目录（包括其中的所有文件和子目录）。

```
root@hecs-288034:/home/development# rm -r old_code
root@hecs-288034:/home/development# ls
source_code
root@hecs-288034:/home/development# 
```

1. （额外任务）假设您发现 `source_code/module1` 目录下有一个名为 `unused_script.py` 的文件不再需要（尽管在这个示例中我们没有创建它，但假设它存在），请使用 `rm` 命令删除它。

   ```
   root@hecs-288034:/home/development# rm -r source_code/module1/unused_script.py
   root@hecs-288034:/home/development# 
   ```

作业：

假设您刚刚登录到一个Linux系统，并位于您的家目录（`~`）下。您需要完成以下一系列复杂的操作来组织和清理您的文件和目录。请按照顺序执行，并给出相应的命令。

1. **创建测试文件**：在家目录下创建三个文本文件，分别命名为`.hidden.txt`（隐藏文件）、`visible1.txt`和`visible2.txt`。

   ```
   cba@hecs-288034:~$ touch .hidden.txt visible1.txt visible2.txt
   cba@hecs-288034:~$ ls
   ```

2. **列出文件和目录**：列出家目录（`~`）下的所有文件和目录，包括隐藏文件，并查看其详细权限和属性。

   ```
   cba@hecs-288034:~$ ls -alR
   .:
   total 20
   drwxr-xr-x 2 cba  cba  4096 May 23 19:33 .
   drwxr-xr-x 5 root root 4096 May 23 19:31 ..
   -rw-r--r-- 1 cba  cba   220 May 23 19:31 .bash_logout
   -rw-r--r-- 1 cba  cba  3526 May 23 19:31 .bashrc
   -rw-r--r-- 1 cba  cba     0 May 23 19:33 .hidden.txt
   -rw-r--r-- 1 cba  cba   807 May 23 19:31 .profile
   -rw-r--r-- 1 cba  cba     0 May 23 19:33 visible1.txt
   -rw-r--r-- 1 cba  cba     0 May 23 19:33 visible2.txt
   cba@hecs-288034:~$ 
   ```

3. **创建工作区**：创建一个新的目录`work_area`，并在其中创建三个子目录：`project_a`、`project_b`和`docs`。

   ```
   cba@hecs-288034:~/work_area$ mkdir project_a porject_b docs
   cba@hecs-288034:~/work_area$ ls
   docs  porject_b  project_a
   ```

4. **移动文本文件**：将家目录下的所有`.txt`文件移动到`work_area/docs`目录中，并确保这些文件在移动后仍然是隐藏的（如果它们是隐藏的）。

   ```
   cba@hecs-288034:~$ mv *.txt work_area/docs
   ```

5. **创建新文件**：在`work_area/project_a`目录下创建一个新的文本文件`notes.txt`，并添加一些内容（例如：`echo "Initial notes for project A" > work_area/project_a/notes.txt`）。

   ```
   cba@hecs-288034:~$ echo "Initial notes for project A" >  work_area/project_a/notes.txt
   ```

6. **复制目录**：递归地复制`work_area/project_a`目录到`work_area/project_b`，并命名为`project_a_backup`。

   ```
   cba@hecs-288034:~/work_area/project_a$ cp notes.txt ../porject_b 
   ```

7. **列出文件并按大小排序**：列出`work_area/docs`目录下的所有文件，并按文件大小降序排列。

```
cba@hecs-288034:~/work_area$ ls -lhS docs
total 0
-rw-r--r-- 1 cba cba 0 May 23 19:33 visible1.txt
-rw-r--r-- 1 cba cba 0 May 23 19:33 visible2.txt
cba@hecs-288034:~/work_arek_area$ 
```

1. **删除所有文件**：删除`work_area/docs`目录下所有文件。

```
cba@hecs-288034:~/work_area$ rm -rf docs

cba@hecs-288034:~/work_area$ ls

porject_b project_a
```

1. **删除目录**：假设您不再需要`work_area/project_b`目录及其所有内容，请递归地强制删除它。

   ```
   cba@hecs-288034:~/work_area$ rm -r porject_b
   cba@hecs-288034:~/work_area$ ls
   project_a  
   ```

2. **清理空目录**：清理`work_area`目录，删除其中所有的空目录（注意：不要删除非空的目录）。

```
rm work_area/
```

1. **创建别名**：回到您的家目录，并创建一个别名`llh`，该别名能够列出当前目录下文件和目录的长格式列表，并以人类可读的格式显示文件大小（类似于`ls -lh`命令）。

```
cba@hecs-288034:~$ mkdir  -m 111 11h
cba@hecs-288034:~$ ls
11h  work_area
cba@hecs-288034:~$ 
```