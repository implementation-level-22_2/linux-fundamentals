### 一、申请SSL证书

1. **登录华为云控制台**：
   - 访问 [华为云官网](https://www.huaweicloud.com/)，点击“登录”，输入账号和密码。
2. **进入SSL证书管理**：
   - 在控制台主页，点击左侧菜单中的“安全与合规”，然后选择“SSL证书管理”。
3. **申请新证书**：
   - 点击“购买证书”按钮，选择适合的证书类型（如单域名、多域名或通配符证书），然后点击“立即购买”。
   - 填写证书信息，包括域名、申请者信息等。
   - 根据需要选择验证方式（如DNS验证、文件验证等）。
4. **完成支付**：
   - 确认订单信息，完成支付。
5. **域名验证**：
   - 根据选择的验证方式进行域名所有权验证。比如，如果选择DNS验证，需要根据提示在域名管理后台添加对应的DNS记录。
6. **下载证书**：
   - 验证通过后，证书颁发成功。可以在“SSL证书管理”页面中下载证书文件。

### 二、为网站设置SSL证书

#### 1. 使用Nginx配置SSL证书

1. **上传证书文件**：

   - 将下载的证书文件（一般包含一个证书文件和一个私钥文件）上传到服务器指定目录，如 `/etc/nginx/ssl/`。

2. **编辑Nginx配置文件**：

   - 打开Nginx配置文件，/etc/nginx/.con/配置文件
   - 添加或修改服务器块配置，启用HTTPS：

   ```shell
   自己的配置下再添加一个sever就好
   server {
       listen 443 ssl;
       server_name yourdomain.com;
   
       ssl_certificate /etc/nginx/ssl/config.com.crt;
       ssl_certificate_key /etc/nginx/ssl/config.com.key;
   
       ssl_protocols TLSv1.2 TLSv1.3;
       ssl_ciphers HIGH:!aNULL:!MD5;
   
       location / {
           root /var/www/html;
           index index.html index.htm;
       }
   }
   ```

3. **测试并重启Nginx**：

   - 测试Nginx配置文件是否正确：

     ```shell
     sudo nginx -t
     ```

   - 重启Nginx以应用新配置：

     ```shell
     sudo systemctl restart nginx
     ```