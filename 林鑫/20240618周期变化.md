### 练习题 1: 显示当前所有的环境变量

* 使用`printenv`或`env`命令来显示所有的环境变量。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ env
SHELL=/bin/bash
PWD=/home/qaq
LOGNAME=qaq
XDG_SESSION_TYPE=tty
MOTD_SHOWN=pam
HOME=/home/qaq
LANG=zh_CN.UTF-8
LS_COLORS=rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=00:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.avif=01;35:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:*~=00;90:*#=00;90:*.bak=00;90:*.old=00;90:*.orig=00;90:*.part=00;90:*.rej=00;90:*.swp=00;90:*.tmp=00;90:*.dpkg-dist=00;90:*.dpkg-old=00;90:*.ucf-dist=00;90:*.ucf-new=00;90:*.ucf-old=00;90:*.rpmnew=00;90:*.rpmorig=00;90:*.rpmsave=00;90:
SSH_CONNECTION=112.5.195.104 1299 172.24.206.176 22
XDG_SESSION_CLASS=user
TERM=xterm
USER=qaq
SHLVL=1
XDG_SESSION_ID=16700
XDG_RUNTIME_DIR=/run/user/1000
SSH_CLIENT=112.5.195.104 1299 22
LC_ALL=zh_CN.UTF-8
PATH=/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games
DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus
SSH_TTY=/dev/pts/2
_=/usr/bin/env
```

### 练习题 2: 显示`HOME`环境变量的值

* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ echo $HOME
/home/qaq
```

### 练习题 3: 临时设置一个新的环境变量

* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ export MY_AGE=18
```

### 练习题 4: 显示新设置的环境变量

* 使用`echo`命令来显示`MY_AGE`的值。

```bash
echo $MY_VARIABLE
```

### 练习题 5: 在新的shell会话中检查环境变量

* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？
  
  ```bash
  qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ MY_AGE
  -bash: MY_AGE: 未找到命令
  临时的环境变量，只能在当前登录用户使用
  ```

### 练习题 6: 修改`PATH`环境变量

* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ PATH=$PATH:/home/qaq
```

将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ PATH=/tmp:$PATH
```

### 练习题 7: 验证`PATH`的修改

* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ echo $PATH
/tmp:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games

```

### 练习题 8: 永久设置环境变量

* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。

例如，对于bash shell，你可以使用：

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ vim .basher
```

如何让`MY_NAME`生效，并验证

```
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ source .basher
```

### 练习题 9: 清理

* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ exit
```

### 练习题 10: 修改默认器

* 使用`EDITOR`变量，修改你默认的编辑器为nano。

```bash
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ EDITOR=/usr/bin/nano
```

### 练习题 11: 修改语言

* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。

```
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ LANG=zh_CN.utf8
```

- 使用`LANGUAGE`变量，让你的命令提示为中文

```
qaq@iZf8z8ck55p8r6n3i00zhuZ:~$ LANGUAGE=zh_CN.utf8
```

### 周期任务练习

执行在家目录touch a.txt

```bash
#1. 每天3:00执行一次
* 3 * * * touch /home/qaq/a.txt
#2. 每周六2:00执行 
* 2 * * 6 touch /home/qaq/a.txt
#3. 每周六1:05执行
5 1 * * 6 touch /home/qaq/a.txt
#4. 每周六1:25执行 
25 1 * * 6 touch /home/qaq/a.txt
#5. 每天8：40执行 
40 8 * * * touch /home/qaq/a.txt
#6. 每天3：50执行 
50 3 * * * touch /home/qaq/a.txt
#7. 每周一到周五的3：40执行 
40 3 W * * touch /home/qaq/a.txt
#8. 每周一到周五的3：41开始，每10分钟执行一次 
41,51 3 W * * touch /home/qaq/a.txt
1-59/10 4-23 W * * touch /home/qaq/a.txt
#9. 每天的10：31开始，每2小时执行一次
31 10-23/2 * * * touch /home/qaq/a.txt
#10. 每周一到周三的9：30执行一次
30 9 * * 1-3 touch /home/qaq/a.txt
#11. 每周一到周五的8：00，每周一到周五的9：00执行一次
0 8-9 W * * touch /home/qaq/a.txt
#12. 每天的23：45分执行一次
45 23 * * * touch /home/qaq/a.txt
#13. 每周三的23：45分执行一次
45 23 * * 3 touch /home/qaq/a.txt
#14. 每周一到周五的9：25到11：35、13：00到15：00之间，每隔10分钟执行一次
25-59/10 9 W * * touch /home/qaq/a.txt
5-59/10 10 W * * touch /home/qaq/a.txt
5-35/10 11 W * * touch /home/qaq/a.txt
0-59/10 13-15 W * * touch /home/qaq/a.txt
#15. 每周一到周五的8：30、8：50、9：30、10：00、10：30、11：00、11：30、13：30、14：00、14：30、5：00分别执行一次
30,50 8 W * * touch /home/qaq/a.txt
30 9-11,13,14  W * * touch /home/qaq/a.txt
0 5,10,11,13,14  W * * touch /home/qaq/a.txt
#16. 每天16：00、10：00执行一次
0 10,16 * * * touch /home/qaq/a.txt
#17. 每天8：10、16：00、21：00分别执行一次
10 8  * * * touch /home/qaq/a.txt
0 16,21  * * * touch /home/qaq/a.txt
#18. 每天7：47、8：00分别执行一次
47 7  * * * touch /home/qaq/a.txt
0 8  * * * touch /home/qaq/a.txt
```
