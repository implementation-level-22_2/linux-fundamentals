绝对路径：完整的描述文件位置的路径就是绝对路径,不需要知道其他的信息就可以根据绝对路径来判断出文件的位置。
从根目录 `/` 开始的完整路径，用于唯一确定文件或目录的位置。
是以`/`根目录开头，而且与当前的工作目录无关。
例如：/home/shi02/docs/1.txt

相对路径：相对于当前工作目录的路径，用于在当前目录的基础上指向文件或目录。
是不以`/`根目录为开头，与当前的工作目录相关。
./表示当前目录
../表示上一级目录
../../表示上一级目录的上一级目录



相关和目录可自行创建后再操作

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c

   ```
    touch exam.c
     cp ../exam.c shiyan.c
   ```

2. 在任何目录下回到用户主目录？

   ```
   cd ~
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？

   ```
     ls -al ./test
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar

   ```
      touch ./test2/{1,2,3,4,5}.txt
      tar -cvzf ./test2/hailiang.tar.gz ./test2/{1..5}.txt
   ```
   
5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？

   ```
   touch file1.txt
   mv file1.txt file2.txt
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？

   ```
    vim bbbb.txt
    按下Esc键，然后输入:wq保存并退出vim编辑器
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？

   ```
      cp ../*.txt *.doc *.bak ./test
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。

   ```
      mv file1.txt ../docs/
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。

   ```
      cp file2.txt ../backup/
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。

    ```
      mv ../docs/* ../archive/
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。

    ```
       cp ../photos/* ../backup/
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。

    ```
        mv ../docs/report.doc ../papers/final_report.doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。

    ```
        touch ../docs/notes.txt
        cp ../docs/notes.txt ../backup/
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。

    ```
        cp -r ../images/*.jpg ../photos/
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。

    ```
     cp ../docs/file1.txt ../docs/file2.txt ../backup/
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中。

    ```
        cp ../docs/*.txt ../text_files/
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。

    ```
        cp -f ../docs/* ../temp/
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。

    ```
        mv -v ../docs/* ../archive/
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。

    ```
        cp ../docs/ ../backup
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。

    ```
        mv ../docs/!(temp_*) ../backup
    
    ```

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。

    ```
       mv -bs report.txt_bak ../docs/report.txt ../archive
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

```
mv -n ../docs/*.pdf ../pdf_files
```

