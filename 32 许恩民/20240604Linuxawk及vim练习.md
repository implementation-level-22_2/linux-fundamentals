作业：

1. vi 编辑器有几种模式?

   ```
   vi 编辑器有三种主要模式：
   
   命令模式：这是 vi 启动后的默认模式，用于执行各种命令，如移动光标、删除文本、复制文本等。
   插入模式：在此模式下，可以输入和编辑文本。
   可视模式：用于选择文本块，可以执行诸如复制、删除等操作
   ```

   

2. 如何进入 vi 编辑器的插入模式

   ```
   在命令模式下，按下以下键之一可以进入插入模式：
   
   i：在光标前插入
   I：在行首插入
   a：在光标后插入
   A：在行尾插入
   o：在当前行下方插入一个新行
   O：在当前行上方插入一个新行
   ```

   

3. 如何进入 vi 编辑器的可视化模式

   ```
   在命令模式下，按下以下键之一可以进入可视模式：
   注：当你第一次启动 vi 或 vim 时，编辑器默认就是在命令模式下。
   v：进入可视模式（选择字符）
   V：进入行可视模式（选择整行）
   Ctrl-v：进入块可视模式（选择矩形块）
   ```

   

4. 在 vi 编辑器中如何复制一行

   ```
   在命令模式下，输入 yy 或 Y 可以复制当前行。
   ```

   

5. 在 vi 编辑器中如何进行粘贴

   ```
   在命令模式下，输入 p 可以在光标后粘贴，输入 P 可以在光标前粘贴。
   ```

   

6. 如何删除从 3 行到 15 行的所有数据

   ```
   在命令模式下，输入 :3,15d 可以删除从第 3 行到第 15 行的所有行。
   ```

   

7. vim练习：

   - 光标移动练习，命令模式下：

     - 单位级 h j k l
     - 单词级 w e b
     - 块级 gg G 0 ^ $ H M L ngg nj nk

     把下列句子按照第一句的正确顺序修改好并把多余的空行删除

     ```
     this is a simple easy vim tutorial
     
     tutorial simple a easy this vim is
     is this tutorial vim simple a easy
     
     
     tutorial vim this is a easy simple
     tutorial easy vim simple a this is
     simple a vim easy tutorial is this
     
     tutorial is easy vim a simple this
     
     
     vim simple this tutorial a easy is
     a vim tutorial simple easy is this
     
     
     easy a simple vim is tutorial this
     vim tutorial is a easy simple this
     a this vim tutorial is easy simple
     this tutorial simple easy a is vim
     
     
     easy tutorial this simple a is vim
     a tutorial easy is this simple vim
     
     a tutorial vim is easy this simple
     simple this easy is vim tutorial a
     
     this tutorial is a easy simple vim
     vim is tutorial simple this easy a
     
     vim is simple this tutorial easy a
     easy a simple is vim this tutorial
     vim is tutorial simple a easy this
     this vim is tutorial simple easy a
     ```

     先敲出以下代码，然后修正以下代码中的错误单词、重复单词、错误格式、多余行，修改函数名为 typing 并为定时器添加 300 毫秒延迟

     ```
     const bbb = () => {
     // this is is a description
     //
     //   another descriptttion 
     const timer   = setTimeout(( ) => {
         console.log(that) alert('cool!')
         // awosome man !
     })
     }
     ```

     尝试在下面的文本中进行复制粘贴练习

     ```
     删除这一行
     粘贴到这一行下面
     剪切 ABC 并把它粘贴到 XYZ 前面，使这部分内容看起来像
     剪切 并把它粘贴到 ABC XYZ 前面。
     ```

     尝试修改下列文本的大小写

     ```
     Change this line to UPPERCASE, THEN TO lowercase.
     ```

     按下面的说明进行操作

     ```
     按 dd 删除本行
     按 . 重复删除操作
     2. 再删除两行
     这行也没了
     p 把刚才删掉的粘回来
     3. 又多出 6 行
     ```

     左缩进、右缩进练习

     ```
     在这一行上依次按 3>>，<< 和 <G 看看效果
     打酱油行，我从你的全世界走过
     ```

1. 只显示/etc/passwd的账户

   ```bash
   Time@hecs-148338:~$ awk -F: '{print $1}' /etc/passwd
   ```

2. 只显示/etc/passwd的账户和对应的shell，并在第一行上添加列名用户制表符shell，最后一行添加----------------

   ```bash
   Time@hecs-148338:/etc$ awk -F: 'BEGIN{print "用户名\tshell"} {print $1 "\t" $7} END{print"----------------"}' /etc/passwd
   ```

3. 搜索/etc/passwd有关键字root的所有行

   ```bash
   Time@hecs-148338:~$ awk -F, '/root/'  /etc/passwd
   ```

4. 统计/etc/passwd文件中，每行的行号，每列的列数，对应的完整行内容以制表符分隔

   ```bash
   awk -F: '{print NR "\t" NF "\t" $0}' /etc/passwd
   ```

5. 输出/etc/passwd文件中以nologin结尾的行

   ```bash
   awk -F: '/nologin$/' /etc/passwd
   ```

6. 输出/etc/passwd文件中uid字段小于100的行

   ```bash
   Time@hecs-148338:~$ awk -F: '$3<100' /etc/passwd
   ```

7. /etc/passwd文件中gid字段大于200的，输出该行第一、第四字段，第一，第四字段并以制表符分隔

   ```bash
   Time@hecs-148338:~$ awk -F: '$4>200 {print $1 "\t" $4}' /etc/passwd
   ```

8. 输出/etc/passwd文件中uid字段大于等于100的行 

   ```bash
   Time@hecs-148338:~$ awk -F: '$3>=100' /etc/passwd
   ```

   



