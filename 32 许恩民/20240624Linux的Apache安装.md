**关闭`nginx`**

```bash
systemctl stop nginx
```

**查看`nginx`进程是否关闭**

```bash
ps aux | grep nginx
```

**系统更新**

```bash
sudo agt update
```

**安装`Debian`系统的`apache2`**

```bash
sudo apt install apache2
```

**启动`apache2`**

```bash
systemctl start apache2
```

**（如果报错：修改`/etc/apache2/apache2.conf`文件里面的最后一行注释掉`IncldeOptionl conf.d/*.conf`）**

**查看`apache2`状态**

```bash
systemctl status apache2.service
```