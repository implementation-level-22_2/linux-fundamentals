**1. 初始文件结构创建**

在您的家目录（`~`）下创建一个名为`my_projects`的目录，并在该目录下创建`proj_a`、`proj_b`两个子目录，同时在`proj_a`中创建`file1.txt`和`file2.jpg`两个文件。

```
mkdir -p ~/my_projects/proj_a
touch ~/my_projects/proj_a/file1.txt
touch ~/my_projects/proj_a/file2.jpg
mkdir ~/my_projects/proj_b
```

**2. 复制同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_a/file1_copy.txt`。

```
cp ~/my_projects/proj_a/file1.txt ~/my_projects/proj_a/file1_copy.txt

```

**3. 复制不同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_b/`，并重命名为`copied_file1.txt`。

```
cp ~/my_projects/proj_a/file1.txt ~/my_projects/proj_b/copied_file1.txt

```

**4. 复制同路径目录**

复制`my_projects/proj_a`到`my_projects/proj_a_copy`。

```
cp -r ~/my_projects/proj_a ~/my_projects/proj_a_copy
```

**5. 复制不同路径目录**

将`my_projects/proj_a`目录复制到您的家目录（`~`），并重命名为`home_proj_a`。

```
cp -r ~/my_projects/proj_a ~/home_proj_a
```

**6. 复制文件和目录到不同路径**

将`my_projects/proj_a/file2.jpg`和`my_projects/proj_b`目录一起复制到`~/archive`目录下。

```
cp ~/my_projects/proj_a/file2.jpg ~/archive/
cp -r ~/my_projects/proj_b ~/archive/
```

**7. 批量复制**

使用shell脚本或命令行工具，将`my_projects`目录下的所有`.txt`文件复制到`~/text_files`目录下。

```
mkdir ~/text_files
cp ~/my_projects/*.txt ~/text_files/
```

**8. 交互式复制文件**

编写一个交互式脚本，提示用户输入要复制的`.jpg`文件的名称，然后将其从`my_projects`目录复制到`~/images`目录下。

```
echo "请输入要复制的 .jpg 文件名："
read filename
cp ~/my_projects/*.jpg ~/images/
```

**9. 显示复制进度**

使用`rsync`命令将`my_projects/proj_a`目录及其内容复制到`~/sync_folder`，并显示复制进度。

```
rsync -avh --progress ~/my_projects/proj_a ~/sync_folder
```

**10. 条件复制**

只有当`my_projects/proj_a/file1.txt`比`~/old_files/file1.txt`新或不存在时，才复制该文件。

```
cp -u ~/my_projects/proj_a/file1.txt ~/old_files/
```

**11. 复制时备份**

在复制`my_projects/proj_a/file1.txt`到`~/backup`目录之前，如果`~/backup/file1.txt`已存在，则先创建一个带有`.bak`后缀的备份文件。

```
if [ -e ~/backup/file1.txt ]; then
    cp ~/backup/file1.txt ~/backup/file1.txt.bak
fi
cp ~/my_projects/proj_a/file1.txt ~/backup/
```

**12. 重命名文件或目录**

将`my_projects/proj_a/file1.txt`重命名为`my_projects/proj_a/new_file1.txt`，如果`new_file1.txt`已存在，则提示用户重新输入新名称。

```
打开一个文本编辑器（如 nano 或 vim）。
复制以下代码并保存为 rename_file.sh：
bash
复制代码
#!/bin/bash

source_file="$HOME/my_projects/proj_a/file1.txt"
target_file="$HOME/my_projects/proj_a/new_file1.txt"

while [ -e "$target_file" ]; do
    echo "文件 $target_file 已存在。"
    read -p "请输入新的文件名（不含路径）: " new_filename
    target_file="$HOME/my_projects/proj_a/$new_filename"
done

mv "$source_file" "$target_file"

if [ $? -ne 0 ]; then
    echo "重命名失败，请检查文件路径和名称。"
else
    echo "文件已成功重命名为 $target_file。"
fi
保存并关闭文件。
打开终端并导航到保存 rename_file.sh 的目录。
为脚本添加执行权限：
bash
复制代码
chmod +x rename_file.sh
运行脚本：
bash
复制代码
./rename_file.sh
```

**13. 移动并重命名文件或目录**

将`my_projects/proj_b`目录移动到`~/moved_projects`并重命名为`moved_proj_b`，如果`moved_proj_b`已存在，则先备份该目录再进行移动和重命名。

```
mv "$source_file" "$target_file"

if [ $? -ne 0 ]; then
    echo "重命名失败，请检查文件路径和名称。"
else
    echo "文件已成功重命名为 $target_file。"
fi

```

