## 笔记

```
常见的执行Linux命令的格式： 命令名(command)  选项(options)  参数(arguments)
命令名：表达的是想要做的事情，例如创建用户、查看文件、重启系统等操作。
选项：用于对命令进行调整
参数：一般指要处理的文件、目录、用户等资源名称。

Linux命令参数的长格式与短格式
长格式：man --help
短格式：man -h
```

### 目录命令

```
/  是指根目录：就是所有目录最顶层的目录

./ 表示当前目录：
./ 一般需要和其他文件夹或者文件结合使用，指代当前目录下的东西
cd ./文件夹   切换到当前目录的某个文件夹
.. 表示上级目录：
cd .. 切换到上级目录
cd ../文件夹    切换到上级目录中的某个文件夹
```

### 常用命令

```
cat(concatenate):它的主要作用是用于查看和连接文件。
语法：cat [选项] [文件]
ls（list directory contents）：用于显示指定工作目录下之内容（列出目前工作目录所含的文件及子目录)
语法：ls [-alrtAFR] [name...]
ls / （以下是/根目录所有目录）
bin               dev   lib         media  net   root     srv  upload  www
boot              etc   lib64       misc   opt   sbin     sys  usr
home  lost+found  mnt    proc  selinux  tmp  var
ls /bin 则显示bin目录下所有文件目录

mv（move file）：用来为文件或目录改名、或将文件或目录移入其它位置。
语法：mv [options] source（源文件或目录） dest（目标文件或目录）
```

### 按键和它的作用

```
 按键         作用
空格键      向下翻一页
PaGe down  向下翻一页
PaGe up    向上翻一页 
home       直接前往首页 
end        直接前往尾页
 /      从上至下搜索某个关键词
 ?      从下至上搜索某个关键词
 n      定位到下一个搜索到的关键词
 N      定位到上一个搜索到的关键词
 q      退出帮助文档
 
双击Tab键能够实现对命令、参数戒文件的内容补全；
Ctrl+C用于终止当前迚程的运行；
Ctrl+D表示键盘输入结束；
Ctrl+l会清空当前终端中已有的内容（相当亍清屏操作）。
```