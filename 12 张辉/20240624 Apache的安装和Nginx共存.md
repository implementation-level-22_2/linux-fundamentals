# 作业

## 1.安装Apache

对于debain系统：

```bash
sudo apt update
sudo apt install apache2
```

配置文件存在于：/etc/apache2 该目录下

## 2.nginx共存

要想Apache和nginx同时运行，需要保证端口不会冲突

要么修改Apache的监听端口要么修改nginx的监听端口

```bash
vim /etc/nginx/conf.d/*.conf #修改nginx网站的监听端口
vim /etc/apache2/ports.conf /etc/apache2/sites-enabled/*.conf #修改Apache网站的监听端口
```
