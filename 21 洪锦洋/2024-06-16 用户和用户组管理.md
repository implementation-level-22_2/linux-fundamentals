## 用户和用户组管理

- 用户管理
  - 添加账户 useradd -m -s /bin/bash db2
  - 删除账户 userdel -r db2
  - 修改账户 usermod [选项同添加账户] db2
  - 查看帐户 getent passwd db2
- 用户组管理
  - 添加用户组 groupadd stu1
  - 删除用户组 groupdel stu1
  - 添加用户到用户组 usermod -aG stu1 db2
- 密码管理
  - 设置密码 passwd
- sudo权限管理
  - usermod -aG sudo xxx
  - 修改/etc/sudoers文件
- 登录权限管理
- 用户环境管理
  - 用户配置文件 ： .bashrc
  - 用户环境变量： PATH、𝑃𝐴𝑇𝐻、HOME、$SHELL

## 文件与目录的权限管理

- 文件类型

  - -普通文件
  - d目录
  - l连接文件
  - b c s p 等其它文件

- 基本权限

  - r 读
  - w 写
  - x 执行

- 权限表示

  - 字符表示
  - 数字表示
    - 4 代表 r
    - 2 代表 w
    - 1 代表 x
  - 权限修改
    - chmod 修改权限
    - chown 修改拥有者
    - chgrp 修改所属组
  - 权限的继承
    - 目录权限下文件和目录的影响
    - umask
  - 特权权限
    - SUID
    - SGID
    - SBIT
  - 隐藏权限
    - i
      - 用在文件上，无法对该文件进行任何修改
      - 用在目录上，无法在里面创建新文件，但可以修改已有文件
    - a
      - 除了可追加内容外，无法进行其它修改
    - 隐藏权限的操作
      - 修改隐藏权限 chattr
        - chattr +i XXX
        - chattr -i XXX
      - 查看隐藏权限
        - lsattr

  可以使用的选项有：

  - -g GID 指定新用户组的组标识号（GID）。
  - -o 一般与-g选项同时使用，表示新用户组的GID可以与系统已有用户组的GID相同。

  #### 实例1：

  ```
  # groupadd group1
  ```

  此命令向系统中增加了一个新组group1，新组的组标识号是在当前已有的最大组标识号的基础上加1。

  #### 实例2：

  ```
  # groupadd -g 101 group2
  ```