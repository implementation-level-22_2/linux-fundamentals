##  cat

正向显示整个文件 -n 加行号

## tac

反向显示整个文件 同上

与cat都是一次性加载，不适合大文件，变慢

## more

分页显示文件，每次显示一屏

一次性加载

空格翻页，回车下一行，q退出

## less

分页显示

上下键/pageuppagedown滚动翻页，/搜索，n下一个匹配项目，N上一个匹配，q退出

-N 显示行号

-M 长格式

## head

默认显示前10行内容

-n 数字 前x行内容

## tail

默认显示后10行内容

-n 同上

-f 实时更新内容

## nl

不给空格行加行号的显示

# 练习

**以下所有操作都在家目录执行：**

### 

1. **操作题一**：使用 `cat` 命令显示 `/etc/passwd` 文件的内容。

   ```
   cat /etc/passwd
   ```

2. **操作题二**：将文件 `/etc/passwd` 的内容复制到 `passwd_bak.txt` 文件中，但不使用 `cp` 命令。

   ```
   cat /etc/passwd >passwd_bak.txt
   ```

3. **操作题三**：新建两个文件 `file1.txt` 和 `file2.txt`，分别写一些不同的内容，再将这两个文件的内容合并到一个新的文件 `file3.txt` 中。

   ```
   echo '曾经沧海难为水'> file1.txt
   echo '除却巫山不是云'> file2.txt
   cat file1.txt file2.txt > file3.txt
   ```

### 

1. 使用命令从尾部开始显示 `bigfile.txt` 文件的内容。

   ```
   tac bigfile.txt 
   ```

2. 尝试找出 `bigfile.txt` 文件的最后一行内容，要使用 `tac` 命令。

   ```
   tac bigfile.txt | head -n 1
   ```

3. 查看 `bigfile.txt` 文件的最后5行内容。

   ```
   tac bigfile.txt -n 5
   ```

4. 倒序查看 `bigfile.txt` 文件的最后5行内容。

   ```
   tac bigfile.txt | tail -n 5
   ```

### 

1. **操作题一**：使用 `more` 命令查看 `bigfile.txt` 文件的内容，并在查看过程中使用空格键翻页。

   ```
   more bigfile.txt 空格
   ```

2. **操作题二**：在 `more` 命令的查看过程中，如何使用回车键来逐行查看文件内容？

   ```
   more bigfile.txt 回车
   ```

3. **操作题三**：如何在 `more` 命令中搜索`bigfile.txt`特定字符串（例如 "error"）并跳转到下一个匹配项？

   ```
   more bigfile.txt /error
   ```

### `less` 命令操作题

1. **操作题一**：使用 `less` 命令查看 `bigfile.txt` 文件，并快速定位到文件的末尾。

   ```
   less bigfile.txt
   ```

2. **操作题二**：在 `less` 命令中，如何向上和向下滚动文件内容？

   ```
   pageup
   pagedown
   ```

3. **操作题三**：在 `less` 命令中，如何搜索`bigfile.txt`一个特定的函数名（例如 `def my_function`），并查看所有匹配项？

   ```
   less bigfile.txt /def my_function +
   ```

### `head` 命令操作题

1. **操作题一**：使用 `head` 命令显示 `bigfile.txt` 文件的前5行内容。

   ```
   head bigfile.txt -n 5
   ```

2. **操作题二**：将 `bigfile.txt` 的前20行内容保存到 `bigfile_20.txt` 文件中。

   ```
   head bigfile.txt -n 20 > bigfile_20.txt
   ```

3. **操作题三**：如何结合 `head` 和 `grep` 命令来查找 `bigfile.txt` 文件中以 "A" 开头的前10行？

   ```
    grep 'A' bigfile.txt | head
   ```

### `tail` 命令操作题

1. **操作题一**：使用 `tail` 命令显示 `bigfile.txt` 文件的最后20行内容。

   ```
   tail bigfile.txt -n 20
   ```

2. **操作题二**：如何实时跟踪一个正在写入的日志文件（如 `bigfile.txt`）的最后10行内容？

   ```
   tail -n 10 -f bigfile.txt
   ```

3. **操作题三**：在 `tail` 命令中，如何反向显示文件的最后10行内容（即按从旧到新的顺序显示）？

   ```
   tail -n 10 bigfile.txt | tac
   ```

### 综合题

**综合题**：假设你有一个非常大的日志文件 `bigfile.txt`，你需要找出所有包含 "ERROR" 字符串的行，并查看这些行及其之前的两行内容。

```
 cat bigfile.txt | grep 'ERROR' -B2
```