# 笔记

## Liunx系统  

```js
liunx操作系统内核 一般指GNU/Linux（单独的Linux内核并不可直接使用，一般搭配GNU套件，故得此称呼），是一种免费使用和自由传播的类UNIX操作系统，它主要受到Minix和Unix思想的启发，是一个基于POSIX的多用户、多任务、支持多线程和多CPU的操作系统。

Ubuntu是一个以桌面应用为主的Linux发行版操作系统，Ubuntu基于Debian发行版和GNOME桌面环境。
广义的Debian是指一个致力于创建自由操作系统的合作组织及其作品，由于Debian项目众多内核分支中以Linux宏内核为主，而且Debian开发者所创建的操作系统中绝大部分基础工具来自于GNU工程，因此“Debian”常指DebianGNU/Linux。

CentOS（Community Enterprise  Operating System，中文意思是社区企业操作系统）是Linux发行版之一，是免费的、开源的、可以重新分发的开源操作系统 。

二、Linux的安装

安装VMware pro 17 虚拟机
安装Debian 12.5（下载源）
```

1. 查看IP地址 

```
 ip addr show 	// 默认的查看命令
```

  2.显示或设置网络设备

```js
ifconfig	 // net-tools组件包
```

  3.更新软件库

```js
apt-get update // 更新软件库
```

4.安装net-tools组件包

```js
apt-get install net-tools 	//安装net-tools组件包
```

5.安装vim编辑器

```js
apt-get install vim 	// 安装vim编辑器，默认的是vi编辑器
```

6.安装SSH服务器

```js
apt-get install ssh   // 安装SSH服务器
//默认情况下，仅普通用户登录
vim /etc/ssh/sshd_cofnig //修改SSH权限
-----
    Port：修改端口号(22)
	PermitRootLogin：启用/禁用以Root身份进行登录(yes)
    PasswordAuthentication：启用/禁用用密码进行身份验证(yes)
------
/etc/init.d/ssh restart  //重启SSH服务器，使命令生效
```

7.安装sudo管理指令

```js
su root 	//切换root管理员模式
apt-get install sudo 	//安装sudo管理指令
sudo vim /etc/sudoers 	//修改用户权限

    root ALL=(ALL:ALL) ALL
    用户名 ALL=(ALL:ALL) ALL   //为用户添加权限
    :wq! 	//强制保存并退出

sudo ls		//测试sudo命令是否生效