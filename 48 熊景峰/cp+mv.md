#  cp+mv

## cp复制文件或目录

cp 源文件 目标文件 复制文件

cp 源文件 目标目录 复制文件到目标目录下

cp -r 源 目标 递归的连目录一起复制

cp -i 如果目标文件存在会询问是否覆盖

cp -p 保留源文件的属性，例如时间戳、权限

cp -v 显示进度信息

cp -u 当源文件比较新的时候才复制

cp -f 强制 不提示直接覆盖

cp -b 复制产生备份文件 尾标~

cp -b -S _bak 指定备份时产生什么后缀的备份文件

## mv移动文件目录 重命名

mv 源 目标 移动

mv 源名 新名 重命名

文件——文件

 已存在：覆盖

 不存在：重命名

文件——目录

 目录存在：

 文件存在：覆盖

 文件不存在：移动

 目录不存在：重命名

目录——目录

 存在：移动

 不存在：重命名

目录——文件

 存在：覆盖

 不存在：重命名

# 练习1

### 练习题 1：复制文件

假设你有两个目录：`/home/user/docs` 和 `/home/user/backup`。

1. 将`/home/user/docs`目录下的`file1.txt`复制到`/home/user/backup`目录下。

   ```
   cp /home/user/docs/file1.txt /home/user/backup/
   ```

2. 复制`/home/user/docs`目录下的所有`.txt`文件到`/home/user/backup`目录下。

   ```
   cp /home/user/docs/*.txt /home/user/backup/
   ```

### 练习题 2：复制文件夹及其内容

1. 假设`/home/user/photos`是一个包含多个图片文件夹的目录，请复制整个`/home/user/photos`目录及其所有内容到`/home/user/archive`目录下。

   ```
   cp -r /home/user/photos/ /home/user/archive/
   ```

### 练习题 3：移动文件

1. 将`/home/user/docs`目录下的`file2.docx`移动到`/home/user/papers`目录下。

   ```
   mv /home/user/docs/file2.docx /home/user/papers/
   ```

2. 如果`/home/user/papers`目录下已经有一个名为`file2.docx`的文件c，请确保移动操作会询问你是否要覆盖它。

   ```
   mv -i /home/user/docs/file2.docx /home/user/papers/
   mv: overwrite '/home/user/papers/file2.docx'? y
   ```

### 练习题 4：重命名文件

1. 将`/home/user/docs`目录下的`oldname.pdf`重命名为`newname.pdf`。

   ```
   mv /home/user/docs/oldname.pdf newname.pdf
   ```

### 练习题 5：结合使用

1. 复制`/home/user/docs`目录下的`report.md`到`/home/user/temp`，然后将复制过去的文件重命名为`temp_report.md`。

   ```
    cp /home/user/docs/report.md  /home/user/temp/temp_report.md
   ```

2. 将`/home/user/docs`目录下所有以`.doc`结尾的文件移动到`/home/user/processed`，并且如果这些文件在目标目录中已经存在，则不覆盖它们。

   ```
   mv -b /home/user/docs/*.doc /home/user/processed/
   ```

### 练习题 6：使用通配符

1. 复制`/home/user/docs`目录下所有以`.jpg`或`.png`结尾的图片文件到`/home/user/images`目录。

   ```
   cd docs/
   cp *.jpg *.png /home/user/images/
   ```

### 练习题 7：详细输出

1. 使用`cp`命令复制文件时，显示详细的复制信息。

   ```
   cp -v 57.jpg /home/user/images/
   '57.jpg' -> '/home/user/images/57.jpg'
   ```

2. 使用`mv`命令移动文件时，显示详细的移动信息。

   ```
   mv -v 59.jpg 5.jpg /home/user/images/
   renamed '59.jpg' -> '/home/user/images/59.jpg'
   renamed '5.jpg' -> '/home/user/images/5.jpg'
   ```

### 练习题 8：更新文件

1. 如果`/home/user/backup`目录中已经有一个与`/home/user/docs`目录下的`file1.txt`同名的文件，并且`/home/user/docs/file1.txt`的内容是更新的，请使用`cp`命令将更新的文件复制到备份目录，并覆盖旧文件（假设你知道旧文件可以被安全地覆盖）。

   ```
   cp -u /home/user/docs/file1.txt  /home/user/backup/
   ```

# 练习2

**1. 初始文件结构创建**

在您的家目录（`~`）下创建一个名为`my_projects`的目录，并在该目录下创建`proj_a`、`proj_b`两个子目录，同时在`proj_a`中创建`file1.txt`和`file2.jpg`两个文件。

```
mkdir my_projects
cd my_projects
mkdir proj_a proj_b
cd proj_a
touch file1.txt file2.jpg
```

**2. 复制同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_a/file1_copy.txt`。

```
cp my_projects/proj_a/file1.txt  my_projects/proj_a/file1_copy.txt
```

**3. 复制不同路径文件**

将`my_projects/proj_a/file1.txt`复制到`my_projects/proj_b/`，并重命名为`copied_file1.txt`。

```
cp my_projects/proj_a/file1.txt my_projects/proj_b/copied_file1.txt
```

**4. 复制同路径目录**

复制`my_projects/proj_a`到`my_projects/proj_a_copy`。

```
cp -r my_projects/proj_a my_projects/proj_a_copy
```

**5. 复制不同路径目录**

将`my_projects/proj_a`目录复制到您的家目录（`~`），并重命名为`home_proj_a`。

```
cp -r my_projects/proj_a /home/524/home_proj_a
```

**6. 复制文件和目录到不同路径**

将`my_projects/proj_a/file2.jpg`和`my_projects/proj_b`目录一起复制到`~/archive`目录下。

```
cp -r my_projects/proj_a/file2.jpg my_projects/proj_b /home/524/archive
```

**7. 批量复制**

将`my_projects`目录下的所有`.txt`文件复制到`~/text_files`目录下。

```
cp my_projects/*.txt text_files/
```

**8. 交互式复制文件**

新建一个`test.jpg`文件，然后将其从`my_projects`目录复制到`~/images`目录下。如果该文件已存在，则提示用户覆盖。

```
cp -i test.jpg /home/524/images/
cp: overwrite '/home/524/images/test.jpg'? y
```

**9. 显示复制进度**

使用`rsync`命令将`my_projects/proj_a`目录及其内容复制到`~/sync_folder`，并显示复制进度。

```
apt-get install rsync
rsync -av -progress my_projects/proj_a /home/524/sync_folde/
sending incremental file list
proj_a/
proj_a/file1.txt
proj_a/file1_copy.txt
proj_a/file2.jpg

sent 263 bytes  received 77 bytes  680.00 bytes/sec
total size is 0  speedup is 0.00
```

**10. 条件复制**

只有当`my_projects/proj_a/file1.txt`比`~/old_files/file1.txt`新或不存在时，才复制该文件。

```
cp -u my_projects/proj_a/file1.txt old_files/file1.txt
```

**11. 复制时备份**

在复制`my_projects/proj_a/file1.txt`到`~/backup`目录之前，如果`~/backup/file1.txt`已存在，则先创建一个带有`.bak`后缀的备份文件。

```
cp -b -S _bak my_projects/proj_a/file1.txt backup/file1.txt 
```

**12. 重命名文件或目录**

将`my_projects/proj_a/file1.txt`重命名为`my_projects/proj_a/new_file1.txt`，如果`new_file1.txt`已存在，则提示用户重新输入新名称。

**13. 移动并重命名文件或目录**

将`my_projects/proj_b`目录移动到`~/moved_projects`并重命名为`moved_proj_b`，如果`moved_proj_b`已存在，则先备份该目录再进行移动和重命名。

# 作业

相关和目录可自行创建后再操作

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c

   ```
   cp exam.c tmp/shiyan.c
   ```

2. 在任何目录下回到用户主目录？

   ```
   cd
   cd ~
   cd /home/526
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？

   ```
   ls -al test
   total 8
   drwxr-xr-x 2 root root 4096 May 26 17:44 .
   drwxr-xr-x 3 root root 4096 May 26 17:44 ..
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar

   ```
   touch {1,2,3,4,5}.txt
   tar -cvf hailiang.tar {1,2,3,4,5}.txt
   ```

5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？

   ```
   touch file1.txt
   mv file1.txt file.2.txt
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？

   ```
   vim bbbb.txt
   i 526
   :wq!
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？

   ```
   mv *.txt *.doc *.bak /tmp/test
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。

   ```
   mv file1.txt docs/
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。

   ```
   mv file2.txt backup/
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。

    ```
    cd docs/
    mv * ../archive/
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。

    ```
    cd photos/
    cp * ../backup/
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。

    ```
    mv docs/report.doc papers/final_report.doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。

    ```
    cd docs/
    touch notes.txt
    cp notes.txt ../backup/
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。

    ```
    cp images/*.jpg photos/
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。

    ```
    cd docs/
    cp file1.txt file2.txt ../backup/
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中。

    ```
    cp docs/*.txt text_files/
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。

    ```
    cp -i docs/* temp/
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。

    ```
    mv -v docs/* archive/
    'docs/file1.txt' -> 'archive/file1.txt'
    'docs/file2.txt' -> 'archive/file2.txt'
    'docs/notes.txt' -> 'archive/notes.txt'
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。

    ```
    cp -v docs/ backup/
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。

    ```
    mv -b docs/report.txt archive/
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

    ```
    mv -n docs/*.pdf pdf_files/
    ```