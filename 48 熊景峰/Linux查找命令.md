#  查找

## find找文件或目录

基本语法：find 路径 条件选项

### 找文件名

-name

eg:find ./ -name 'file1' 找名为file1的

### 找类型

-type f/d

eg:find ./ -type f -name 'file1' 找名为file1的文件

find ./ -type d -name 'file1' 找名为file1的目录

### 找时间

-atime 访问时间

-ctime 创建时间

-mtime 修改时间

### 找大小

-empty 空的

-size 大小+单位

## grep从文件内容找关键字

grep '我' 1.txt 从1.txt里面找**我**

grep -r ‘我’ ./* 从当前目录及子目录找**我**

### 与find搭配使用

-exec {} \；

## which找可执行文件

which ls

找ls的工作路径/用来检测有没有这个命令

## whereis找可执行文件

路径

配置文件

源代码

手册

## type查看命令的类型

type nginx

## stat查看文件/目录的属性

查看文件的大小，权限，时间等等

## locate第三方

# 作业

### 操作题

1. **查找当前目录及其子目录中所有扩展名为 `.log` 的文件**：

   ```
    find ./ -type f -name '*.log'
   ```

2. **在 `/var/logs` 目录及其子目录中查找所有包含关键字 `error` 的文件，并显示匹配的行**：

   ```
   grep -r -n 'error' /var/logs
   ```

3. **在 `/home/user/docs` 目录中查找文件名包含 `report` 的所有文件**：

   ```
   touch docs/report.txt && find /home/sx/docs/ -type f -name '*report*'
   ```

4. **查找 `/etc` 目录中最近7天内修改过的文件**：

   ```
   find /etc -type f -mtime -7
   ```

5. **显示 `/usr/bin` 目录中名为 `python` 的可执行文件的路径**：

   ```
   find /usr/bin -type f -name 'python'
   ```

6. **查找系统中名为 `ls` 的命令及其手册页位置**：

   ```
   whereis ls
   ```

7. **查找当前目录中包含关键字 `TODO` 的所有文件，并显示匹配的行和文件名**：

   ```
   grep -n -H 'TODO' ./ 
   ```

8. **在 `/home/user/projects` 目录中查找所有包含关键字 `function` 的 `.js` 文件**：

   ```
   find /home/sx/proects -type f -name '*.js' -exec grep 'function' {} \;
   ```

9. **查找并显示当前目录及其子目录中所有空文件**：

   ```
   find ./ -type f -empty
   ```

10. **在 `/var/www` 目录中查找包含关键字 `database` 的所有文件，并只显示文件名**：

    ```
    find /var/www -type f -exec grep -l 'database' {} \; 
    ```

### 综合操作题

**综合操作题：**

假设你在一个名为 `/home/user/workspace` 的目录中工作。你需要完成以下任务：

1. 查找该目录中所有扩展名为 `.conf` 的文件。
2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行。
3. 将包含关键字 `server` 的文件名和匹配的行保存到一个名为 `server_lines.txt` 的文件中。

**预期命令步骤：**

1. 查找所有扩展名为 `.conf` 的文件：

   ```
   find /home/user/workspace -type f -name '*.conf'
   ```

2. 在这些 `.conf` 文件中查找包含关键字 `server` 的行：

   ```
   find /home/user/workspace -type f -name '*.conf' -exec grep  -n 'server' {} \;
   ```

3. 将结果保存到 `server_lines.txt` 文件中：

   ```
   find /home/user/workspace -type f -name '*.conf' -exec grep  -n 'server' {} \ >server_lines.txt
   ```

通过这套操作题和综合操作题，你可以全面地了解和应用Linux系统中与文件和内容查询相关的常用命令。