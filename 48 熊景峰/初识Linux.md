查看`IP`：

```
ip addr show // 默认的查看命令
ifconfig // net-tools组件包的命令，组件自己安装

apt-get update // 更新软件库
apt-get install net-tools

// 安装vim编辑器，默认的是vi编辑器
apt-get install vim  // vi 文件名，变成vim 文件名
```

安装：

```
apt-get install ssh   // 安装ssh,默认情况，只有普通用户可以登录

// 开启root用户登录
vim /etc/ssh/sshd_cofnig //修改/etc/ssh/sshd.config 文件 

// 开22端口，允许root登录为yes,启用密码验证功能

// 修改完重启ssh服务
/etc/init.d/ssh restart

// 用sudo命令，自己安装
apt-get isntall sudo 

//sudo配置
https://blog.csdn.net/jasonzhoujx/article/details/80468885
```