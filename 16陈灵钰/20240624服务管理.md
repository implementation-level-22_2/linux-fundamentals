````bash
1.init是Linux系统操作中不可缺少的程序之一。init进程，它是一个由内核启动的用户级进程，然后由它来启动后面的任务，包括多用户环境，网络等。

2.service命令其实是去/etc/init.d目录下，去执行相关程序，init.d目录包含许多系统各种服务的启动和停止脚本。当Linux启动时，会寻找这些目录中的服务脚本，并根据脚本的run level确定不同的启动级别。

3.systemctl是一个systemd工具，主要负责控制systemd系统和服务管理器。
在systemd管理体系中，被管理的deamon(守护进程)称作unit(单元)，对于单元的管理是通过命令systemctl来进行控制的。unit表示不同类型的systemd对象，通过配置文件进行标识和配置；文件主要包含了系统服务、监听socket、保存的系统快照以及其它与init相关的信息。


一.service nginx start :

1.如果 你是system v,其实是用/etc/init.d/nginx start实现

2.如果 你是systemd ，其实是用systemctl start nginx 来实现

二.早期的linux可以/etc/init.d/服务名 操作命令 或者 service 服务名 操作命令 

	现代的linux可以systemctl 操作命令 服务名

三.操作命令包含:
指令	                                                                 功能
systemctl list-unit-files	                              获取所用可用单元
systemctl list-units	                                    获取所有运行中的单元
systemctl status service	                           获取service的状态
systemctl start/stop/estart/reload service	改变service 的状态 启动、重启、停止、重载服务
systemctl enable/disable service	             是否开机自启
systemctl kill service	                                强制结束服务
systemctl daemon-reload	                         刷新systemd的配置文件

四.查看所有运行的服务：
1.service --status-all    [简约版，直观]
2.systemctl list-unit-files   [ 详细版，要自行筛选]

练习：

给自己的服务安装apache服务

```bash
systemctl stop nginx    //停止nginx
systemctl status nginx  //查看nginx运行状态
apt install apache2 //安装apache
systemctl start apache2  //开启apache
systemctl status apache2
```

或者

```bash
默认情况下，nginx监听80端口，apache监听80端口。

要改变nginx监听的端口，编辑nginx的配置文件/etc/nginx/sites-available/default，将listen 80;改为listen 8080;。

要改变apache监听的端口，编辑apache的配置文件/etc/httpd/conf/httpd.conf或/etc/apache2/ports.conf，找到Listen 80，并将80改为其它端口号，比如Listen 8081。

重启nginx和apache以应用更改：
sudo systemctl restart nginx
sudo systemctl restart apache2
```xxxxxxxxxx root@hecs-344360:~# export LANGUAGE=zh_CN.utf8##  周期任务练习执行在家目录touch a.txt1. 每天3:00执行一次   ~~~ shell   0 3 * * *    ~~~   2. 每周六2:00执行    ~~~ shell   0 2 * * 6    ~~~   3. 每周六1:05执行   ~~~ shell   5 1 * * 64. 每周六1:25执行       ~~~ shell   25 1 * * 65. 每天8：40执行    ~~~ shell   40 8 * * *6. 每天3：50执行    ~~~ shell   50 3 * * *7. 每周一到周五的3：40执行    ~~~ shell   40 3 * * 3-58. 每周一到周五的3：41开始，每10分钟执行一次    ~~~ shell   41-59/10,*/10 3,1-2,4-23 * * 1-59. 每天的10：31开始，每2小时执行一次   ~~~ shell   31 10-23/2 * * * 10. 每周一到周三的9：30执行一次    ~~~ shell    30 9 * * 1-311. 每周一到周五的8：00，每周一到周五的9：00执行一次    ~~~ shell    0 8,9 * * 1-512. 每天的23：45分执行一次    ~~~ shell    45 23 * * *13. 每周三的23：45分执行一次    ~~~ shell    45 23 * * 314. 每周一到周五的9：25到11：35、13：00到15：00之间，每隔10分钟执行一次    ~~~ shell    25-35/10 9-11 * * 1-5     0 13-15 * * 1-5    15. 每周一到周五的8：30、8：50、9：30、10：00、10：30、11：00、11：30、13：30、14：00、14：30、5：00分别执行一次    ~~~ shell    30,50 8 * * 1-5    0,30 10-11,13-14 * * 1-5    0 5 * * 1-5    ~~~16. 每天16：00、10：00执行一次    ~~~ shell    0 10,16 * * *17. 每天8：10、16：00、21：00分别执行一次    ~~~ shell    10 8 * * *    0 16,21 * * *18. 每天7：47、8：00分别执行一次    ~~~ shell    47 7 * * *    0 8 * * *## 环境变量练习### 练习题 1: 显示当前所有的环境变量* 使用`printenv`或`env`命令来显示所有的环境变量。```bashroot@hecs-344360:~# printenvprint environmentroot@hecs-344360:~# env```### 练习题 2: 显示`HOME`环境变量的值* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。```bashroot@hecs-344360:~# echo $HOME```### 练习题 3: 临时设置一个新的环境变量* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。```bashroot@hecs-344360:~# MY_AGE=18root@hecs-344360:~# echo $MY_AGE 18```### 练习题 4: 显示新设置的环境变量* 使用`echo`命令来显示`MY_AGE`的值。```bashroot@hecs-344360:~# echo $MY_VARIABLE```### 练习题 5: 在新的shell会话中检查环境变量* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？  ~~~ shell  显示空白，就是没有设置  root@hecs-344360:~# echo $MY_AGE      ~~~  ### 练习题 6: 修改`PATH`环境变量* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置```bashroot@hecs-344360:~# echo $PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/binroot@hecs-344360:~# export PATH=$PATH:$HOMEroot@hecs-344360:~# echo $PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root```将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。```bashroot@hecs-344360:~# export PATH=/tmp:$PATHroot@hecs-344360:~# echo $PATH /tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root```### 练习题 7: 验证`PATH`的修改* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。```bash/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root```### 练习题 8: 永久设置环境变量* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。例如，对于bash shell，你可以使用：```bashroot@hecs-344360:~# vim .bashrc export MY_NAME="奥德彪"root@hecs-344360:~# source .bashrc source 主动更新.bashrc```如何让`MY_NAME`生效，并验证```shellroot@hecs-344360:~# echo $MY_NAME 奥德彪```### 练习题 9: 清理* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。```bashroot@hecs-344360:~# unset MY_AGE 清除对MY_AGE的设置#之前的设置/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/binroot@hecs-344360:~# export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/binroot@hecs-344360:~# echo $PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin```### 练习题 10: 修改默认器* 使用`EDITOR`变量，修改你默认的编辑器为nano。```bashroot@hecs-344360:~# export EDITOR=nanoroot@hecs-344360:~# echo $EDITOR nano```### 练习题 11: 修改语言* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。```bashroot@hecs-344360:~# export LANG=zh_CN.utf8```- 使用`LANGUAGE`变量，让你的命令提示为中文```bashroot@hecs-344360:~# export LANGUAGE=zh_CN.utf8```bash
````