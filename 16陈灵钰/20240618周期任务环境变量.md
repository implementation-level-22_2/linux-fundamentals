##  周期任务练习

执行在家目录touch a.txt

1. 每天3:00执行一次

   ~~~ shell
   0 3 * * * 
   ~~~

   

2. 每周六2:00执行 

   ~~~ shell
   0 2 * * 6 
   ~~~

   

3. 每周六1:05执行

   ~~~ shell
   5 1 * * 6

4. 每周六1:25执行 

   

   ~~~ shell
   25 1 * * 6

5. 每天8：40执行 

   ~~~ shell
   40 8 * * *

6. 每天3：50执行 

   ~~~ shell
   50 3 * * *

7. 每周一到周五的3：40执行 

   ~~~ shell
   40 3 * * 3-5

8. 每周一到周五的3：41开始，每10分钟执行一次 

   ~~~ shell
   41-59/10,*/10 3,1-2,4-23 * * 1-5

9. 每天的10：31开始，每2小时执行一次

   ~~~ shell
   31 10-23/2 * * * 

10. 每周一到周三的9：30执行一次

    ~~~ shell
    30 9 * * 1-3

11. 每周一到周五的8：00，每周一到周五的9：00执行一次

    ~~~ shell
    0 8,9 * * 1-5

12. 每天的23：45分执行一次

    ~~~ shell
    45 23 * * *

13. 每周三的23：45分执行一次

    ~~~ shell
    45 23 * * 3

14. 每周一到周五的9：25到11：35、13：00到15：00之间，每隔10分钟执行一次

    ~~~ shell
    25-35/10 9-11 * * 1-5 
    0 13-15 * * 1-5
    

15. 每周一到周五的8：30、8：50、9：30、10：00、10：30、11：00、11：30、13：30、14：00、14：30、5：00分别执行一次

    ~~~ shell
    30,50 8 * * 1-5
    0,30 10-11,13-14 * * 1-5
    0 5 * * 1-5
    ~~~

16. 每天16：00、10：00执行一次

    ~~~ shell
    0 10,16 * * *

17. 每天8：10、16：00、21：00分别执行一次

    ~~~ shell
    10 8 * * *
    0 16,21 * * *

18. 每天7：47、8：00分别执行一次

    ~~~ shell
    47 7 * * *
    0 8 * * *

## 环境变量练习

### 练习题 1: 显示当前所有的环境变量

* 使用`printenv`或`env`命令来显示所有的环境变量。


```bash
root@hecs-344360:~# printenv
print environment
root@hecs-344360:~# env

```
### 练习题 2: 显示`HOME`环境变量的值

* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。


```bash
root@hecs-344360:~# echo $HOME

```
### 练习题 3: 临时设置一个新的环境变量

* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。


```bash
root@hecs-344360:~# MY_AGE=18
root@hecs-344360:~# echo $MY_AGE 
18
```
### 练习题 4: 显示新设置的环境变量

* 使用`echo`命令来显示`MY_AGE`的值。


```bash
root@hecs-344360:~# echo $MY_VARIABLE
```
### 练习题 5: 在新的shell会话中检查环境变量

* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？

  ~~~ shell
  显示空白，就是没有设置
  root@hecs-344360:~# echo $MY_AGE
  
  
  ~~~

  

### 练习题 6: 修改`PATH`环境变量

* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置


```bash
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
root@hecs-344360:~# export PATH=$PATH:$HOME
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root

```
将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。

```bash
root@hecs-344360:~# export PATH=/tmp:$PATH
root@hecs-344360:~# echo $PATH 
/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root
```

### 练习题 7: 验证`PATH`的修改

* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。


```bash
/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root
```
### 练习题 8: 永久设置环境变量

* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。

例如，对于bash shell，你可以使用：


```bash
root@hecs-344360:~# vim .bashrc 
export MY_NAME="奥德彪"
root@hecs-344360:~# source .bashrc 
source 主动更新.bashrc

```
如何让`MY_NAME`生效，并验证

```shell
root@hecs-344360:~# echo $MY_NAME 
奥德彪
```

### 练习题 9: 清理

* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。


```bash
root@hecs-344360:~# unset MY_AGE 
清除对MY_AGE的设置

#之前的设置
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

root@hecs-344360:~# export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

```
### 练习题 10: 修改默认器

* 使用`EDITOR`变量，修改你默认的编辑器为nano。


```bash
root@hecs-344360:~# export EDITOR=nano
root@hecs-344360:~# echo $EDITOR 
nano
```

### 练习题 11: 修改语言

* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。

```bash
root@hecs-344360:~# export LANG=zh_CN.utf8
```

- 使用`LANGUAGE`变量，让你的命令提示为中文

````bash
root@hecs-344360:~# export LANGUAGE=zh_CN.utf8##  周期任务练习

执行在家目录touch a.txt

1. 每天3:00执行一次

   ~~~ shell
   0 3 * * * 
   ~~~

   

2. 每周六2:00执行 

   ~~~ shell
   0 2 * * 6 
   ~~~

   

3. 每周六1:05执行

   ~~~ shell
   5 1 * * 6

4. 每周六1:25执行 

   

   ~~~ shell
   25 1 * * 6

5. 每天8：40执行 

   ~~~ shell
   40 8 * * *

6. 每天3：50执行 

   ~~~ shell
   50 3 * * *

7. 每周一到周五的3：40执行 

   ~~~ shell
   40 3 * * 3-5

8. 每周一到周五的3：41开始，每10分钟执行一次 

   ~~~ shell
   41-59/10,*/10 3,1-2,4-23 * * 1-5

9. 每天的10：31开始，每2小时执行一次

   ~~~ shell
   31 10-23/2 * * * 

10. 每周一到周三的9：30执行一次

    ~~~ shell
    30 9 * * 1-3

11. 每周一到周五的8：00，每周一到周五的9：00执行一次

    ~~~ shell
    0 8,9 * * 1-5

12. 每天的23：45分执行一次

    ~~~ shell
    45 23 * * *

13. 每周三的23：45分执行一次

    ~~~ shell
    45 23 * * 3

14. 每周一到周五的9：25到11：35、13：00到15：00之间，每隔10分钟执行一次

    ~~~ shell
    25-35/10 9-11 * * 1-5 
    0 13-15 * * 1-5
    

15. 每周一到周五的8：30、8：50、9：30、10：00、10：30、11：00、11：30、13：30、14：00、14：30、5：00分别执行一次

    ~~~ shell
    30,50 8 * * 1-5
    0,30 10-11,13-14 * * 1-5
    0 5 * * 1-5
    ~~~

16. 每天16：00、10：00执行一次

    ~~~ shell
    0 10,16 * * *

17. 每天8：10、16：00、21：00分别执行一次

    ~~~ shell
    10 8 * * *
    0 16,21 * * *

18. 每天7：47、8：00分别执行一次

    ~~~ shell
    47 7 * * *
    0 8 * * *

## 环境变量练习

### 练习题 1: 显示当前所有的环境变量

* 使用`printenv`或`env`命令来显示所有的环境变量。


```bash
root@hecs-344360:~# printenv
print environment
root@hecs-344360:~# env

```
### 练习题 2: 显示`HOME`环境变量的值

* 使用`echo`命令和`$`符号来显示`HOME`环境变量的值。


```bash
root@hecs-344360:~# echo $HOME

```
### 练习题 3: 临时设置一个新的环境变量

* 设置一个名为`MY_AGE`的环境变量，并将其值设置为`18`。


```bash
root@hecs-344360:~# MY_AGE=18
root@hecs-344360:~# echo $MY_AGE 
18
```
### 练习题 4: 显示新设置的环境变量

* 使用`echo`命令来显示`MY_AGE`的值。


```bash
root@hecs-344360:~# echo $MY_VARIABLE
```
### 练习题 5: 在新的shell会话中检查环境变量

* 打开一个新的终端窗口或标签页，并尝试显示`MY_AGE`的值。你会看到什么？为什么？

  ~~~ shell
  显示空白，就是没有设置
  root@hecs-344360:~# echo $MY_AGE
  
  
  ~~~

  

### 练习题 6: 修改`PATH`环境变量

* 将`你当前用户的家目录`添加到你的`PATH`环境变量的末尾位置


```bash
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
root@hecs-344360:~# export PATH=$PATH:$HOME
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root

```
将`/tmp`添加到你的`PATH`环境变量的开始位置，（注意：这可能会覆盖其他路径中的同名命令，所以请谨慎操作）。

```bash
root@hecs-344360:~# export PATH=/tmp:$PATH
root@hecs-344360:~# echo $PATH 
/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root
```

### 练习题 7: 验证`PATH`的修改

* 使用`echo`命令显示`PATH`的值，并确认`前面添加的目录`已经被添加到对应位置。


```bash
/tmp:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root
```
### 练习题 8: 永久设置环境变量

* 在你的shell配置文件中（如`~/.bashrc`、`~/.bash_profile`、`~/.zshrc`等，取决于你使用的shell和配置）添加一行来永久设置`MY_NAME`，值设置为`奥德彪`。

例如，对于bash shell，你可以使用：


```bash
root@hecs-344360:~# vim .bashrc 
export MY_NAME="奥德彪"
root@hecs-344360:~# source .bashrc 
source 主动更新.bashrc

```
如何让`MY_NAME`生效，并验证

```shell
root@hecs-344360:~# echo $MY_NAME 
奥德彪
```

### 练习题 9: 清理

* 清除你之前设置的`MY_AGE`和`PATH`的修改（如果你不想永久保留它们）。


```bash
root@hecs-344360:~# unset MY_AGE 
清除对MY_AGE的设置

#之前的设置
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

root@hecs-344360:~# export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
root@hecs-344360:~# echo $PATH 
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

```
### 练习题 10: 修改默认器

* 使用`EDITOR`变量，修改你默认的编辑器为nano。


```bash
root@hecs-344360:~# export EDITOR=nano
root@hecs-344360:~# echo $EDITOR 
nano
```

### 练习题 11: 修改语言

* 使用`LANG`变量，让你的文件支持中文和utf8编码来避免乱码。

```bash
root@hecs-344360:~# export LANG=zh_CN.utf8
```

- 使用`LANGUAGE`变量，让你的命令提示为中文

```bash
root@hecs-344360:~# export LANGUAGE=zh_CN.utf8
```
````