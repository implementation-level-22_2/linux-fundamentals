Awk 是由 Aho、Weinberger 和 Kernighan 三位科学家开发的，特别擅长文本处理的 Linux 工具，是 Linux 下最常用的工具之一。Awk 也是一种编程语言，在编程语言排行榜上也能排上号。

AWK 脚本由一个或多个语句块组成，每个语句块可以是以下三种之一：

1. 开始语句块：在程序开始时执行 `BEGIN { print "start" }`
2. 通用语句块：逐行处理文件内容 `pattern { commands }`,
3. 结束语句块：在程序结束时执行 `END { print "end" }`

Awk 中的常用选项（限命令行）

* `-F`：指定输入字段分隔符。例如，`-F,` 将逗号设置为字段分隔符。
  
  * 默认是以空格\t等为分隔符
  * 类似于FS="分隔符" 在文件中使用

* `-v`：赋值外部变量。例如，`-v var=value`。 min=60

* `-f`：指定 Awk 脚本文件。例如，`-f script.awk`。

* `-W`：控制警告和其他运行时选项。例如，`-W version` 用于显示版本信息。

Awk 中的常用动作（action）

* `print`：打印指定内容。例如，`print $1` 打印第一字段。

* `printf`：格式化输出。例如，`printf "%s\n", $1` 以格式化方式打印第一字段。

作业

1. 只显示/etc/passwd的账户
   
   ```
   awk -F ‘{print $1}’ /etc/passwd
   ```

2. 只显示/etc/passwd的账户和对应的shell，并在第一行上添加列名用户制表符shell，最后一行添加----------------
   
   ```
   
   ```

3. 搜索/etc/passwd有关键字root的所有行
   
   ```
   awk -F '/root/{print}' /etc/passwd
   ```

4. 统计/etc/passwd文件中，每行的行号，每列的列数，对应的完整行内容以制表符分隔
   
   ```
   awk -F '{print NR,NF,$0}' /etc/passwd
   ```

5. 输出/etc/passwd文件中uid字段小于100的行
   
   ```
   awk -F: '$3<100 {print $0}' /etc/passwd
   ```

6. /etc/passwd文件中gid字段大于200的，输出该行第一、第四字段，第一，第四字段并以制表符分隔
   
   ```
   awk -F: '$4>200 {OFS="\t";print $1,$4}' /etc/passwd
   ```

7. 输出/etc/passwd文件中uid字段大于等于100的行

```
awk -F: '$3>=100 {print $0}' /etc/passwd
```

5 输出/etc/passwd文件中以nologin结尾的行

```
awk -F: '$NF~/nologin/ {print $1,$NF}' /etc/passwd
```
