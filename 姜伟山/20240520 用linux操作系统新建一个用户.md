## 一.用linux操作系统新建一个用户，密码设置以刚建的用户登录

### 1. 新建一个用户

首先，您需要以root用户或具有sudo权限的用户登录系统。然后，使用`useradd`命令来创建新用户。假设新用户的用户名为`newuser`。

```bash
sudo useradd newuser
```

### 2. 设置密码

使用`passwd`命令为新用户设置密码。

```bash
sudo passwd newuser
```

系统会提示您输入并确认新密码。

### 3. 以新建的用户登录

在完成上述步骤后，您可以使用以下方法之一切换到新用户：

#### 方法1：使用`su`命令

使用`su`命令切换到新用户。您需要输入新用户的密码。

```bash
su - newuser
```

#### 方法2：使用`ssh`命令

如果您通过SSH远程登录服务器，可以使用以下命令：

```bash
ssh newuser@hostname
```

其中`hostname`是您要登录的主机名或IP地址。

### 4. 验证新用户的登录

无论使用哪种方法，成功登录后，您可以使用`whoami`命令来确认当前用户。

```bash
whoami
```

如果显示`newuser`，则表示您已成功以新用户登录。

### 示例操作过程

下面是一个完整的操作示例：

```bash
# 以具有sudo权限的用户登录
sudo useradd newuser          # 创建新用户
sudo passwd newuser           # 设置新用户密码
# 输入新密码并确认
su - newuser                  # 切换到新用户
# 输入新用户密码
whoami                        # 验证当前用户
# 显示 newuser
```

## 二.使用命令，在自己的家目录，以以下层次依次建立文件夹

假设我们要在新建的用户`newuser`的主目录下创建以下层次的文件夹结构：

```bash
国家
└── 省份
    └── 市
        └── 县（区）
            └── 镇（街道）
                └── 村（小区）
                    └── 姓名
```

以下是具体步骤：

### 1. 切换到新建的用户

如果你还未切换到新建的用户，可以使用以下命令：

```bash
su - newuser
```

### 2. 创建文件夹结构

使用`mkdir`命令和`-p`选项来创建嵌套目录。`-p`选项会自动创建必要的父目录。

```bash
mkdir -p ~/国家/省份/市/县（区）/镇（街道）/村（小区）/姓名
```

### 3. 验证文件夹结构

可以使用`tree`命令来查看目录结构。如果系统中没有`tree`命令，可以安装它。以Ubuntu为例，安装命令如下：

```bash
sudo apt-get install tree
```

然后查看目录结构：

```bash
tree ~/国家
```

完整操作示例：

```bash
# 切换到新用户
su - newuser

# 创建文件夹结构
mkdir -p ~/国家/省份/市/县（区）/镇（街道）/村（小区）/姓名

# 验证文件夹结构
tree ~/国家
```

### 示例输出

如果文件夹结构创建成功，`tree`命令会显示类似以下的输出：

```bash
home/newuser/国家
└── 省份
    └── 市
        └── 县（区）
            └── 镇（街道）
                └── 村（小区）
                    └── 姓名
```

通过这些步骤，您可以在新建用户的主目录下创建指定的文件夹层次结构。

在姓名文件夹，建立一个自己的学号文件

假设你的学号是`12345678`。以下是如何在`姓名`文件夹中创建一个包含你学号的文件：

### 1. 切换到新建的用户

如果你还未切换到新建的用户，可以使用以下命令：

```bash
su - newuser
```

## 三.创建学号文件

使用`touch`命令来创建一个名为`学号.txt`的文件，并将学号写入其中。可以使用`echo`命令将学号写入文件。

```bash
# 切换到姓名文件夹
cd ~/国家/省份/市/县（区）/镇（街道）/村（小区）/姓名

# 创建学号文件并写入学号
echo "20240001" > ~/姓名/学号.txt
```

### 3. 验证文件内容

使用`cat`命令查看文件内容，确保学号已正确写入文件。

```bash
cat 学号.txt
```

完整操作示例：

```bash
# 切换到新用户
su - newuser

# 切换到姓名文件夹
cd ~/国家/省份/市/县（区）/镇（街道）/村（小区）/姓名

# 创建学号文件并写入学号
echo "20240001" > ~/姓名/学号.txt

# 验证文件内容
cat 学号.txt
```

### 示例输出

如果文件内容正确，`cat`命令会显示学号：

```bash
12345678
```

通过这些步骤，您可以在`姓名`文件夹中创建一个包含你学号的文件

在这个文件中，写一篇不少于400字的日记

```bash
sudo vim 12345678.txt
```

## 四.最后，将国家这个文件夹打包，同时练习怎么解压

要将名为“国家”的文件夹打包，你可以使用`tar`命令。在终端中执行以下命令：

```bash
tar -czvf 国家.tar.gz 国家
```

这将创建一个名为“国家.tar.gz”的压缩文件，其中包含了名为“国家”的文件夹及其内容。

参考：https://openai.com/chatgpt/

GPT-4o