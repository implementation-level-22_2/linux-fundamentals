```bash
以下所有操作都在家目录执行：

操作题一：使用 cat 命令显示 /etc/passwd 文件的内容。
cat etc/passwd 

操作题二：将文件 /etc/passwd 的内容复制到 passwd_bak.txt 文件中，但不使用 cp 命令。
cat etc/passwd > passwd_bak_txt

操作题三：新建两个文件 file1.txt 和 file2.txt，分别写一些不同的内容，再将这两个文件的内容合并到一个新的文件 file3.txt 中。
echo "文本" > file1.txt
echo "文本" > file2.txt 
cat file1.txt file2.txt > file3.txt

使用命令从尾部开始显示 bigfile.txt 文件的内容。
tac bigfile.txt

尝试找出 bigfile.txt 文件的最后一行内容，要使用 tac 命令。
tac bigfile.txt | tail -n 1

查看 bigfile.txt 文件的最后5行内容。
tail bigfile.txt -n 5

倒序查看 bigfile.txt 文件的最后5行内容。
tac bigfile.txt | tail -n 5

操作题一：使用 more 命令查看 bigfile.txt 文件的内容，并在查看过程中使用空格键翻页。
more bigfile.txt

操作题二：在 more 命令的查看过程中，如何使用回车键来逐行查看文件内容？
more bigfile.txt

// ENTER 
操作题三：如何在 more 命令中搜索bigfile.txt特定字符串（例如 "error"）并跳转到下一个匹配项？
more bigfile.txt

// 按 / 键输入搜索在字符串，然后按 'n' 键跳转到下一个匹配项
less 命令操作题

操作题一：使用 less 命令查看 bigfile.txt 文件，并快速定位到文件的末尾。
less bigfile.txt 
// 按 'G' 键快速 定位到末尾

操作题二：在 less 命令中，如何向上和向下滚动文件内容？
less bigfile.txt
// 使用箭头上下键滚动，或按 'k'和'j'键向上和向下滚动内容

操作题三：在 less 命令中，如何搜索bigfile.txt一个特定的函数名（例如 def my_function），并查看所有匹配项？
less bigfile.txt
// 按 '/'键输入你要搜索在函数名，在按 'n' 匹配所有选项

head 命令操作题
操作题一：使用 head 命令显示 bigfile.txt 文件的前5行内容。
head bigfile.txt -n 5
//使用`-n`选项指定行数：`head -n 20 file.txt`（显示前20行）

操作题二：将 bigfile.txt 的前20行内容保存到 bigfile_20.txt 文件中。
head -n 20 bigfile.txt > bigfile_20.txt
//使用`-n`选项指定行数：`head -n 20 file.txt`（显示前20行）

操作题三：如何结合 head 和 grep 命令来查找 bigfile.txt 文件中以 "A" 开头的前10行？
grep "^A" bigfile.txt | head -n 10
tail 命令操作题

操作题一：使用 tail 命令显示 bigfile.txt 文件的最后20行内容。
tail -n 20 bigfile.txt 

操作题二：如何实时跟踪一个正在写入的日志文件（如 bigfile.txt）的最后10行内容？
tail -f bigfile.txt
// f ：实时追踪文件的变化并输出新增的内容。通常用于监视日志文件的增长。

操作题三：在 tail 命令中，如何反向显示文件的最后10行内容（即按从旧到新的顺序显示）？
tail -n 10 bigfile.txt  | tac 
// -n [行数]：显示文件的最后指定行数。如果不指定行数，则默认为显示最后10行。

综合题
综合题：假设你有一个非常大的日志文件 bigfile.txt，你需要找出所有包含 "ERROR" 字符串的行，并查看这些行及其之前的两行内容。
grep -b2 'ERROR' bigfile.txt 或grep -B 2 'ERROR' bigfile.txt
// -B 2 或 -b2 参数都表示显示匹配行及其之前的两行内容。xxxxxxxxxx less bigfile.txt | grep -b 2 "ERROR"scp 本地文件路径\文件名 用户名@IP地址:服务器内文件地址bash
```