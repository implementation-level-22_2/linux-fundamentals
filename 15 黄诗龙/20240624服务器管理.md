# 1.笔记

## 服务器管理

service nginx start:

如果你是system v，其实是用/etc/init.d/nginx start 实现

如果你是systemd，其实是用systemctl start nginx 实现

## 操作命令

启用：start

停止：stop

重启：restart

重新加载配置：reload

查看状态：status

开机自启动：enable

禁用：disable

list-jobs 查看正在运行的服务