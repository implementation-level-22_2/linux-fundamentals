## Linux 操作系统内核

单独的Linux内核并不可直接使用，一般搭配GNU套件（GNU/Linux）

Linux发行版本：Centos，Debian，RedHat...

Linux的文件系统是采用级层式的树状目录结构，在此结构中的最上层是根目录“/”

Linux的命令大全 	https://www.linuxcool.com

通过虚拟机安装Linux系统

安装VM pro 17 虚拟机

安装Debian 12.5

```js
//配置ssh以便实现远程连接
apt-get install ssh
vim /etc/ssh/sshd_config//修改/etc/ssh/sshd.config文件 
/*
 Port 22
 PermitRootLogin prohibit-password 改为 PermitRootLogin yes
 PasswordAuthentication yes
 */
//开端口22，允许root用户远程登录，设置是否使用口令验证。
/etc/init.d/ssh restart //配完重启
apt-get isntall sudo//普通用户权限低，需临时借用，sudo需安装
adduser 用户名 sudo//将用户名添加到sudo
apt-get install vim//安装vim编辑器，vi不好用
vim /etc/sudoers//进入sudoers文件修改
%sudo ALL = (ALL:ALL)ALL下输入 用户名 ALL =(ALL:ALL)ALL
sudo apt-get install net-tools
sudo ipconfig//net-tools组件包命令，需安装net-tools
```

