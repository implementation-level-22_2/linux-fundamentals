### Debian Linux 目录结构

Linux 一切文件存在

#### 结构

/ 	       根目录

bin		常规执行程序的存放位置	binary	普通用户可看

boot	  包含引导加载程序和内核文件

dev	   设备文件  tty相当于vty		/etc/sda 硬盘设备文件	sda	sda1 硬盘分区

etc	    存放系统配置文件	/etc/apt/soures.list  软件源配置文件

home	普通用户家目录所在

lib	     存放系统库文件	存放共用文件 共享

media   用于挂载可移动介质的挂载点	如U盘，光盘等

mnt	   用于手动挂载临时文件系统的挂载点

opt	    可选的第三方软件包的安装目录   源码编译安装

proc	  虚拟文件系统,提供有关系统和运行进程的信息

root	   root用户的家目录

run		运行时临时文件目录

sbin	   存放系统管理员需要的系统管理命令

srv		 存放服务的数据目录

sys		包含虚拟文件系统，如文件系统相关的信息和统计数据

tmp	    临时文件目录

usr		 大部分用户安装的程序和文件

var		 经常变化的文件，如日志文件、缓存文件等

lost+found	文件系统检查程序将损坏的文件片段放置在此外

#### 命令格式

```js
command [options] [arguments] //命令名称 [命令选项] [命令参数]
/*
systemctl status ssh
ls -l /home/user 列出目录内容
cp source.txt destination.txt 复制文件
find /home/user -name "*.txt" 查找文件
sudo apt-get install vim 安装软件包
password username 更改用户密码
*/
ls -l //查看当前文件	d开头为目录 l开头为链接

//man命令
//命令参数的长格式于短式
mam --help	man -h
```

#### 注意事项

linux 严格区分大小写

注意前后顺序

/等结构

不要漏掉空格

#### 快捷键技巧

Tab 补全命令

Ctr+C 命令终止

Ctrl+D 结束键盘输入

Ctrl+L 清空终端
