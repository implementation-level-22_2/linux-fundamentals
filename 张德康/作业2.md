1. 使用 `cd` 命令进入 `development` 目录（假设您已经在包含 `development` 目录的父目录中）。

   ```bash
   cd development/
   ```

2. 在 `development` 目录下创建名为 `old_code` 的子目录。

   ```bash
    mkdir old_code
   ```

3. 在 `old_code` 目录下创建以下文件和目录结构：

   - `module1/script1.py`
   - `module1/script1.pyc`
   - `module1/script2.py`
   - `module1/script2.pyo`
   - `module2/script3.py`
   - `module2/script3.pyc`

   ```bash
   cd old_code/
   mkdir module1 module2
   touch script1.py script1.pyc script2.py script2.pyo
   cd ../module2
   touch script3.py script3.pyc
   ```

4. 使用 `ls -l` 命令查看 `old_code` 目录下的所有文件和目录。

   ```bash
   ls -l ..
   total 8
   drwxr-xr-x 2 root root 4096 May 23 10:38 module1
   drwxr-xr-x 2 root root 4096 May 23 10:39 module2
   
   ls -lR ..
   ..:
   total 8
   drwxr-xr-x 2 root root 4096 May 23 10:38 module1
   drwxr-xr-x 2 root root 4096 May 23 10:39 module2
   
   ../module1:
   total 0
   -rw-r--r-- 1 root root 0 May 23 10:38 script1.py
   -rw-r--r-- 1 root root 0 May 23 10:38 script1.pyc
   -rw-r--r-- 1 root root 0 May 23 10:38 script2.py
   -rw-r--r-- 1 root root 0 May 23 10:38 script2.pyo
   
   ../module2:
   total 0
   -rw-r--r-- 1 root root 0 May 23 10:39 script3.py
   -rw-r--r-- 1 root root 0 May 23 10:39 script3.pyc
   ```

5. 手动删除 `old_code` 目录中所有的 `.pyc` 和 `.pyo` 文件。

   ```bash
   cd module1
   rm  *.pyc *.pyo
   cd ../module2
   rm  *.pyc
   ```

6. 创建一个新的目录 `source_code`，并使用 `cp` 命令手动复制 `old_code` 目录中所有 `.py` 文件到 `source_code` 目录中，同时保持原始目录结构。

   ```bash
   cd ../..
   mkdir source_code
   cp -r old_code/module* source_code/
   ```

7. 使用 `mkdir` 命令在 `source_code` 目录下创建两个新的子目录 `tests` 和 `docs`。

   ```bash
   mkdir tests docs
   ```

8. 使用 `touch` 命令在 `source_code/docs` 目录下创建两个空文件 `architecture.md` 和 `requirements.md`。

   ```bash
   cd docs
   touch architecture.md requirements.md
   ```

9. 回到 `development` 目录的根目录，使用 `ls -R` 命令递归地列出整个 `development` 目录的结构。

   ```bash
   root@hecs-157832:/home# ls -R development/
   ```

10. ls清理工作：删除整个 `old_code` 目录（包括其中的所有文件和子目录）。

    ```bash
    cd development/
    rm -rf old_code/
    ```

11. （额外任务）假设您发现 `source_code/module1` 目录下有一个名为 `unused_script.py` 的文件不再需要（尽管在这个示例中我们没有创建它，但假设它存在），请使用 `rm` 命令删除它。

```bash
rm -f module1/unused_script.py
