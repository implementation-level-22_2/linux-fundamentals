### exam.txt 文件内容：

```
This is a text file for practice.
It contains some words like dog, cat, and bird.
There are also numbers like 123 and 456.
# heihei
We will use sed to manipulate this file.
```

#### 修改操|作

1. 使用 `sed` 将文件 `exam.txt` 中所有的 "dog" 替换为 "cat"，并将结果输出到标准输出。

```
 sed 's/dog/cat/g' 1.txt
```

1. 使用 `sed` 将文件 `exam.txt` 中所有的 "123" 替换为 "OneTwoThree"，并将结果保存到新文件 `updated_exam.txt` 中。

```
 sed 's/123/OneTwoThree/g' 1.txt >updated_exam.txt
```

#### 删除操作

1. 使用 `sed` 删除文件 `exam.txt` 中所有以 "#" 开头的注释行，并将结果输出到标准输出。

```
sed "/^#/d" 1.txt
```

1. 使用 `sed` 删除文件 `exam.txt` 中所有包含 "words" 的行，并将结果保存到新文件 `clean_exam.txt` 中。

```
sed "/words/d" 1.txt > clean_exam.txt
```

#### 插入操作

1. 使用 `sed` 在文件 `exam.txt` 的第2行插入一行 "Welcome to sed manipulation"，并将结果保存到新文件 `updated_exam.txt` 中。

```
 sed "2a\Welcome to sed manipulation" updated_exam.txt 
```

1. 使用 `sed` 在文件 `exam.txt` 的`numbers`所在行插入一行 "This is a new line"，并将结果输出到标准输出。

```
 sed "/numbers/a\This is a new line" 1.txt 
```

#### 追加操作

1. 使用 `sed` 在文件 `exam.txt` 的末尾追加一行 "End of practice"，并将结果保存到新文件 `updated_exam.txt` 中。

```
sed '$a\End of practice' 1.txt
```

1. 使用 `sed` 在文件 `exam.txt` 的每一行末尾追加内容 " - 2024-05-31"，并将结果输出到标准输出。

```
sed "a\ - 2024-05-31" 1.txt
```

#### 整行替换操作

1. 使用 `sed` 将文件 `exam.txt` 中所有以字母 "W" 开头的行替换为 "Not Available"，并将结果输出到标准输出。/

```
sed '/^W/c\Not Available/' 1.txt
```

1. 使用 `sed` 将文件 `exam.txt` 中所有包含 "words" 的行替换为 "Replaced"，并将结果保存到新文件 `updated_exam.txt` 中。

```
sed '/words/c\Replaced' 1.txt >updated_exam.txt
```

#### 多命令操作

1. 使用 `sed` 删除文件 `exam.txt` 中`dog`所在行，`file`换成`文件`，并将结果输出到标准输出。

```
sed '/dog/d;s/file/文件/' 1.txt
```

1. 使用 `sed` 将文件 `exam.txt` 中，删除空白行并将所有以 "It" 开头的行替换为 "This"，

```
sed "/^$/d;s/^ItThis/g" 1.txt
```

#### 脚本文件操作

1. 创建一个 `sed` 脚本文件 `replace_apple_with_orange.sed`，实现将文件 `exam.txt` 中所有 "apple" 替换为 "orange" 的功能，并将结果输出到标准输出。

```

```

运行脚本：

```

```

1. 创建一个 `sed` 脚本文件 `remove_blank_lines.sed`，实现删除文件 `exam.txt` 中所有空白行的功能，并将结果保存到新文件 `cleaned_exam.txt` 中。

```

```

运行脚本：

```

```

