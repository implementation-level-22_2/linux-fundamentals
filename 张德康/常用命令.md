## 刚安装完Deiban系统 7步走：

1. 设置国内软件源镜像  /etc/apt/sources.list 
2. 更新软件包列表:apt update
3. 更新系统:apt upgrade -y
4. 安装vim:apt install vim -y  locate 数据库 find 实时
5. 编辑网卡配置，设置静态IP:

```bash
vim /etc/network/interfaces

# 按如下设置
# The primary network interface
allow-hotplug ens192
iface ens192 inet static
address 172.16.90.71
netmask 255.255.255.0
gateway 172.16.90.1

#重新启动网络服务
sudo systemctl restart networking
```

5. 修改SSHD配置，允许root用户远程登录 

```bash
vim /etc/ssh/sshd_config

# 取消如下代码的注释，并且将其值设置为yes，以允许root用户的远程登录
PermitRootLogin yes

```

6. 配置环境变量，简单化ls的用法 


/etc/profile.d    /etc/nginx/conf.d/

```bash
 export LS_OPTIONS='--color=auto'
 alias ls='ls $LS_OPTIONS'
 alias ll='ls $LS_OPTIONS -l'
 alias l='ls $LS_OPTIONS -lA'

```
7. 配置环境变量，设置当前系统字符集，以接受中文

/etc/profile.d/lang.sh
```bash
export LC_ALL=C.utf8
```

要确保设置生效，可以重新加载该文件并检查环境变量

```sh
source /etc/profile.d/lang.sh
echo $LC_ALL
```



几个常用命令

### 1. `time`

用于测量命令的执行时间。

**示例**：

```bash
time ls
```

输出：

```plaintext
real    0m0.003s
user    0m0.001s
sys     0m0.002s
```

### 2. `date`

- **%Y**：四位数的年份。
- **%m**：两位数的月份。
- **%d**：两位数的日期。

**示例**：

```bash
date "+%Y-%m-%d %H:%M:%S"
```

输出：

```plaintext
2024-06-10 15:30:00
```

### 3. `timedatectl`

**示例**：

```bash
timedatectl status
```

输出：

```plaintext
Local time: Mon 2024-06-10 15:30:00 UTC
Universal time: Mon 2024-06-10 15:30:00 UTC
RTC time: Mon 2024-06-10 15:30:00
Time zone: Etc/UTC (UTC, +0000)
System clock synchronized: yes
NTP service: active
```

### 4. `reboot`

重新启动系统。

**核心术语**：
- **systemd**：系统和服务管理器。

**示例**：

```bash
sudo reboot
```

### 5. `poweroff`   

关闭系统电源。

**halt**：停止系统所有的 CPU 功能。

### 6. `wget`

从网络上下载文件

**示例**：

```bash
wget https://example.com/file.txt
```

### 7. `curl`

从网络上获取或发送数据。

**示例**：

```bash
curl -O https://example.com/file.txt
```

### 8. `ps`

查看当前运行的进程。

**示例**：

```bash
ps aux  # 显示所有用户的所有进程 一般会结合。grep,awk等过滤数据
```

### 9. `kill`

向进程发送信号（通常用于终止进程）。

**示例**：

```bash
kill 1234  # 用pidof 进程名。来查看具体的进程的进程号
```

### 10. `killall`

向指定名称的所有进程发送信号。

`先安装psmisc`

安装psmisc 后，就会有pstree和killall

**示例**：

```bash
killall firefox
```

### 11. `ip`

显示和操作网络接口和路由。

- **address**：IP 地址。
- **route**：路由信息。

**示例**：

```bash
ip addr show
```

### 12. `ss`

**使用场景**：显示套接字统计信息。旧版是netstat

- **TCP**：传输控制协议。
- **UDP**：用户数据报协议。

**示例**：

```bash
ss -tuln # 可以查看哪些端口开放着
```

### 13. `uname`

显示系统信息。

- **kernel**：操作系统内核。
- **OS**：操作系统。

**示例**：

```bash
uname -a
```

### 14. `uptime`

显示系统运行时间和负载。

- **load average**：系统平均负载。

**示例**：

```bash
uptime
```

### 15. `who`

显示当前登录用户信息。

- **login**：用户登录信息。
- **TTY**：终端类型。

**示例**：

```bash
who
```

### 16. `last`

显示系统上最近的登录信息。

- **wtmp**：记录登录和注销事件的文件。

**示例**：

```bash
last
```

### 17. `ping`

测试网络连通性。

- **ICMP**：互联网控制消息协议。
- **echo request**：回显请求。

**示例**：

```bash
ping example.com
```

### 18. `traceroute`

**使用场景**：显示到达网络主机的路径。

**核心术语**：
- **hop**：从一个网络节点到另一个的跳转。
- **TTL**：生存时间。

**示例**：

```bash
traceroute example.com
```

### 19. `history`

显示命令历史记录。

- **bash history**：记录用户输入的命令历史。

**示例**：

```bash
history
```

1. **`free`**：该命令用于显示系统内存的使用情况
- `total`: 总内存量
    - `used`: 已使用的内存量
    - `free`: 空闲的内存量
    - `shared`: 用于共享的内存量
    - `buff/cache`: 用于缓存的内存量
    - `available`: 可用的内存量
    
2. **`df -h`**：该命令用于显示文件系统的磁盘空间利用情况。
- `Filesystem`: 文件系统设备
    - `Size`: 文件系统总容量
    - `Used`: 已使用的空间
    - `Avail`: 可用空间
    - `Use%`: 使用百分比
    - `Mounted on`: 挂载点



`du` 命令用于估算文件或目录的磁盘使用情况。

1. **查看当前目录的磁盘使用情况**：

   ```
   复制代码
   du -h
   ```
   
2. **查看特定目录的磁盘使用情况**：

   ```
   du -h /目录名
   ```
   
3. **显示文件或目录的总磁盘使用量**：

   ```
   du -sh /path/to/file_or_directory
   ```
   
   这将显示指定文件或目录的总磁盘使用量， `-s` 参数表示仅显示总和， `-h` 表示以人类可读的格式显示。

4. **显示目录中每个文件的磁盘使用量**：

   ```
   复制代码
   du -ah /path/to/directory
   ```
   
   这将显示指定目录中每个文件的磁盘使用量， `-a` 参数表示包括所有文件。
   
5. **按照磁盘使用量排序显示目录**：

   ```
   du -h | sort -h
   ```
   
   这将按照磁盘使用量从小到大排序显示当前目录及其所有子目录的磁盘使用情况。

6. **限制显示的深度**：

   ```
   du -h --max-depth=1 /path/to/directory
   ```
   
   这将仅显示指定目录的直接子目录的磁盘使用情况， `--max-depth` 参数用于指定显示的深度。

- **重定向**：
  - `>`：将输出重定向到文件（覆盖）。
  - `>>`：将输出追加到文件。
  - `<`：将文件内容作为输入。
  - `2>`：将错误输出重定向到文件。
  - `2>>`：将错误输出追加到文件。
  - `&>`：将标准输出和错误输出同时重定向到文件。


## 思考：

```
<< 是什么功能？
从标准输入中读入，直到遇到分界符停止
()把命令包括起来，是干什么功能？
() 表示在当前 shell 中将多个命令作为一个整体执行。需要注意的是，使用 () 括起来的命令在执行前面都不会切换当前工作目录，也就是说命令组合都是在当前工作目录下被执行的，尽管命令中有切换目录的命令一条命令需要独占一个物理行，如果需要将多条命令放在同一行，命令之间使用命令分隔符（;）分隔
```