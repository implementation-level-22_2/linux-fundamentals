````
相关和目录可自行创建后再操作

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c
   
   ```
    touch exam.c
     cp exam.c ../../tmp/shiyan.c
   cp exam.c /tmp/shiyan.c
   
   ```

2. 在任何目录下回到用户主目录？
   
   ```
   cd ~
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？
   
   ```
   ls -al test
   ls -al /tmp/test
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar
   
   ```
   text2$ touch 1.txt 2.txt 3.txt 4.txt 5.txt
    tar -cvf hailiang.tar 1.txt 2.txt 3.txt 4.txt 5.txt 
   
   ```

5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？
   
   ```
   touch file1.txt
   mv file1.txt file2.txt
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？
   
   ```
   vim bbbb.txt
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？
   
   ```
   cp *.txt *.doc *bak ../../tmp/test/
   cp *.txt *.doc *bak /tmp/test/
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。
   
   ```
   mv file1.txt ../../home/msy/docs/
   mv file1.txt /home/msy/docs/
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。
   
   ```
   mv file2.txt ../../home/msy/backup/
   mv file2.txt /home/msy/backup/
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。
    
    ```
   msy@hecs-155801:~/docs$ mv * ../archive
   msy@hecs-155801:~/docs$ mv * /home/msy/archive
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。
    
    ```
    cp -r photos/ backup/
    cp -r photos/ /home/msy/backup/
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。
    
    ```
    mv docs/report.doc ./papers/final_report_doc
    mv docs/report.doc home/msy/papers/final_report_doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。
    
    ```
    touch notes.txt
    cp notes.txt ../backup/
    cp notes.txt /home/msy/backup/
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。
    
    ```
     cp images/*.jpg photos/
     cp images/*.jpg /home/msy/photos/
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。
    
    ```
    cp file1.txt file2.txt ../backup/
    cp file1.txt file2.txt /home/msy/backup/
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中。
    
    ```
    cp docs/*.txt text_files/
    cp /home/msy/docs/*.txt /home/msy/text_files/
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。
    
    ```
    cp -i docs/* temp/
    `cp -i /home/msy/docs/* /home/dqq/temp/`
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。
    
    ```
    mv -v docs/* archive/
    mv -v docs/* /home/msy/archive/
        'docs/file1.txt' -> 'archive/file1.txt'
        'docs/file2.txt' -> 'archive/file2.txt'
        'docs/notes.txt' -> 'archive/notes.txt'
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。
    
    ```
    cp -v docs/ ./backup/
    cp -v docs/ /home/msy/backup/
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。
    
    ```
    mv docs/[^temp_]* backup/ 
    `mv /home/msy/docs/[^temp_]* /home/msy/backup/`
    ```

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。
    
    ```
    mv -b docs/report.txt archive/
    mv -b /home/msy/docs/report.txt /home/msy/archive/`
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

```
mv -n docs/*.pdf pdf_files/
mv -n /home/msy/docs/*.pdf /home/msy/pdf_files/
```
````
