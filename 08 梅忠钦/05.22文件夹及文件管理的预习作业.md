1. ### 综合练习题：大学生生活场景中的文件夹和文件管理
   
   假设你是一名大学生，现在面临着整理学习资料和课程作业的任务。请按照以下要求完成相关的文件夹和文件管理操作：
   
   1. 在你的个人文档目录下创建一个名为`学习资料`的文件夹，并进入该文件夹。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~$ mkdir 学习资料
      whh@iZbp1caa4ll2hntdi7lfnmZ:~$ ls
      学习资料
      
      ```
   
   2. 在`学习资料`文件夹中创建一个名为`计算机科学`的文件夹。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~$ cd 学习资料
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料$ mkdir 计算机科学
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料$ ls
      计算机科学
      
      ```
   
   3. 在`计算机科学`文件夹中创建两个子文件夹，分别命名为`课程资料`和`编程项目`。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料$ cd 计算机科学
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ mkdir 课程资料 编程项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
      编程项目  课程资料
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
      编程项目  课程资料
      
      ```
   
   4. 将你最近的一门计算机科学课程的课件文件（假设文件名为`CS101_第一讲.pdf`）放入`课程资料`文件夹。
      
      ```js
      C:\Users\仔仔梅> scp E:010_路由器RIP动态路由配置.pdf whh@47.98.196.218:/home/whh/学习资料/计算机科学/课程资料
      whh@47.98.196.218's password:
      E:010_路由器RIP动态路由配置.pdf                                              100%  163KB   1.6MB/s   00:00
      ```
   
   5. 在`编程项目`文件夹中创建一个名为`Java项目`的文件夹。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~$ cd 学习资料/计算机科学/编程项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目$ mkdir java项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目$ ls
      java项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目$ 
      
      ```
   
   6. 在`Java项目`文件夹中创建两个空文件，分别命名为`主程序.java`和`工具类.java`。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目$ cd java项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ touch 主程序.java 工具类.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ ls
      主程序.java  工具类.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ 
      ```
   
   7. 复制`主程序.java`并命名为`备份_主程序.java`。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ cp 主程序.java 备份_主程序.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ ls
      主程序.java  备份_主程序.java  工具类.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ 
      
      ```
   
   8. 创建一个名为`Python项目`的文件夹，并将`工具类.java`移动到`Python项目`文件夹中。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ mv 工具类.java /home/whh/学习资料/计算机科学/Python项目
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ ls
      主程序.java  备份_主程序.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ 
      
      ```
   
   9. 列出`计算机科学`文件夹中所有文件和文件夹的内容。
      
      ```js
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目/java项目$ cd ../
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学/编程项目$ cd ../
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ld
      ld: no input files
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
      Python项目  编程项目  课程资料
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ find
      .
      ./课程资料
      ./课程资料/E:010_路由器RIP动态路由配置.pdf
      ./编程项目
      ./编程项目/java项目
      ./编程项目/java项目/主程序.java
      ./编程项目/java项目/备份_主程序.java
      ./Python项目
      ./Python项目/工具类.java
      whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ 
      
      ```
   
   10. 删除`编程项目`文件夹及其包含的所有内容
       
       ```js
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ rm -r 编程项目
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
       Python项目  课程资料
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ 
       
       ```
   
   11. 重命名`Python项目`为`数据分析项目`。
       
       ```js
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ rm -r 编程项目
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
       Python项目  课程资料
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ mv Python项目 数据分析项目
       whh@iZbp1caa4ll2hntdi7lfnmZ:~/学习资料/计算机科学$ ls
       数据分析项目  课程资料
       
       ```
   
   12. 最后，列出当前所在目录的路径。
       
       ```js
       
       ```
