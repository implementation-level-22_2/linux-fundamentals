# 笔记

在给你的网站配置SSL证书之前，你需要确保达成以下条件：

1. 购买域名并备案
2. 购买云服务器
3. 确定自己的服务器类型——nginx（我的）

接下来在对应的云服务器上查找SSL相关配置文档，我的是阿里云

## 参考文档

```
# https://developer.aliyun.com/article/1211986?spm=5176.21213303.J_qCOwPWspKEuWcmp8qiZNQ.47.4a402f3dfjM9Lm&scm=20140722.S_community@@%E6%96%87%E7%AB%A0@@1211986._.ID_community@@%E6%96%87%E7%AB%A0@@1211986-RL_SSL%E8%AF%81%E4%B9%A6%E5%85%8D%E8%B4%B9-LOC_llm-OR_ser-V_3-RE_new2-P0_3

# https://help.aliyun.com/zh/ssl-certificate/user-guide/install-ssl-certificates-on-nginx-servers-or-tengine-servers?spm=a2c4g.11186623.0.i1#concept-n45-21x-yfb
```

1. 申请SSL证书
2. 进入nginx配置，修改，开放443端口

```bash
/etc/nginx/config/..comconfig
```

## 重启Nginx以应用新配置

```bash
systemctl restart nginx
```
