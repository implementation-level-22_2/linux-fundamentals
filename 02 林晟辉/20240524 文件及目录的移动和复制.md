相关和目录可自行创建后再操作

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c

   ```shell
   touch exam.c && cp exam.c /tmp/shiyan.c
   ```

2. 在任何目录下回到用户主目录？

   ```shell
   cd ~
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？

   ```shell
   ls -al /tmp/test
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar

   ```shell
   cd /tmp/test2 && touch 1.txt 2.txt 3.txt 4.txt 5.txt
   ```

5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？

   ```shell
   touch file1.txt && mv file1.txt file2.txt
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？

   ```shell
   vim bbbb.txt
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？

   ```shell
   cp *.txt *.doc *.bak /tmp/test
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。

   ```shell
   mv file1.txt /home/qaq/docs
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。

   ```shell
   cp file2.txt /home/qaq/backup
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。

    ```shell
    mv -r docs/* archive/
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。

    ```shell
    cp -r photos backup
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。

    ```shell
    mv docs/report.doc papers/final_report.doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。

    ```shell
    touch docs/notes.txt && cp docs/notes.txt backup
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。

    ```shell
    cp images/*.jpg photos
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。

    ```shell
    cp docs/file1.txt docs/file2.txt backup
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中。

    ```shell
    cp docs/*.txt text_files
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。

    ```shell
    mv docs/*.* temp
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。

    ```shell
    mv -v docs/*.* archive
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。

    ```shell
    cp -r docs/*/ backup
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。

    ```shell
    mv docs/!(temp_*) backup
    ```

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。

    ```shell
    mv -b report.txt_bak docs/report.txt archive
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

    ```shell
    mv -n docs/*.pdf pdf_files
    ```

    