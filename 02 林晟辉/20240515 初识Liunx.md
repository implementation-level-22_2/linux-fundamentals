## Liunx

```
一种操作系统内核，有centos，debian，redhat等。
```

## 命令

### 查看ip地址

```js
ip addr show
```

### ssh

```js
apt-get install ssh

apt-get install vim

vim /etc/ssh/sshd_config

port 21 打开端口

permitrootlogin yes 启用root登录

passwordauthentication yes 验证密码

permitemptypasswords no 禁止使用空密码登录

/etc/init.d/ssh restart
```

### sudo

```
su root 更换给root模式

apt-get install sudo

sudo vim /etc/sudoers

root all=(all:all) all

用户名 all=(all:all) all 添加用户名
```

