

# 作业

~~~java
1. ### 综合练习题：大学生生活场景中的文件夹和文件管理

   假设你是一名大学生，现在面临着整理学习资料和课程作业的任务。请按照以下要求完成相关的文件夹和文件管理操作：

   1. 在你的个人文档目录下创建一个名为`学习资料`的文件夹，并进入该文件夹。
   
     xxx@iZbp165mdz3f1zavzs9xn7Z:~$ mkdir -p /学习资料/计算机科学

   
   2. 在`学习资料`文件夹中创建一个名为`计算机科学`的文件夹。
   
     xxx@iZbp165mdz3f1zavzs9xn7Z:~$ mkdir -p /学习资料/计算机科学

   
   3. 在`计算机科学`文件夹中创建两个子文件夹，分别命名为`课程资料`和`编程项目`。
   
    xxx@iZbp165mdz3f1zavzs9xn7Z:~$ cd /home/xxx/学习资料/计算机科学
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ mkdir 课程编码 编码项目

   
   4. 将你最近的一门计算机科学课程的课件文件（假设文件名为`CS101_第一讲.pdf`）放入`课程资料`文件夹。
   
 xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ cd /home/xxx/学习资料/计算机科学/课程编码
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/课程编码$ touch cs101第一讲.pdf
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/课程编码$ nano cs101第一讲.pdf

   
   5. 在`编程项目`文件夹中创建一个名为`Java项目`的文件夹。
   xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ cd /home/xxx/学习资料/计算机科学/编码项目
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目$ mkdir java项目

    
   
   6. 在`Java项目`文件夹中创建两个空文件，分别命名为`主程序.java`和`工具类.java`。
   
     xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目/java项目$ touch 主程序.java 工具类.java

   
   7. 复制`主程序.java`并命名为`备份_主程序.java`。
   
      xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目/java项目$ cp 主程序.java 备份_主程序.java

   
   8. 创建一个名为`Python项目`的文件夹，并将`工具类.java`移动到`Python项目`文件夹中。
   
     xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目/java项目$ mkdir python项目
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目/java项目$ mv 工具类.java python项目/

   
   9. 列出`计算机科学`文件夹中所有文件和文件夹的内容。
   
     xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ ls -1

  
   
   10. 删除`编程项目`文件夹及其包含的所有内容。
   
      xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ rm -rf 编程项目
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学$ ls
编码项目  课程编码
（删除失败 无语了都）
   
   11. 重命名`Python项目`为`数据分析项目`。
   xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目$ cd java项目/
xxx@iZbp165mdz3f1zavzs9xn7Z:~/学习资料/计算机科学/编码项目/java项目$ mv python项目 数据分析项目

   
   12. 最后，列出当前所在目录的路径。
   
      xxx@iZbp165mdz3f1zavzs9xn7Z：cd /home/xxx/学习资料/计算机科学/编码项目/java项目
   
~~~



