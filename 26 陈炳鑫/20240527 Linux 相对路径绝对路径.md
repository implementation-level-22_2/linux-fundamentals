相关和目录可自行创建后再操作

1. 在家目录下建立文件exam.c，将文件exam.c拷贝到/tmp这个目录下，并改名为 shiyan.c

   ```bash
   touch /home/username/exam.c && cp /home/username/exam.c /tmp/shiyan.c
   touch ./exam.c && cp ./exam.c ../../temp/shiyan.c
   ```

2. 在任何目录下回到用户主目录？

   ```bash
   cd ~
   cd
   cd /home/username
   ```

3. 用长格式列出/tmp/test目录下所有的文件包括隐藏文件？

   ```bash
   ls -al /tmp/test
   ls -al ../../tmp/test
   ```

4. /tmp/test2目录下，创建5个文件分别是 1.txt 2.txt 3.txt 4.txt 5.txt，压缩这5个文件，压缩包的名字是hailiang.tar

   ```bash
   touch /tmp/test2/{1..5}.txt && tar -czvf hailiang.tar /tmp/test2/{1..5}.txt
   touch ../../tmp/test2/{1..5}.txt && tar -czvf hailiang.tar ../../tmp/test2/{1..5}.txt
   ```

5. 当前目录，建立文件 file1.txt 并更名为 file2.txt？

   ```bash
   touch /home/username/file1.txt && mv /home/username/file1.txt /home/username/file2.txt
   touch ./file1.txt && mv ./file1.txt ./file2.txt
   ```

6. 当前目录，用vim建立文件bbbb.txt 并将用户名的加入其中保存退出？

   ```bash
   vim /home/username/bbbb.txt
   vim ./bbbb.txt
   ```

7. 将家目录中扩展名为txt、doc和bak的文件全部复制到/tmp/test目录中？

   ```bash
   cp /home/username/*.txt /home/username/*.doc /home/username/*.bak /tmp/test
   cp ./*.txt ./*.doc ./*.bak ../../temp/test
   ```

8. 将文件file1.txt从当前目录移动到家目录的/docs中。

   ```bash
   mv /home/username/file1.txt /home/username/docs
   mv ./file1.txt ./docs
   ```

9. 复制文件file2.txt从当前目录到家目录/backup中。

   ```bash
   cp /home/username/file2.txt /home/username/backup
   cp ./file2.txt ./backup
   ```

10. 将家目录/docs中的所有文件和子目录移动到家目录/archive中。

    ```bash
    mv -p /home/username/docs/* /home/username/archive
    mv -p ./docs/* ./archive
    ```

11. 复制家目录/photos及其所有内容到家目录/backup中。

    ```bash
    cp -p /home/username/photos /home/username/backup
    cp -p ./photos ./backup
    ```

12. 将文件家目录/docs/report.doc移动到家目录/papers中，并将其重命名为final_report.doc。

    ```bash
    mv /home/username/docs/report.doc /home/username/papers/final_report.doc
    mv ./docs/report.doc ./papers/final_report.doc
    ```

13. 在家目录/docs中创建一个名为notes.txt的空文件，并将其复制到目录家目录/backup中。

    ```bash
    touch /home/username/docs/notes.txt && cp /home/username/docs/notes.txt /home/username/backup
    touch ./docs/notes.txt && cp ./docs/notes.txt ./backup
    ```

14. 复制家目录/images中所有以.jpg结尾的文件到家目录/photos中。

    ```bash
    cp /home/username/images/*.jpg /home/username/photos
    cp ./images/*.jpg ./photos
    ```

15. 将文件家目录/docs/file1.txt和家目录/docs/file2.txt复制到家目录/backup中。

    ```bash
    cp /home/username/docs/{file1,file2}.txt /home/username/backup
    cp ./docs/{file1,file2}.txt ./backup
    ```

16. 将家目录/docs中的所有.txt文件复制到家目录/text_files中。

    ```bash
    cp /home/username/docs/*.txt /home/username/text_files
    cp ./docs/*.txt ./text_files
    ```

17. 将家目录/docs中的所有文件移动到家目录/temp中，并且如果文件已存在，则覆盖它们。

    ```bash
    mv -f /home/username/docs/* /home/username/temp
    mv -f ./docs/* ./temp
    ```

18. 将家目录/docs中的所有文件移动到家目录/archive中，并且在移动时显示详细的移动信息。

    ```bash
    mv -v /home/username/docs/* /home/username/archive
    mv -v ./docs/* ./archive
    ```

19. 复制家目录/docs中的所有子目录及其内容到家目录/backup中。

    ```bash
    cp -r /home/username/docs/* /home/username/backup
    cp -r ./docs/* ./archive
    ```

20. 将家目录/docs中的所有文件和子目录移动到家目录/backup中，但排除文件名以"temp_"开头的文件。

    ```bash
    mv -r /home/username/docs/!(temp_)* /home/username/backup
    mv -r ./docs/!(temp_*) ./backup
    ```

21. 将目录/docs/report.txt移动到家目录/archive中，但如果目标目录中已存在同名文件，则不直接覆盖，先备份同名文件为report.txt_bak。

    ```bash
    mv -b /home/qaq/report.txt_bak /home/username/docs/report.txt /home/username/archive
    mv -b ./docs/report.txt_bak ./docs/report.txt ./archive
    ```

22. 将家目录/docs中所有以.pdf结尾的文件复制到家目录/pdf_files中，并且如果目标目录中已存在同名文件，则忽略它们。

    ```bash
    mv -n /home/username/docs/*.pdf /home/username/pdf_files
    mv -n ./docs/*.pdf ./pdf_files
    ```