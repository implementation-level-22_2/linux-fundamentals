# Debian 安装postgreSQL

## 1、安装postgreSQL

对于Debian系统，APT软件包管理就有postgreSQL

postgreSQL官网：[PostgreSQL: Linux downloads (Debian)](https://www.postgresql.org/download/linux/debian/)

```bash
apt update
apt install postgresql -y
sudo -u postgres psql -c "SELECT version();" #验证postgreSQL版本 若出现：PostgreSQL 11.5 (Debian 11.5-1+deb10u1) on x86_64-pc-linux-gnu, compiled by gcc (Debian 8.3.0-6) 8.3.0, 64-bit 类似该提示及表示安装成功
```

## 2、配置postgreSQL

修改管理密码（我个人安装过程中并未弹出密码设置页面，其他人我就不知道了）

```bash
sudo -i -u postgres #切换到postgres用户
psql #进入postgreSQL shell
ALTER USER postgres PASSWORD 'new_password'; #修改postgres的密码
\q #退出postgreSQL shell
psql -U postgres -W #验证登录
```

此时就可以用新密码在本地登录到postgreSQL

要想配置远程访问需要修改两个配置文件 1.**postgresql.conf** 2.  **pg_hba.conf**

```bash
vim /etc/postgresql/你安装的版本号/main/postgresql.conf
#原代码：“#listen_addresses = 'localhost'”
#修改后：“listen_addresses = '*'” *表示任意主机
vim /etc/postgresql/你安装的版本号/main/pg_hba.conf
#在文件末尾追加：“host all all 0.0.0.0/0 md5”
```

此时可以从任意主机使用默认端口访问到postgreSQL

## 3、配置Navicat

对于Navicat 17以下版本，由于postgreSQL 15及以上版本字段修改，会导致连接postgreSQL出现以下报错：

![900e28cfa7cf43c991f12a5ee76e4e2.png](https://s2.loli.net/2024/07/02/XNYshtMRoZ2Jfda.png)

此时需要修改Navicat安装路径下的libcc.dll文件，该文件需要十六进制编辑器，在线编辑网址：https://hexed.it

使用该网站打开libcc.dll（请自行提前备份该文件）查找"SELECT DISTINCT datlastsysoid" 替换为 "SELECT DISTINCT dattablespace" 后生成新的名为libcc.dll的dll文件并将原文件进行替换即可