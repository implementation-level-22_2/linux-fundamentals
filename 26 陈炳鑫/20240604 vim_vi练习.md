1. vi 编辑器有几种模式?

   ```
    刚进入vi 时的默认模式。这个模式下能够进行：移动光标、整行的复制粘贴、整行删除 等基本操作。
      #编辑模式：
      
      #命令行模式（末行模式）：
      在一般指令模式下，按 ":" "/" "?" 均可进入命令行模式。由于此模式的输入会显示在窗口的最后一行，也叫末行模式。此模式下能够进行：搜索、保存、离开 等操作。
   ```

2. 如何进入 vi 编辑器的插入模式

   ```
   在一般指令模式下，按 "a" "i" "o" 均可进入编辑模式。此模式下能够进行：文本的输入、删除。
   ```

3. 如何进入 vi 编辑器的可视化模式

   ```
   在Vim命令模式下，输入 v 或者 V 或者 Ctrl + v 都可进入可视化模式，这三个Vim可视化模式的主要区别在于：
      字符选择模式: 选中光标经过的所有字符，普通模式下按 v 进入
      行选择模式：选中光标经过的所有行，普通模式下按 V 进入
      块选择模式：选中一整个矩形框表示的所有文本，普通模式下按 Ctrl + v 进入
   ```

4. 在 vi 编辑器中如何复制一行

   ```
   yy -- 复制当前行
      可以鼠标拖动选择复制 按y复制选中的
   ```

5. 在 vi 编辑器中如何进行粘贴

   ```
   按p粘贴
      在当前光标之前粘贴按大写P
   ```

6. 如何删除从 3 行到 15 行的所有数据

   ```
   //删除行范围
      在命令行模式下，
      :3,15d
   ```

7. vim练习：

   - 光标移动练习，命令模式下：

     - 单位级 h j k l
     - 单词级 w e b
     - 块级 gg G 0 ^ $ H M L ngg nj nk

     把下列句子按照第一句的正确顺序修改好并把多余的空行删除

     ```
     :/^$/d- 删除空行
     ```
     
     
     
     ```
     this is a simple easy vim tutorial
     
     tutorial simple a easy this vim is
     is this tutorial vim simple a easy
     
     
     tutorial vim this is a easy simple
     tutorial easy vim simple a this is
     simple a vim easy tutorial is this
     
     tutorial is easy vim a simple this
     
     
     vim simple this tutorial a easy is
     a vim tutorial simple easy is this
     
     
     easy a simple vim is tutorial this
     vim tutorial is a easy simple this
     a this vim tutorial is easy simple
     this tutorial simple easy a is vim
     
     
     easy tutorial this simple a is vim
     a tutorial easy is this simple vim
     
     a tutorial vim is easy this simple
     simple this easy is vim tutorial a
     
     this tutorial is a easy simple vim
     vim is tutorial simple this easy a
     
     vim is simple this tutorial easy a
     easy a simple is vim this tutorial
     vim is tutorial simple a easy this
     this vim is tutorial simple easy a
     ```
     
     先敲出以下代码，然后修正以下代码中的错误单词、重复单词、错误格式、多余行，修改函数名为 typing 并为定时器添加 300 毫秒延迟
     
     ```
     const bbb = () => {
     // this is is a description
     //
     //   another descriptttion 
     const timer   = setTimeout(( ) => {
         console.log(that) alert('cool!')
         // awosome man !
     })
     }
     ```
     
     尝试在下面的文本中进行复制粘贴练习
     
     ```
     删除这一行
     粘贴到这一行下面
     剪切 ABC 并把它粘贴到 XYZ 前面，使这部分内容看起来像
     剪切 并把它粘贴到 ABC XYZ 前面。
     ```

     尝试修改下列文本的大小写

     ```
     Change this line to UPPERCASE, THEN TO lowercase.
     ```
     
     按下面的说明进行操作
     
     ```
     按 dd 删除本行
     按 . 重复删除操作
     2. 再删除两行
     这行也没了
     p 把刚才删掉的粘回来
     3. 又多出 6 行
     ```
     
     左缩进、右缩进练习
     
     ```
     在这一行上依次按 3>>，<< 和 <G 看看效果
     打酱油行，我从你的全世界走过
     ```