# Apache 安装、部署SSL证书、nginx共存

## 1.安装Apache

对于debain系统：

```bash
sudo apt update
sudo apt install apache2 -y
```

配置文件存在于：/etc/apache2 该目录下

## 2.nginx共存

要想Apache和nginx同时运行，需要保证端口不会冲突

要么修改Apache的监听端口要么修改nginx的监听端口

```bash
vim /etc/nginx/conf.d/*.conf #修改nginx网站的监听端口
vim /etc/apache2/ports.conf /etc/apache2/sites-enabled/*.conf #修改Apache网站的监听端口
```

## 3.部署SSL证书

和nginx一样Apache支持不同网站不同.conf文件进行管理目录任意只要Apache主配置文件能够加载到就行 个人建议是放在sites-enabled，Apache默认网站也在这里方便管理 

SSL证书获取方式参考之前的nginx部署SSL证书.md

### http区块：

对于http访问需要配置监听端口、网站目录、域名、日志路径等，和nginx类似但是代码块名称、关键字名称不同

```bash
<VirtualHost *:你自己的端口>
    ServerAdmin webmaster@localhost
	ServerName 域名
	ServerAlias 别名
	DocumentRoot 你自己的网站目录 #设定http直接访问
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

### https区块：

和http区块一样但是要额外配置SSL证书，配置方式和nginx一样但是证书格式不一样

```bash
a2enmod ssl #手动开启Apache的SSL模块 后再修改配置文件
```

```bash
<VirtualHost *:你自己的端口>
    ServerName 你自己的域名
    ServerAlias 域名的别名
    ServerAdmin webmaster@localhost
    DocumentRoot 你自己网站的目录
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    SSLEngine on
    SSLHonorCipherOrder on
    SSLProtocol TLSv1.1 TLSv1.2 TLSv1.3
    SSLCipherSuite ECDHE-RSA-AES128-GCM-SHA256:ECDHE:ECDH:AES:HIGH:!NULL:!aNULL:!MD5:!ADH:!RC4
    SSLCertificateFile 你自己的证书public.crt文件路径
    SSLCertificateKeyFile 你自己证书.key文件路径
    SSLCertificateChainFile 你自己证书chain.crt文件路径
</VirtualHost>
```

### http重定向跳转到https

Apache 也可以设置HTTP重定向到HTTPS写法多种 以下是个人喜欢的写法

```bash
vim /etc/apache2/apache2.conf
#原代码 ：
#		<Directory /var/www/>
#			Options Indexes FollowSymLinks
#			AllowOverride None
#			Require all granted
#		</Directory>
#修改后：
#		<Directory /var/www/>
#			Options Indexes FollowSymLinks
#			AllowOverride all
#			Require all granted
#		</Directory> 开启.htaccess功能
vim /var/www/你自己网站/.htaccess #创建或编辑.htaccess文件
#添加以下内容：
# RewriteEngine On 
# RewriteCond %{HTTPS} off 
# RewriteRule ^(.*) https://%{SERVER_NAME}:你自己的端口/ [R=301,L]
a2enmod rewrite #手动开启Apache的rewrite模块
```

