# 一、排序

sort 

简介：sort命令的功能是对文件内容进行排序。有时文本中的内容顺序不正确，一行行地手动修改实在太麻烦了。此时使用sort命令就再合适不过了，它能够对文本内容进行再次排序。

语法：

```bash
sort 参数 文件
#参数参考https://www.linuxcool.com/sort
#常用:-t 指定分隔符 -n按数值排列 -r反序 -k num 对num列进行排序 num 可以是多个以逗号隔开 也可以是以点隔开
#如 1,3 表示 第一列到第三列 1.2 表示 第一列第二个字符
sort -t '' -k num filename.filetype
```

# 二、去重

uniq

简介：

uniq命令来自英文单词unique的缩写，中文译为“独特的、唯一的”，其功能是去除文件中的重复内容行。uniq命令能够去除掉文件中相邻的重复内容行，如果两端相同内容，但中间夹杂了其他文本行，则需要先使用sort命令进行排序后再去重，这样保留下来的内容就都是唯一的了。

语法：

```bash
uniq 参数 文件
#参数参考https://www.linuxcool.com/uniq
#常于sort配合使用
#常用:-c显示重复的次数
uniq -c 文件
```

# 三、安装MySQL

1. 先从MySQL官网下载MySQL APT Repository的.deb包然后使用dpkg命令安装该deb包

   ```bash
   wget https://repo.mysql.com//mysql-apt-config_0.8.30-1_all.deb
   dpkg -i mysql-apt-config_0.8.30-1_all.deb
   ```

   配置除了MySQL Preview Packages 可以改成enable 其余默认就行 

   ![image.png](https://s2.loli.net/2024/07/01/azGm9VEyRO21Yr6.png)

2. 此时才能用apt 安装mysql-server组件，未安装第一步的deb包会报错提示没有该包

   ```bash
   apt-get update
   apt-get install mysql-server -y
   ```

   安装期间会让设置root密码，其余默认就行

3. 此时 可以使用命令查看版本和连接数据库

   ```bash
   mysql --version
   mysql -u root -p #会让用户输入root密码 输入正确密码后 提示符变为 mysql>
   ```

   此时已经进入到mysql数据库模式，但还不是可视化界面若要让其余主机能访问到还需要对用户进行设置

   ```sql
   CREATE USER 'USERNAME'@'%' IDENTIFIED BY 'PASSWD';#创建作用域为任意主机的用户
   GRANT ALL PRIVILEGES ON *.* TO 'USERNAME'@'%' WITH GRANT OPTION;#赋予所有权限
   FLUSH PRIVILEGES;#刷新权限列表
   ```

   之后就可以使用刚刚创建的作用在所有主机的用户进行远程访问数据库

