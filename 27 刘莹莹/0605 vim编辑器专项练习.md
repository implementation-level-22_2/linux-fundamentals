1. vi 编辑器有几种模式?

   ```bash
   三种模式 ：
   
   1.命令模式：此模式下，可使用方向键（上下左右键）或k，j，h，i移动光标的位置，还可以对文件内容进行复制，粘贴，替换，删除等操作。
   
   2.输入模式
   
   3.末行模式
   ```

   

2. 如何进入 vi 编辑器的插入模式

   ```bash
   a i o 命令行模式切换为插入模式
   
   a/A
   
   i/I
   
   o/O
   ```

   

3. 如何进入 vi 编辑器的可视化模式

   

4. 在 vi 编辑器中如何复制一行

   ```
   y0
   ```

   

5. 在 vi 编辑器中如何进行粘贴

     

   ```bash
   p -- 粘贴剪贴板内容到光标下方
      P -- 粘贴剪贴板内容到光标上方
   ```

   

6. 如何删除从 3 行到 15 行的所有数据

   ```js
   //删除行范围
   在命令行模式下，
   ：3,15d
   ```

7. vim练习：

   - 光标移动练习，命令模式下：

     - 单位级 h j k l
     - 单词级 w e b
     - 块级 gg G 0 ^ $ H M L ngg nj nk

     把下列句子按照第一句的正确顺序修改好并把多余的空行删除

     ```bash
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     this is a simple easy vim tutorial
     ...
     ```

     先敲出以下代码，然后修正以下代码中的错误单词、重复单词、错误格式、多余行，修改函数名为 typing 并为定时器添加 300 毫秒延迟

     ```bash
     const bbb = typing() => {
     // this is a description
     //   another description 
     const timer = setTimeout(( ) => {
         console.log(that) alert('cool!',300)
         // awosome man !
     })
     }
     ```

     尝试在下面的文本中进行复制粘贴练习

     ```bash
     dd -- 删除这一行
     yy,p -- 粘贴到这一行下面
     v,d,P -- 剪切 ABC 并把它粘贴到 XYZ 前面，使这部分内容看起来像
     剪切 并把它粘贴到 ABC XYZ 前面。
     ```

     尝试修改下列文本的大小写

     ```bash
     Change this line to UPPERCASE, THEN TO lowercase.
     gu -- 全部修改为小写
     gU -- 全部修改为大写
     
     guw -- 单词修改为小写
     gUw -- 单词修改为大写
     
     gu~ -- 字符修改为小写
     gU~ -- 字符修改为大写
     ```

     按下面的说明进行操作

     ```bash
     按 dd 删除本行 //删除第一行
     按 . 重复删除操作 //删除第一行
     2. 再删除两行 //删除第三、四行
     p 把刚才删掉的粘回来 //第五行之下粘贴第三、四行
     3. 又多出 6 行 //重复上一次操作三次
     ```

     左缩进、右缩进练习

     ```bash
     在这一行上依次按 3>>，<< 和 <G 看看效果
     打酱油行，我从你的全世界走过
     3>>:前三行右缩进
     <<：当前行左缩进
     <G：全部左缩进
     ```

